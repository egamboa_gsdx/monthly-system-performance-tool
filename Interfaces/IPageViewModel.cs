﻿using MaterialDesignThemes.Wpf;
using System.Collections.Generic;

namespace GSDSystemPerformanceTool
{
    public interface IPageViewModel
    {
        public bool IsSelected { get; set; }
        public string ProgressIsVisible { get; set; }
        public string Opacity { get; set; }
        public string SelectedYear { get; set; }
        public string Name { get; set; }
        public List<ISubPageViewModel> Views { get; set; }
    }
}
