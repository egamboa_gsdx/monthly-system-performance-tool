﻿using MaterialDesignThemes.Wpf;

namespace GSDSystemPerformanceTool
{
    public interface ISubPageViewModel
    {
        public bool IsSelected { get; set; }
        public string ProgressIsVisible { get; set; }
        public string SelectedYear { get; set; }
        public string Name { get; set; }
        public PackIconKind Icon { get; set; }
    }
}
