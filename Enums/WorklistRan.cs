﻿namespace GSDSystemPerformanceTool.Enums
{
    public enum WorklistRanStatus
    {
        WorklistRan = 0,
        Maintenance = 1,
        WorklistAndMaintenance = 2,
        NoWorklistRan = 3
    }
}
