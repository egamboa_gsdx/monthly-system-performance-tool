﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using GSDSystemPerformanceTool.Utilities;


namespace GSDSystemPerformanceTool
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string[] Args;
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            if (e.Args.Length > 0)
            {
                Args = e.Args;
                if (Args[0].Equals("collect"))
                {
                    DataCollector dataCollector = new DataCollector();
                    dataCollector.CollectData();
                    MainWindow.Close();
                }
            }

            MainWindow = new MainWindow();
            MainWindow.Show();
        }
    }
}
