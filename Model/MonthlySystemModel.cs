﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Collections.ObjectModel;
using PropertyChanged;

namespace GSDSystemPerformanceTool.Model
{
    [AddINotifyPropertyChangedInterface]
    public class MonthlySystemModel
    {

        public MonthlySystemModel(int numOfDays, string month, string year,
            string dir = @"C:\Users\Public\Documents\Storm\Status Log Files\",
            string dbDir = @"C:\ProgramData\Gold Standard Diagnostics\Storm\DataFolder\DataBase\StormDB.sqlite",
            string application = "Storm Instrument Manager")
        {
            NumOfDays = numOfDays;
            Month = month;
            MonthNum = DateTime.ParseExact(Month, "MMM", CultureInfo.CurrentCulture).Month;
            Year = year;
            Days = new ObservableCollection<DailySystemModel>();

            for (int i = 0; i < numOfDays; i++)
            {
                Days.Add(new DailySystemModel(i + 1, Month, Year, dir, dbDir, application));
            }
        }
        public string Year { get; set; }
        public string Month { get; set; }
        public int MonthNum { get; set; }
        public int NumOfDays { get; set; }
        public TimeSpan FluidTransferAction { get; set; }
        public TimeSpan WashAction { get; set; }
        public TimeSpan ShakerAction { get; set; }
        public TimeSpan IncubateAction { get; set; }
        public TimeSpan HeatAction { get; set; }
        public TimeSpan ReadWellAction { get; set; }
        public TimeSpan PrimeAction { get; set; }
        public TimeSpan ProbeWashAction { get; set; }
        public TimeSpan TotalRunTime { get; set; }
        
        public TimeSpan ScreenTime { get; set; }
        public TimeSpan LowTime { get; set; }
        public TimeSpan HighTime { get; set; }
        public TimeSpan ExtraHighTime { get; set; }
        public TimeSpan TppaTime { get; set; }
        public TimeSpan RprTppaTime { get; set; }
        public int TotalWorklistRun { get; set; }
        public int StoppedWorklistRun { get; set; }
        public int CompletedWorklistRun { get; set; }
        public int ImageCaptureCount { get; set; }
        public int ScreenCount { get; set; }
        public int RprTppaCount { get; set; }
        public int LowCount { get; set; }
        public int HighCount { get; set; }

        public int ExtraHighCount { get; set; }
        public int TppaCount { get; set; }
        public string PrimeInstrumentCount { get; set; }
        public string ProbeAlignmentCount { get; set; }
        public string ReaderAlignmentCount { get; set; }
        public string ReaderCalibrationCount { get; set; }
        public string LuminescenceReaderCalCount { get; set; }
        public string WashPumpCalibrationCount { get; set; }
        public string CameraAlignmentCount { get; set; }
        public string CloseSoftwareCount { get; set; }
        public string NonSoftwareRelatedCount { get; set; }
        public string CameraFocusCount { get; set; }
        public string OpenSoftwareCount { get; set; }
        public ObservableCollection<DailySystemModel> Days { get; set; }

    }
}
