﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;

namespace GSDSystemPerformanceTool.Model
{
    [AddINotifyPropertyChangedInterface]
    public class YearlySystemModel
    {
        public YearlySystemModel(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
        public TimeSpan YearlyFluidTransferAction { get; set; }
        public TimeSpan YearlyWashAction { get; set; }
        public TimeSpan YearlyShakerAction { get; set; }
        public TimeSpan YearlyIncubateAction { get; set; }
        public TimeSpan YearlyHeatAction { get; set; }
        public TimeSpan YearlyReadWellAction { get; set; }
        public TimeSpan YearlyPrimeAction { get; set; }
        public TimeSpan YearlyProbeWashAction { get; set; }
        public TimeSpan YearlyTotalRunTime { get; set; }
        public TimeSpan YearlyScreenTime { get; set; }
        public TimeSpan YearlyLowTime { get; set; }
        public TimeSpan YearlyHighTime { get; set; }
        public TimeSpan YearlyExtraHighTime { get; set; }
        public TimeSpan YearlyTppaTime { get; set; }
        public TimeSpan YearlyRprTppaTime { get; set; }
        public string YearlyPrimeInstrumentCount { get; set; }
        public string YearlyProbeAlignmentCount { get; set; }
        public string YearlyReaderAlignmentCount { get; set; }
        public string YearlyReaderCalibrationCount { get; set; }
        public string YearlyLuminescenceReaderCalCount { get; set; }
        public string YearlyWashPumpCalibrationCount { get; set; }
        public string YearlyCameraAlignmentCount { get; set; }
        public string YearlyCloseSoftwareCount { get; set; }
        public string YearlyNonSoftwareRelatedCount { get; set; }
        public string YearlyCameraFocusCount { get; set; }
        public string YearlyOpenSoftwareCount { get; set; }
        public int YearlyTotalWorklistRun { get; set; }
        public int YearlyStoppedWorklistRun { get; set; }
        public int YearlyCompletedWorklistRun { get; set; }
        public int YearlyImageCaptureCount { get; set; }
        public int YearlyScreenCount { get; set; }
        public int YearlyLowCount { get; set; }
        public int YearlyHighCount { get; set; }
        public int YearlyExtraHighCount { get; set; }
        public int YearlyTppaCount { get; set; }
        public int YearlyRprTppaCount { get; set; }

    }
}
