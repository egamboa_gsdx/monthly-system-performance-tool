﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Data.SQLite;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;
using GSDSystemPerformanceTool.Utilities;
using GSDSystemPerformanceTool.Converters;
using GSDSystemPerformanceTool.Enums;
using GSD.TestLab.DataCollection;
using GSD.TestLab.DataCollection.Enums;
using System.Security.Permissions;

namespace GSDSystemPerformanceTool.Model
{
    [AddINotifyPropertyChangedInterface]
    public class DailySystemModel
    {
        private string _month;
        private string _year;
        private string _delimiter;
        private DataCollection _data;
        public DailySystemModel(int day, string month, string year, string filepath, string dbFilePath, string application)
        {
            DayIndex = day;
            _month = month;
            _year = year;
            _data = new DataCollection();
            _delimiter = "M;";
            Header = string.Format("{0} {1}\t(No Worklist Ran)", Month, DayIndex);
            WorklistRan = WorklistRanStatus.NoWorklistRan;

            GetData(filepath);
        }
        public int DayIndex { get; set; }
        public string Month
        {
            get { return _month; }
            set { _month = value; }
        }
        public string Header { get; set; }
        public TimeSpan DailyFluidTransferAction { get; set; }
        public TimeSpan DailyWashAction { get; set; }
        public TimeSpan DailyShakerAction { get; set; }
        public TimeSpan DailyIncubateAction { get; set; }
        public TimeSpan DailyHeatAction { get; set; }
        public TimeSpan DailyReadWellAction { get; set; }
        public TimeSpan DailyPrimeAction { get; set; }
        public TimeSpan DailyProbeWashAction { get; set; }
        public TimeSpan DailyTotalRunTime { get; set; }
        public TimeSpan DailyScreenTime { get; set; }
        public TimeSpan DailyLowTime { get; set; }
        public TimeSpan DailyHighTime { get; set; }
        public TimeSpan DailyExtraHighTime { get; set; }
        public TimeSpan DailyTppaTime { get; set; }
        public TimeSpan DailyRprTppaTime { get; set; }
        public string DailyPrimeInstrumentCount { get; set; }
        public string DailyProbeAlignmentCount { get; set; }
        public string DailyReaderAlignmentCount { get; set; }
        public string DailyReaderCalibrationCount { get; set; }
        public string DailyLuminescenceReaderCalCount { get; set; }
        public string DailyWashPumpCalibrationCount { get; set; }
        public string DailyCameraAlignmentCount { get; set; }
        public string DailyCloseSoftwareCount { get; set; }
        public string DailyNonSoftwareRelatedCount { get; set; }
        public string DailyCameraFocusCount { get; set; }
        public string DailyOpenSoftwareCount { get; set; }
        public string DailyBuild { get; set; }
        public string DailyStartPrime { get; set; }
        public string Dailyliquinox { get; set; }
        public string DailyDI50 { get; set; }
        public string DailyShutdownEmptyWaste { get; set; }
        public string DailyShutdownDI50 { get; set; }
        public string DailyAutoAlign { get; set; }
        public string DailyManualAlign { get; set; }
        public int DailyTotalWorklistRun { get; set; }
        public int DailyStoppedWorklistRun { get; set; }
        public int DailyCompletedWorklistRun { get; set; }
        public int DailyImageCaptureCount { get; set; }
        public int DailyScreenCount { get; set; }
        public int DailyLowCount { get; set; }
        public int DailyHighCount { get; set; }
        public int DailyExtraHighCount { get; set; }
        public int DailyTppaCount { get; set; }
        public int DailyRprTppaCount { get; set; }
        public bool IsExpanded { get; set; }
        public WorklistRanStatus WorklistRan { get; set; }

        private void GetData(string dir = @"C:\Users\Public\Documents\Storm\Status Log Files\")
        {
            int monthnum = DateTime.ParseExact(_month, "MMM", CultureInfo.CurrentCulture).Month;
            var folder = dir + $"\\{_year}_{monthnum:D2}";

            if (Directory.Exists(folder))
            {
                string[] allfilepaths = Directory.GetFiles(folder);
                if (allfilepaths.Length == 0)
                {
                    DailyFluidTransferAction = TimeSpan.FromSeconds(0);
                    DailyReadWellAction = TimeSpan.FromSeconds(0);
                    DailyWashAction = TimeSpan.FromSeconds(0);
                    DailyPrimeAction = TimeSpan.FromSeconds(0);
                    DailyShakerAction = TimeSpan.FromSeconds(0);
                    DailyHeatAction = TimeSpan.FromSeconds(0);
                    DailyIncubateAction = TimeSpan.FromSeconds(0);
                    DailyTotalRunTime = TimeSpan.FromSeconds(0);
                    DailyTotalWorklistRun = 0;
                    DailyCompletedWorklistRun = 0;
                    DailyStoppedWorklistRun = 0;
                    DailyCompletedWorklistRun = 0;
                    DailyImageCaptureCount = 0;
                    DailyProbeAlignmentCount = "0";
                    DailyCameraAlignmentCount = "0";
                    DailyReaderAlignmentCount = "0";
                    return;
                }

                List<string> dailyfiles = new List<string>();
                int worklistCount = 0;
                for (int i = 0; i < allfilepaths.Length; i++)
                {
                    var modifiedDate = File.GetLastWriteTime(allfilepaths[i]);
                    var modDay = modifiedDate.Day;
                    var modMonth = modifiedDate.ToString("MMM");
                    if (modMonth == Month && modDay == DayIndex)
                    {
                        dailyfiles.Add(allfilepaths[i]);
                        worklistCount++;
                    }
                }
                DailyTotalWorklistRun = worklistCount;
                DailyStoppedWorklistRun = 0;

                
                int mliFile = 0;
                foreach (var dailyfile in dailyfiles)
                {
                    if (dailyfile.Contains("MLI"))
                    {
                        mliFile++;
                    }
                }
                    

                if (!dailyfiles.Any())
                {
                    DailyFluidTransferAction = TimeSpan.FromSeconds(0);
                    DailyReadWellAction = TimeSpan.FromSeconds(0);
                    DailyWashAction = TimeSpan.FromSeconds(0);
                    DailyPrimeAction = TimeSpan.FromSeconds(0);
                    DailyShakerAction = TimeSpan.FromSeconds(0);
                    DailyHeatAction = TimeSpan.FromSeconds(0);
                    DailyIncubateAction = TimeSpan.FromSeconds(0);
                    DailyTotalRunTime = TimeSpan.FromSeconds(0);
                    DailyTotalWorklistRun = 0;
                    DailyCompletedWorklistRun = 0;
                    DailyStoppedWorklistRun = 0;
                    DailyCompletedWorklistRun = 0;
                    DailyImageCaptureCount = 0;
                    DailyProbeAlignmentCount = "0";
                    DailyCameraAlignmentCount = "0";
                    DailyReaderAlignmentCount = "0";
                    return;
                }
                foreach (var dailyfile in dailyfiles)
                {


                    string[] filelines = File.ReadAllLines(dailyfile);
                    var totalruntimestring = filelines.SkipWhile(x => x != "----Worklist Actions----").Skip(1).TakeWhile(x => x != string.Empty).ToList();

                    if (int.Parse(_year) < 2021)
                    {
                        totalruntimestring = filelines.SkipWhile(x => !x.Contains("Worklist Started")).TakeWhile(x => x != string.Empty).ToList();
                    }

                    DailyTotalRunTime += _data.GetTotalWorklistRunTime(filelines.ToList());

                    if (dailyfile.Contains("Stopped"))
                    {
                        DailyStoppedWorklistRun++;
                    }
                    
                    DailyFluidTransferAction += _data.TotalFluidTransfer(filelines.ToList());
                    DailyReadWellAction += _data.GetActionTime(totalruntimestring, WorklistActionType.ReadWell);
                    DailyWashAction += _data.GetActionTime(totalruntimestring, WorklistActionType.WashWell);
                    DailyPrimeAction += _data.GetActionTime(totalruntimestring, WorklistActionType.Prime);
                    DailyShakerAction += _data.TotalShaker(filelines.ToList());
                    DailyHeatAction += _data.GetActionTime(filelines.ToList(), WorklistActionType.Heat);
                    DailyIncubateAction += _data.TotalIncubation(filelines.ToList());
                    DailyProbeWashAction += _data.GetActionTime(filelines.ToList(), WorklistActionType.ProbeWash);
                    DailyImageCaptureCount += _data.GetImageCaptureCount(filelines.ToList());
                    DailyScreenCount += _data.GetScreenCount(filelines.ToList());
                    DailyScreenTime += _data.GetScreenTime(filelines.ToList());
                    DailyLowCount += _data.GetLowCount(filelines.ToList());
                    DailyLowTime += _data.GetLowTime(filelines.ToList());
                    DailyHighCount += _data.GetHighCount(filelines.ToList());
                    DailyHighTime += _data.GetHighTime(filelines.ToList());
                    DailyExtraHighCount += _data.GetExtraHighCount(filelines.ToList());
                    DailyExtraHighTime += _data.GetExtraHighTime(filelines.ToList());
                    DailyTppaCount += _data.GetTppaCount(filelines.ToList());
                    DailyTppaTime += _data.GetTppaTime(filelines.ToList());
                    DailyRprTppaCount += _data.GetRprTppaCount(filelines.ToList());
                    DailyRprTppaTime += _data.GetRprTppaTime(filelines.ToList());
                }

                //DailyCompletedWorklistRun = DailyTotalWorklistRun - DailyStoppedWorklistRun - mliFile;
                DailyCompletedWorklistRun = DailyScreenCount + DailyLowCount+DailyHighCount+DailyExtraHighCount+DailyTppaCount+DailyRprTppaCount;
                if (DailyCompletedWorklistRun > 0)
                {
                    Header = string.Format("{0} {1} \t{2} Worklist(s) Ran (No Maintenance)", Month, DayIndex, DailyCompletedWorklistRun);
                    WorklistRan = WorklistRanStatus.WorklistRan;
                }
                
            }
            else
            {
                DailyFluidTransferAction = TimeSpan.FromSeconds(0);
                DailyReadWellAction = TimeSpan.FromSeconds(0);
                DailyWashAction = TimeSpan.FromSeconds(0);
                DailyPrimeAction = TimeSpan.FromSeconds(0);
                DailyShakerAction = TimeSpan.FromSeconds(0);
                DailyHeatAction = TimeSpan.FromSeconds(0);
                DailyIncubateAction = TimeSpan.FromSeconds(0);
                DailyTotalRunTime = TimeSpan.FromSeconds(0);
                DailyTotalWorklistRun = 0;
                DailyCompletedWorklistRun = 0;
                DailyStoppedWorklistRun = 0;
                DailyCompletedWorklistRun = 0;
                DailyImageCaptureCount = 0;
                DailyProbeAlignmentCount = "0";
                DailyCameraAlignmentCount = "0";
                DailyReaderAlignmentCount = "0";
            }
        }
    }
}
