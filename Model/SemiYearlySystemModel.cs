﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;

namespace GSDSystemPerformanceTool.Model
{
    [AddINotifyPropertyChangedInterface]
    public class SemiYearlySystemModel
    {
        public SemiYearlySystemModel(string name, int index)
        {
            Name = name;
            Index = index;
        }
        public string Name { get; set; }
        public int Index { get; set; }
        public TimeSpan SemiYearlyFluidTransferAction { get; set; }
        public TimeSpan SemiYearlyWashAction { get; set; }
        public TimeSpan SemiYearlyShakerAction { get; set; }
        public TimeSpan SemiYearlyIncubateAction { get; set; }
        public TimeSpan SemiYearlyHeatAction { get; set; }
        public TimeSpan SemiYearlyReadWellAction { get; set; }
        public TimeSpan SemiYearlyPrimeAction { get; set; }
        public TimeSpan SemiYearlyProbeWashAction { get; set; }
        public TimeSpan SemiYearlyTotalRunTime { get; set; }
        public TimeSpan SemiYearlyScreenTime { get; set; }
        public TimeSpan SemiYearlyLowTime { get; set; }
        public TimeSpan SemiYearlyHighTime { get; set; }
        public TimeSpan SemiYearlyExtraHighTime { get; set; }
        public TimeSpan SemiYearlyTppaTime { get; set; }
        public TimeSpan SemiYearlyRprTppaTime { get; set; }

        public string SemiYearlyPrimeInstrumentCount { get; set; }
        public string SemiYearlyProbeAlignmentCount { get; set; }
        public string SemiYearlyReaderAlignmentCount { get; set; }
        public string SemiYearlyReaderCalibrationCount { get; set; }
        public string SemiYearlyLuminescenceReaderCalCount { get; set; }
        public string SemiYearlyWashPumpCalibrationCount { get; set; }
        public string SemiYearlyCameraAlignmentCount { get; set; }
        public string SemiYearlyCloseSoftwareCount { get; set; }
        public string SemiYearlyNonSoftwareRelatedCount { get; set; }
        public string SemiYearlyCameraFocusCount { get; set; }
        public string SemiYearlyOpenSoftwareCount { get; set; }
        public int SemiYearlyTotalWorklistRun { get; set; }
        public int SemiYearlyStoppedWorklistRun { get; set; }
        public int SemiYearlyCompletedWorklistRun { get; set; }
        public int SemiYearlyImageCaptureCount { get; set; }
        public int SemiYearlyScreenCount { get; set; }
        public int SemiYearlyLowCount { get; set; }
        public int SemiYearlyHighCount { get; set; }
        public int SemiYearlyExtraHighCount { get; set; }
        public int SemiYearlyTppaCount { get; set; }
        public int SemiYearlyRprTppaCount { get; set; }
    }
}
