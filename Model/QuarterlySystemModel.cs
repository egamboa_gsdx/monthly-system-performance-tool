﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;

namespace GSDSystemPerformanceTool.Model
{
    [AddINotifyPropertyChangedInterface]
    public class QuarterlySystemModel
    {
        public QuarterlySystemModel(string name, int index)
        {
            Name = name;
            Index = index;
        }
        public string Name { get; set; }
        public int Index { get; set; }
        public TimeSpan QuarterlyFluidTransferAction { get; set; }
        public TimeSpan QuarterlyWashAction { get; set; }
        public TimeSpan QuarterlyShakerAction { get; set; }
        public TimeSpan QuarterlyIncubateAction { get; set; }
        public TimeSpan QuarterlyHeatAction { get; set; }
        public TimeSpan QuarterlyReadWellAction { get; set; }
        public TimeSpan QuarterlyPrimeAction { get; set; }
        public TimeSpan QuarterlyProbeWashAction { get; set; }
        public TimeSpan QuarterlyTotalRunTime { get; set; }
        public TimeSpan QuarterlyScreenTime { get; set; }
        public TimeSpan QuarterlyLowTime { get; set; }
        public TimeSpan QuarterlyHighTime { get; set; }
        public TimeSpan QuarterlyExtraHighTime { get; set; }
        public TimeSpan QuarterlyTppaTime { get; set; }
        public TimeSpan QuarterlyRprTppaTime { get; set; }

        public string QuarterlyPrimeInstrumentCount { get; set; }
        public string QuarterlyProbeAlignmentCount { get; set; }
        public string QuarterlyReaderAlignmentCount { get; set; }
        public string QuarterlyReaderCalibrationCount { get; set; }
        public string QuarterlyLuminescenceReaderCalCount { get; set; }
        public string QuarterlyWashPumpCalibrationCount { get; set; }
        public string QuarterlyCameraAlignmentCount { get; set; }
        public string QuarterlyCloseSoftwareCount { get; set; }
        public string QuarterlyNonSoftwareRelatedCount { get; set; }
        public string QuarterlyCameraFocusCount { get; set; }
        public string QuarterlyOpenSoftwareCount { get; set; }
        public int QuarterlyTotalWorklistRun { get; set; }
        public int QuarterlyStoppedWorklistRun { get; set; }
        public int QuarterlyCompletedWorklistRun { get; set; }
        public int QuarterlyImageCaptureCount { get; set; }
        public int QuarterlyScreenCount { get; set; }
        public int QuarterlyLowCount { get; set; }
        public int QuarterlyHighCount { get; set; }
        public int QuarterlyExtraHighCount { get; set; }

        public int QuarterlyTppaCount { get; set; }
        public int QuarterlyRprTppaCount { get; set; }
    }
}
