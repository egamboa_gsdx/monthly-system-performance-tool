﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using GSDSystemPerformanceTool.Enums;

namespace GSDSystemPerformanceTool.Converters
{
    public class WorklistRanColorConverter : IValueConverter
    {
        public object Convert(object value, Type targettype, object parameter, CultureInfo culture)
        {
            var val = (WorklistRanStatus)value;
            switch (val)
            {
                case WorklistRanStatus.WorklistRan: return "Blue";
                case WorklistRanStatus.Maintenance: return "Blue";
                case WorklistRanStatus.WorklistAndMaintenance: return "Green";
                case WorklistRanStatus.NoWorklistRan: return "Red";
                default: return "Red";
            }
        }

        public object ConvertBack(object value, Type targettype, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
