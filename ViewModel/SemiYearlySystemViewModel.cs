﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;
using MaterialDesignThemes.Wpf;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Command;
using GSDSystemPerformanceTool.Model;

namespace GSDSystemPerformanceTool.ViewModel
{
    [AddINotifyPropertyChangedInterface]
    public class SemiYearlySystemViewModel : ISubPageViewModel
    {
        private MonthlySystemViewModel _monthlyVM;

        public SemiYearlySystemViewModel()
        {
            Name = "Half Year-to-Date";
            Icon = PackIconKind.MusicNoteHalf;
            SemiYears = new ObservableCollection<SemiYearlySystemModel>()
            {
                new SemiYearlySystemModel("First Half", 0),
                new SemiYearlySystemModel("Second Half", 1)
            };
            SelectedYear = DateTime.Now.Year.ToString();
            GetSemiYearlyData(SelectedYear);
        }
        public bool IsSelected { get; set; }
        public string SelectedYear { get; set; }
        public string ProgressIsVisible { get; set; } = "Hidden";
        public string Name { get; set; }
        public PackIconKind Icon { get; set; }
        public ObservableCollection<SemiYearlySystemModel> SemiYears { get; set; }

        public async void GetSemiYearlyData(string year)
        {
            await Task.Run(() =>
            {
                SelectedYear = year;

                _monthlyVM = SimpleIoc.Default.GetInstance<MonthlySystemViewModel>();

                foreach (var semiyear in SemiYears)
                {
                    semiyear.SemiYearlyFluidTransferAction = TimeSpan.Zero;
                    semiyear.SemiYearlyWashAction = TimeSpan.Zero;
                    semiyear.SemiYearlyShakerAction = TimeSpan.Zero;
                    semiyear.SemiYearlyIncubateAction = TimeSpan.Zero;
                    semiyear.SemiYearlyHeatAction = TimeSpan.Zero;
                    semiyear.SemiYearlyReadWellAction = TimeSpan.Zero;
                    semiyear.SemiYearlyPrimeAction = TimeSpan.Zero;
                    semiyear.SemiYearlyProbeWashAction = TimeSpan.Zero;
                    semiyear.SemiYearlyTotalRunTime = TimeSpan.Zero;
                    semiyear.SemiYearlyScreenTime = TimeSpan.Zero;
                    semiyear.SemiYearlyLowTime = TimeSpan.Zero;
                    semiyear.SemiYearlyHighTime = TimeSpan.Zero;
                    semiyear.SemiYearlyExtraHighTime = TimeSpan.Zero;
                    semiyear.SemiYearlyTppaTime = TimeSpan.Zero;
                    semiyear.SemiYearlyRprTppaTime = TimeSpan.Zero;


                    semiyear.SemiYearlyPrimeInstrumentCount = "0";
                    semiyear.SemiYearlyProbeAlignmentCount = "0";
                    semiyear.SemiYearlyReaderAlignmentCount = "0";
                    semiyear.SemiYearlyReaderCalibrationCount = "0";
                    semiyear.SemiYearlyLuminescenceReaderCalCount = "0";
                    semiyear.SemiYearlyWashPumpCalibrationCount = "0";
                    semiyear.SemiYearlyCameraAlignmentCount = "0";
                    semiyear.SemiYearlyCameraFocusCount = "0";
                    semiyear.SemiYearlyOpenSoftwareCount = "0";
                    semiyear.SemiYearlyCloseSoftwareCount = "0";
                    semiyear.SemiYearlyNonSoftwareRelatedCount = "0";

                    semiyear.SemiYearlyTotalWorklistRun = 0;
                    semiyear.SemiYearlyCompletedWorklistRun = 0;
                    semiyear.SemiYearlyStoppedWorklistRun = 0;
                    semiyear.SemiYearlyImageCaptureCount = 0;
                    semiyear.SemiYearlyScreenCount = 0;
                    semiyear.SemiYearlyLowCount = 0;
                    semiyear.SemiYearlyHighCount = 0;
                    semiyear.SemiYearlyExtraHighCount = 0;
                    semiyear.SemiYearlyTppaCount = 0;
                    semiyear.SemiYearlyRprTppaCount = 0;

                }
                int result;
                foreach (var month in _monthlyVM.MonthlyLogs)
                {
                    if (month.Year.Equals(SelectedYear))
                    {
                        if (month.Month.Equals("Jan") || month.Month.Equals("Feb") || month.Month.Equals("Mar") ||
                            month.Month.Equals("Apr") || month.Month.Equals("May") || month.Month.Equals("Jun"))
                        {
                            SemiYears[0].SemiYearlyFluidTransferAction += month.FluidTransferAction;
                            SemiYears[0].SemiYearlyWashAction += month.WashAction;
                            SemiYears[0].SemiYearlyShakerAction += month.ShakerAction;
                            SemiYears[0].SemiYearlyIncubateAction += month.IncubateAction;
                            SemiYears[0].SemiYearlyHeatAction += month.HeatAction;
                            SemiYears[0].SemiYearlyReadWellAction += month.ReadWellAction;
                            SemiYears[0].SemiYearlyPrimeAction += month.PrimeAction;
                            SemiYears[0].SemiYearlyProbeWashAction += month.ProbeWashAction;
                            SemiYears[0].SemiYearlyTotalRunTime += month.TotalRunTime;
                            SemiYears[0].SemiYearlyImageCaptureCount += month.ImageCaptureCount;
                            SemiYears[0].SemiYearlyScreenTime += month.ScreenTime;
                            SemiYears[0].SemiYearlyLowTime += month.LowTime;
                            SemiYears[0].SemiYearlyHighTime += month.HighTime;
                            SemiYears[0].SemiYearlyExtraHighTime += month.ExtraHighTime;
                            SemiYears[0].SemiYearlyTppaTime += month.TppaTime;
                            SemiYears[0].SemiYearlyRprTppaTime += month.RprTppaTime;
                            

                            SemiYears[0].SemiYearlyPrimeInstrumentCount = int.TryParse(month.PrimeInstrumentCount, out result) ?
                                (result + int.Parse(SemiYears[0].SemiYearlyPrimeInstrumentCount)).ToString() : month.PrimeInstrumentCount;
                            SemiYears[0].SemiYearlyProbeAlignmentCount = int.TryParse(month.ProbeAlignmentCount, out result) ?
                                (result + int.Parse(SemiYears[0].SemiYearlyProbeAlignmentCount)).ToString() : month.ProbeAlignmentCount;
                            SemiYears[0].SemiYearlyReaderAlignmentCount = int.TryParse(month.ReaderAlignmentCount, out result) ?
                                (result + int.Parse(SemiYears[0].SemiYearlyReaderAlignmentCount)).ToString() : month.ReaderAlignmentCount;
                            SemiYears[0].SemiYearlyReaderCalibrationCount = int.TryParse(month.ReaderCalibrationCount, out result) ?
                                (result + int.Parse(SemiYears[0].SemiYearlyReaderCalibrationCount)).ToString() : month.ReaderCalibrationCount;
                            SemiYears[0].SemiYearlyLuminescenceReaderCalCount = int.TryParse(month.LuminescenceReaderCalCount, out result) ?
                                (result + int.Parse(SemiYears[0].SemiYearlyLuminescenceReaderCalCount)).ToString() : month.LuminescenceReaderCalCount;
                            SemiYears[0].SemiYearlyWashPumpCalibrationCount = int.TryParse(month.WashPumpCalibrationCount, out result) ?
                                (result + int.Parse(SemiYears[0].SemiYearlyWashPumpCalibrationCount)).ToString() : month.WashPumpCalibrationCount;
                            SemiYears[0].SemiYearlyCameraAlignmentCount = int.TryParse(month.CameraAlignmentCount, out result) ?
                                (result + int.Parse(SemiYears[0].SemiYearlyCameraAlignmentCount)).ToString() : month.CameraAlignmentCount;
                            SemiYears[0].SemiYearlyCameraFocusCount = int.TryParse(month.CameraFocusCount, out result) ?
                                (result + int.Parse(SemiYears[0].SemiYearlyCameraFocusCount)).ToString() : month.CameraFocusCount;
                            SemiYears[0].SemiYearlyOpenSoftwareCount = int.TryParse(month.OpenSoftwareCount, out result) ?
                                (result + int.Parse(SemiYears[0].SemiYearlyOpenSoftwareCount)).ToString() : month.OpenSoftwareCount;
                            SemiYears[0].SemiYearlyCloseSoftwareCount = int.TryParse(month.CloseSoftwareCount, out result) ?
                                (result + int.Parse(SemiYears[0].SemiYearlyCloseSoftwareCount)).ToString() : month.CloseSoftwareCount;
                            SemiYears[0].SemiYearlyNonSoftwareRelatedCount = int.TryParse(month.NonSoftwareRelatedCount, out result) ?
                                (result + int.Parse(SemiYears[0].SemiYearlyNonSoftwareRelatedCount)).ToString() : month.NonSoftwareRelatedCount;

                            SemiYears[0].SemiYearlyTotalWorklistRun += month.TotalWorklistRun;
                            SemiYears[0].SemiYearlyCompletedWorklistRun += month.CompletedWorklistRun;
                            SemiYears[0].SemiYearlyStoppedWorklistRun += month.StoppedWorklistRun;
                            SemiYears[0].SemiYearlyScreenCount += month.ScreenCount;
                            SemiYears[0].SemiYearlyLowCount += month.LowCount;
                            SemiYears[0].SemiYearlyHighCount += month.HighCount;
                            SemiYears[0].SemiYearlyExtraHighCount += month.ExtraHighCount;
                            SemiYears[0].SemiYearlyTppaCount += month.TppaCount;
                            SemiYears[0].SemiYearlyRprTppaCount += month.RprTppaCount;
                        }
                        else if (month.Month.Equals("Jul") || month.Month.Equals("Aug") || month.Month.Equals("Sep") ||
                                 month.Month.Equals("Oct") || month.Month.Equals("Nov") || month.Month.Equals("Dec"))
                        {
                            SemiYears[1].SemiYearlyFluidTransferAction += month.FluidTransferAction;
                            SemiYears[1].SemiYearlyWashAction += month.WashAction;
                            SemiYears[1].SemiYearlyShakerAction += month.ShakerAction;
                            SemiYears[1].SemiYearlyIncubateAction += month.IncubateAction;
                            SemiYears[1].SemiYearlyHeatAction += month.HeatAction;
                            SemiYears[1].SemiYearlyReadWellAction += month.ReadWellAction;
                            SemiYears[1].SemiYearlyPrimeAction += month.PrimeAction;
                            SemiYears[1].SemiYearlyProbeWashAction += month.ProbeWashAction;
                            SemiYears[1].SemiYearlyTotalRunTime += month.TotalRunTime;
                            SemiYears[1].SemiYearlyImageCaptureCount += month.ImageCaptureCount;
                            SemiYears[1].SemiYearlyScreenTime += month.ScreenTime;
                            SemiYears[1].SemiYearlyLowTime += month.LowTime;
                            SemiYears[1].SemiYearlyHighTime += month.HighTime;
                            SemiYears[1].SemiYearlyExtraHighTime += month.ExtraHighTime;
                            SemiYears[1].SemiYearlyTppaTime += month.TppaTime;
                            SemiYears[1].SemiYearlyRprTppaTime += month.RprTppaTime;

                            SemiYears[1].SemiYearlyPrimeInstrumentCount = int.TryParse(month.PrimeInstrumentCount, out result) ?
                                (result + int.Parse(SemiYears[1].SemiYearlyPrimeInstrumentCount)).ToString() : month.PrimeInstrumentCount;
                            SemiYears[1].SemiYearlyProbeAlignmentCount = int.TryParse(month.ProbeAlignmentCount, out result) ?
                                (result + int.Parse(SemiYears[1].SemiYearlyProbeAlignmentCount)).ToString() : month.ProbeAlignmentCount;
                            SemiYears[1].SemiYearlyReaderAlignmentCount = int.TryParse(month.ReaderAlignmentCount, out result) ?
                                (result + int.Parse(SemiYears[1].SemiYearlyReaderAlignmentCount)).ToString() : month.ReaderAlignmentCount;
                            SemiYears[1].SemiYearlyReaderCalibrationCount = int.TryParse(month.ReaderCalibrationCount, out result) ?
                                (result + int.Parse(SemiYears[1].SemiYearlyReaderCalibrationCount)).ToString() : month.ReaderCalibrationCount;
                            SemiYears[1].SemiYearlyLuminescenceReaderCalCount = int.TryParse(month.LuminescenceReaderCalCount, out result) ?
                                (result + int.Parse(SemiYears[1].SemiYearlyLuminescenceReaderCalCount)).ToString() : month.LuminescenceReaderCalCount;
                            SemiYears[1].SemiYearlyWashPumpCalibrationCount = int.TryParse(month.WashPumpCalibrationCount, out result) ?
                                (result + int.Parse(SemiYears[1].SemiYearlyWashPumpCalibrationCount)).ToString() : month.WashPumpCalibrationCount;
                            SemiYears[1].SemiYearlyCameraAlignmentCount = int.TryParse(month.CameraAlignmentCount, out result) ?
                                (result + int.Parse(SemiYears[1].SemiYearlyCameraAlignmentCount)).ToString() : month.CameraAlignmentCount;
                            SemiYears[1].SemiYearlyCameraFocusCount = int.TryParse(month.CameraFocusCount, out result) ?
                                (result + int.Parse(SemiYears[1].SemiYearlyCameraFocusCount)).ToString() : month.CameraFocusCount;
                            SemiYears[1].SemiYearlyOpenSoftwareCount = int.TryParse(month.OpenSoftwareCount, out result) ?
                                (result + int.Parse(SemiYears[1].SemiYearlyOpenSoftwareCount)).ToString() : month.OpenSoftwareCount;
                            SemiYears[1].SemiYearlyCloseSoftwareCount = int.TryParse(month.CloseSoftwareCount, out result) ?
                                (result + int.Parse(SemiYears[1].SemiYearlyCloseSoftwareCount)).ToString() : month.CloseSoftwareCount;
                            SemiYears[1].SemiYearlyNonSoftwareRelatedCount = int.TryParse(month.NonSoftwareRelatedCount, out result) ?
                                (result + int.Parse(SemiYears[1].SemiYearlyNonSoftwareRelatedCount)).ToString() : month.NonSoftwareRelatedCount;

                            SemiYears[1].SemiYearlyTotalWorklistRun += month.TotalWorklistRun;
                            SemiYears[1].SemiYearlyCompletedWorklistRun += month.CompletedWorklistRun;
                            SemiYears[1].SemiYearlyStoppedWorklistRun += month.StoppedWorklistRun;
                            SemiYears[1].SemiYearlyScreenCount += month.ScreenCount;
                            SemiYears[1].SemiYearlyLowCount += month.LowCount;
                            SemiYears[1].SemiYearlyHighCount += month.HighCount;
                            SemiYears[1].SemiYearlyExtraHighCount += month.ExtraHighCount;
                            SemiYears[1].SemiYearlyTppaCount += month.TppaCount;
                            SemiYears[1].SemiYearlyRprTppaCount += month.RprTppaCount;
                        }
                    }
                }
            });
        }
    }
}
