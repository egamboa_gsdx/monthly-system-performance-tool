﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;

namespace GSDSystemPerformanceTool.ViewModel
{
    public class RecentLogViewModel
    {
        public RecentLogViewModel(string header)
        {
            Header = header;
            OpenRecentLogCommand = new RelayCommand<string>(OpenRecentLogExecute);
            this.RecentLogsList = new List<RecentLogViewModel>();
        }

        private void OpenRecentLogExecute(string paths)
        {
            var mainVM = SimpleIoc.Default.GetInstance<MainWindowViewModel>();
            mainVM.OpenRecentCommand?.Execute(paths);
        }
        public string Header { get; set; }
        public ICommand OpenRecentLogCommand { get; set; }
        public IList<RecentLogViewModel> RecentLogsList { get; private set; }
    }
}
