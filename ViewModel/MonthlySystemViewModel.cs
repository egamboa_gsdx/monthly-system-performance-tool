﻿using GSDSystemPerformanceTool.Model;
using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows;
using System.Windows.Input;
using PropertyChanged;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using Ookii.Dialogs.Wpf;
using GSDSystemPerformanceTool.Utilities;
using GSD.TestLab.DataCollection;


namespace GSDSystemPerformanceTool.ViewModel
{
    [AddINotifyPropertyChangedInterface]
    public class MonthlySystemViewModel : IPageViewModel
    {
        private string _month;
        private DataCollector _dataCollector;

        public MonthlySystemViewModel()
        {
            Name = "Monthly Performance";
            MonthlyLogs = new ObservableCollection<MonthlySystemModel>();
            _dataCollector = new DataCollector();

            SelectedMonthCommand = new RelayCommand(SelectedMonthExecute);
            SelectedYearCommand = new RelayCommand(SelectedYearExecute);
            SelectFolderCommand = new RelayCommand(SelectFolderExecute);

            string initWorklistFolder = Directory.Exists(@"C:\Users\Public\Documents\Storm\Status Log Files")
                ? @"C:\Users\Public\Documents\Storm\Status Log Files"
                : @"C:\Users\Public\Documents\AIX1000\Status Log Files";
            string initDBFile = File.Exists(@"C:\ProgramData\Gold Standard Diagnostics\AIX1000\DataFolder\DataBase\DataBaseFile.sqlite3")
                ? @"C:\ProgramData\Gold Standard Diagnostics\Storm\DataFolder\DataBase\StormDB.sqlite"
                : @"C:\ProgramData\Gold Standard Diagnostics\AIX1000\DataFolder\DataBase\DataBaseFile.sqlite3";
            string initMessageLogs = Directory.Exists(@"C:\Users\Public\Documents\Storm\Message Log Files")
                ? @"C:\Users\Public\Documents\Storm\Message Log Files"
                : @"C:\Users\Public\Documents\AIX1000\Message Log Files";

            ApplicationName = DataCollection.GetApplicationName(initWorklistFolder);
            InstrumentName = DataCollection.GetInstrumentName(initWorklistFolder);
            ComputerName = DataCollection.GetComputerName(initWorklistFolder);

            

            AddMonths(initWorklistFolder, initDBFile);

            SelectedMonthNum = DateTime.Now.Month;

            ProgressIsVisible = "Hidden";
            Opacity = "1";
        }

        private void SelectedMonthExecute()
        {
            GetMonth();
        }
        private void SelectedYearExecute()
        {
            GetMonth();
            var yearlyVM = SimpleIoc.Default.GetInstance<YearlySelectorViewModel>();
            yearlyVM.SelectedYear = SelectedYear;
            yearlyVM.UpdateYearCommand?.Execute(yearlyVM.SelectedYear);
        }

        private async void GetMonth()
        {
            ProgressIsVisible = "Visible";
            Opacity = "0.35";
            if (string.IsNullOrEmpty(Month))
            {
                Month = DateTime.Now.ToString("MMM");
            }

            await Task.Run(() =>
            {
                SelectedMonth = MonthlyLogs.FirstOrDefault(x => x.Month.Equals(Month) && x.Year.Equals(SelectedYear));

                if (SelectedMonth == null)
                {
                    SelectedMonth = new MonthlySystemModel(Months.GetValueOrDefault(Month), Month, SelectedYear)
                    {
                        TotalRunTime = TimeSpan.FromSeconds(0),
                        TotalWorklistRun = 0,
                        FluidTransferAction = TimeSpan.FromSeconds(0),
                        WashAction = TimeSpan.FromSeconds(0),
                        PrimeAction = TimeSpan.FromSeconds(0),
                        ShakerAction = TimeSpan.FromSeconds(0),
                        HeatAction = TimeSpan.FromSeconds(0),
                        IncubateAction = TimeSpan.FromSeconds(0),
                        ReadWellAction = TimeSpan.FromSeconds(0),
                        ProbeAlignmentCount = "0",
                        CameraAlignmentCount = "0",
                        ReaderAlignmentCount = "0"
                    };
                }
            });
            ProgressIsVisible = "Hidden";
            Opacity = "1";
        }

        public ObservableCollection<MonthlySystemModel> MonthlyLogs { get; set; }
        public MonthlySystemModel SelectedMonth { get; set; }
        public int SelectedMonthNum { get; set; }
        public string SelectedYear { get; set; }
        public string ApplicationName { get; set; }
        public string InstrumentName { get; set; }
        public string ComputerName { get; set; }
        public ICommand SelectedMonthCommand { get; set; }
        public ICommand SelectedYearCommand { get; set; }
        public ICommand SelectFolderCommand { get; set; }
        public bool IsSelected { get; set; }
        public string ProgressIsVisible { get; set; }
        public string Opacity { get; set; }
        public string Name { get; set; }
        public List<ISubPageViewModel> Views { get; set; }
        public string Month 
        {
            get { return _month; }
            set { _month = value; }
        }
        public Dictionary<string, int> Months
        {
            get
            {
                return new Dictionary<string, int>()
                {
                    {"Jan", 31 },
                    {"Feb", 28 }, 
                    {"Mar", 31 }, 
                    {"Apr", 30 }, 
                    {"May", 31 }, 
                    {"Jun", 30 },
                    {"Jul", 31 }, 
                    {"Aug", 31 }, 
                    {"Sep", 30 }, 
                    {"Oct", 31 }, 
                    {"Nov", 30 },
                    {"Dec", 31 }
                };
            }
        }
      
        public List<string> Years
        {
            get
            {
                List<string> allyears = new List<string>();

                var currentyear = DateTime.Now.Year;

                for (int i = 2019; i < currentyear + 1; i++)
                {
                    allyears.Add(i.ToString());
                }

                return allyears;
            }
        }

        public async void AddMonths(string statuslogDir, string databaseDir)
        {
            ProgressIsVisible = "Visible";
            Opacity = "0.35";
            var yvm = SimpleIoc.Default.GetInstance<YearlySelectorViewModel>();
            await Task.Run(() =>
            {
                
                if (Directory.Exists(statuslogDir))
                {
                    string[] folders = Directory.GetDirectories(statuslogDir);

                    foreach (var folder in folders)
                    {
                        string year = folder.Substring(folder.LastIndexOf("\\") + 1, 4);
                        string month = DateTime.ParseExact(folder.Substring(folder.IndexOf("_") + 1), "MM", CultureInfo.CurrentCulture,
                            DateTimeStyles.None).ToString("MMM");

                        if (int.Parse(year) >= 2019)
                        {
                            MonthlyLogs.Add(new MonthlySystemModel(Months.GetValueOrDefault(month), month, year, statuslogDir));
                        }
                    }   
                }

                var currentyear = DateTime.Now.Year;

                for (int i = 2019; i <= currentyear; i++)
                {
                    foreach (var month in Months)
                    {
                        if (!MonthlyLogs.Any(x => x.Month.Equals(month.Key) && x.Year == i.ToString()))
                        {
                            MonthlyLogs.Add(new MonthlySystemModel(month.Value, month.Key, i.ToString(), statuslogDir, databaseDir, ApplicationName));
                        }
                    }
                }

                MonthlyLogs = new ObservableCollection<MonthlySystemModel>(MonthlyLogs.OrderBy(x => int.Parse(x.Year)).ThenBy(x => x.MonthNum));

                _dataCollector.InitDataCollection(MonthlyLogs, statuslogDir);
                _dataCollector.GetSQLData(MonthlyLogs, InstrumentName, databaseDir, ApplicationName);
            });
            

            Month = DateTime.Now.ToString("MMM");
            SelectedYear = DateTime.Now.ToString("yyyy");
            GetMonth();
            ProgressIsVisible = "Hidden";
            Opacity = "1";
            yvm.ApplicationName = ApplicationName;
            yvm.InstrumentName = InstrumentName;
            yvm.ComputerName = ComputerName;
            yvm.SelectedYear = SelectedYear;
            yvm.UpdateYearCommand?.Execute(SelectedYear);
            
        }

        private void SelectFolderExecute()
        {
            var dialog = new VistaFolderBrowserDialog();
            var databaseDialog = new VistaOpenFileDialog();
            dialog.Description = "Please Select a Folder";

            if ((bool)dialog.ShowDialog())
            {
                var yearlyview = SimpleIoc.Default.GetInstance<YearlySelectorViewModel>();
                string[] folders = Directory.GetDirectories(dialog.SelectedPath);
                if (!folders.Any())
                {
                    MessageBox.Show("Selected folder does not contain any worklist logs...", "Incorrect Folder Selected", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //ApplicationName = string.Empty;
                    //InstrumentName = string.Empty;
                    //ComputerName = string.Empty;
                    //GetMonth();
                    //yearlyview.ApplicationName = ApplicationName;
                    //yearlyview.InstrumentName = InstrumentName;
                    //yearlyview.ComputerName = ComputerName;
                    return;
                }

                MessageBoxResult mbr = MessageBox.Show("Is there an associated database available?", "Database", MessageBoxButton.YesNoCancel, MessageBoxImage.Information);
                if (mbr == MessageBoxResult.Yes)
                {
                    databaseDialog.ShowDialog();
                    if (!File.Exists(databaseDialog.FileName))
                    {
                        MessageBox.Show("File does not exist.", "Invalid File", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    
                    MonthlyLogs = new ObservableCollection<MonthlySystemModel>();

                    ApplicationName = DataCollection.GetApplicationName(dialog.SelectedPath);
                    InstrumentName = DataCollection.GetInstrumentName(dialog.SelectedPath);
                    ComputerName = DataCollection.GetComputerName(dialog.SelectedPath);

                    AddMonths(dialog.SelectedPath, databaseDialog.FileName);
                }
                else if (mbr == MessageBoxResult.No)
                {
                    MonthlyLogs = new ObservableCollection<MonthlySystemModel>();

                    ApplicationName = DataCollection.GetApplicationName(dialog.SelectedPath);
                    InstrumentName = DataCollection.GetInstrumentName(dialog.SelectedPath);
                    ComputerName = DataCollection.GetComputerName(dialog.SelectedPath);

                    AddMonths(dialog.SelectedPath, "NoDb");
                }

                string db = !string.IsNullOrEmpty(databaseDialog.FileName) ? databaseDialog.FileName : "NoDb";
                FileHistoryHelper.AddFileHistory($"{dialog.SelectedPath}, {db}");

                var mainVM = SimpleIoc.Default.GetInstance<MainWindowViewModel>();
                if (mainVM != null)
                {
                    var filehistory = FileHistoryHelper.GetFileHistory();
                    var newList = new List<RecentLogViewModel>();

                    foreach (var file in filehistory)
                    {
                        newList.Add(new RecentLogViewModel(file.FullInstrumentPath));
                    }

                    mainVM.RecentLogsList = new ObservableCollection<RecentLogViewModel>(newList);
                }
            }
        }
    }
}
