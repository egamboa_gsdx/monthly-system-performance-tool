﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using PropertyChanged;
using GSDSystemPerformanceTool.View;

namespace GSDSystemPerformanceTool.ViewModel
{
    [AddINotifyPropertyChangedInterface]
    public class DataCollectionSchedulerViewModel : Window
    {
        public DataCollectionSchedulerViewModel()
        {
            OkCommand = new RelayCommand<Window>(OkExecute);
            CancelCommand = new RelayCommand<Window>(CancelExecute);
        }

        private void OkExecute(Window win)
        {
            MessageBox.Show(SetTime + ". This actually does anything");
            MessageBox.Show("...for now");
            win.DialogResult = true;
        }
        private void CancelExecute(Window win)
        {
            win.DialogResult = false;
        }

        public string SetTime { get; set; }
        public ICommand OkCommand { get; set; }
        public ICommand CancelCommand { get; set; }
    }
}
