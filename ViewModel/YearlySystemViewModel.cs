﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using PropertyChanged;
using MaterialDesignThemes.Wpf;
using GSDSystemPerformanceTool.Model;

namespace GSDSystemPerformanceTool.ViewModel
{
    [AddINotifyPropertyChangedInterface]
    public class YearlySystemViewModel : ISubPageViewModel
    {
        private MonthlySystemViewModel _monthlySystem;
        public YearlySystemViewModel()
        {
            Name = "Year-to-Date";
            Icon = PackIconKind.MusicNoteWhole;
            Year = new YearlySystemModel("Yearly Accumulation");

            GetYearlyData(DateTime.Now.Year.ToString());
        }
        public bool IsSelected { get;set; }
        public string SelectedYear { get; set; }
        public string ProgressIsVisible { get; set; }
        public string Name { get; set; }
        public PackIconKind Icon { get; set; }
        public YearlySystemModel Year { get; set; }

        public async void GetYearlyData(string year)
        {
            int result;
            await Task.Run(() =>
            {
                SelectedYear = year;

                _monthlySystem = SimpleIoc.Default.GetInstance<MonthlySystemViewModel>();

                Year.YearlyFluidTransferAction = TimeSpan.Zero;
                Year.YearlyWashAction = TimeSpan.Zero;
                Year.YearlyShakerAction = TimeSpan.Zero;
                Year.YearlyIncubateAction = TimeSpan.Zero;
                Year.YearlyHeatAction = TimeSpan.Zero;
                Year.YearlyReadWellAction = TimeSpan.Zero;
                Year.YearlyPrimeAction = TimeSpan.Zero;
                Year.YearlyProbeWashAction = TimeSpan.Zero;
                Year.YearlyTotalRunTime = TimeSpan.Zero;
                Year.YearlyScreenTime = TimeSpan.Zero;
                Year.YearlyLowTime = TimeSpan.Zero;
                Year.YearlyHighTime = TimeSpan.Zero;
                Year.YearlyExtraHighTime = TimeSpan.Zero;
                Year.YearlyTppaTime = TimeSpan.Zero;
                Year.YearlyRprTppaTime = TimeSpan.Zero;

                Year.YearlyPrimeInstrumentCount = "0";
                Year.YearlyProbeAlignmentCount = "0";
                Year.YearlyReaderAlignmentCount = "0";
                Year.YearlyReaderCalibrationCount = "0";
                Year.YearlyLuminescenceReaderCalCount = "0";
                Year.YearlyWashPumpCalibrationCount = "0";
                Year.YearlyCameraAlignmentCount = "0";
                Year.YearlyCameraFocusCount = "0";
                Year.YearlyOpenSoftwareCount = "0";
                Year.YearlyCloseSoftwareCount = "0";
                Year.YearlyNonSoftwareRelatedCount = "0";

                Year.YearlyTotalWorklistRun = 0;
                Year.YearlyCompletedWorklistRun = 0;
                Year.YearlyStoppedWorklistRun = 0;
                Year.YearlyImageCaptureCount = 0;
                Year.YearlyScreenCount = 0;
                Year.YearlyLowCount = 0;
                Year.YearlyHighCount = 0;
                Year.YearlyExtraHighCount = 0;
                Year.YearlyTppaCount = 0;
                Year.YearlyRprTppaCount = 0;


                foreach (var month in _monthlySystem.MonthlyLogs)
                {
                    if (month.Year.Equals(SelectedYear))
                    {
                        Year.YearlyFluidTransferAction += month.FluidTransferAction;
                        Year.YearlyWashAction += month.WashAction;
                        Year.YearlyShakerAction += month.ShakerAction;
                        Year.YearlyIncubateAction += month.IncubateAction;
                        Year.YearlyHeatAction += month.HeatAction;
                        Year.YearlyReadWellAction += month.ReadWellAction;
                        Year.YearlyPrimeAction += month.PrimeAction;
                        Year.YearlyProbeWashAction += month.ProbeWashAction;
                        Year.YearlyTotalRunTime += month.TotalRunTime;
                        Year.YearlyImageCaptureCount += month.ImageCaptureCount;
                        Year.YearlyScreenTime += month.ScreenTime;
                        Year.YearlyLowTime += month.LowTime;
                        Year.YearlyHighTime += month.HighTime;
                        Year.YearlyExtraHighTime += month.ExtraHighTime;
                        Year.YearlyTppaTime += month.TppaTime;
                        Year.YearlyRprTppaTime += month.RprTppaTime;


                        Year.YearlyPrimeInstrumentCount = int.TryParse(month.PrimeInstrumentCount, out result) ?
                                (result + int.Parse(Year.YearlyPrimeInstrumentCount)).ToString() : month.PrimeInstrumentCount;
                        Year.YearlyProbeAlignmentCount = int.TryParse(month.ProbeAlignmentCount, out result) ?
                            (result + int.Parse(Year.YearlyProbeAlignmentCount)).ToString() : month.ProbeAlignmentCount;
                        Year.YearlyReaderAlignmentCount = int.TryParse(month.ReaderAlignmentCount, out result) ?
                            (result + int.Parse(Year.YearlyReaderAlignmentCount)).ToString() : month.ReaderAlignmentCount;
                        Year.YearlyReaderCalibrationCount = int.TryParse(month.ReaderCalibrationCount, out result) ?
                            (result + int.Parse(Year.YearlyReaderCalibrationCount)).ToString() : month.ReaderCalibrationCount;
                        Year.YearlyLuminescenceReaderCalCount = int.TryParse(month.LuminescenceReaderCalCount, out result) ?
                            (result + int.Parse(Year.YearlyLuminescenceReaderCalCount)).ToString() : month.LuminescenceReaderCalCount;
                        Year.YearlyWashPumpCalibrationCount = int.TryParse(month.WashPumpCalibrationCount, out result) ?
                            (result + int.Parse(Year.YearlyWashPumpCalibrationCount)).ToString() : month.WashPumpCalibrationCount;
                        Year.YearlyCameraAlignmentCount = int.TryParse(month.CameraAlignmentCount, out result) ?
                            (result + int.Parse(Year.YearlyCameraAlignmentCount)).ToString() : month.CameraAlignmentCount;
                        Year.YearlyCameraFocusCount = int.TryParse(month.CameraFocusCount, out result) ?
                            (result + int.Parse(Year.YearlyCameraFocusCount)).ToString() : month.CameraFocusCount;
                        Year.YearlyOpenSoftwareCount = int.TryParse(month.OpenSoftwareCount, out result) ?
                            (result + int.Parse(Year.YearlyOpenSoftwareCount)).ToString() : month.OpenSoftwareCount;
                        Year.YearlyCloseSoftwareCount = int.TryParse(month.CloseSoftwareCount, out result) ?
                            (result + int.Parse(Year.YearlyCloseSoftwareCount)).ToString() : month.CloseSoftwareCount;
                        Year.YearlyNonSoftwareRelatedCount = int.TryParse(month.NonSoftwareRelatedCount, out result) ?
                            (result + int.Parse(Year.YearlyNonSoftwareRelatedCount)).ToString() : month.NonSoftwareRelatedCount;

                        Year.YearlyTotalWorklistRun += month.TotalWorklistRun;
                        Year.YearlyCompletedWorklistRun += month.CompletedWorklistRun;
                        Year.YearlyStoppedWorklistRun += month.StoppedWorklistRun;
                        Year.YearlyScreenCount += month.ScreenCount;
                        Year.YearlyLowCount += month.LowCount;
                        Year.YearlyHighCount += month.HighCount;
                        Year.YearlyExtraHighCount += month.ExtraHighCount;
                        Year.YearlyTppaCount += month.TppaCount;
                        Year.YearlyRprTppaCount += month.RprTppaCount;
                    }
                }
            });
        }

    }
}
