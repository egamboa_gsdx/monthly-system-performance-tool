﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PropertyChanged;
using GalaSoft.MvvmLight.Ioc;
using MaterialDesignThemes.Wpf;

using GSDSystemPerformanceTool.Model;

namespace GSDSystemPerformanceTool.ViewModel
{
    [AddINotifyPropertyChangedInterface]
    public class QuarterlySystemViewModel : ISubPageViewModel
    {
        private MonthlySystemViewModel _monthlyVM;
        public QuarterlySystemViewModel()
        {
            Name = "Quarter-to-Date";
            Icon = PackIconKind.MusicNoteQuarter;
            //_monthlyVM = SimpleIoc.Default.GetInstance<MonthlySystemViewModel>();
            Quarters = new ObservableCollection<QuarterlySystemModel>() 
            {
                new QuarterlySystemModel("First Quarter", 0),
                new QuarterlySystemModel("Second Quarter", 1),
                new QuarterlySystemModel("Third Quarter", 2),
                new QuarterlySystemModel("Fourth Quarter", 3)
            };
            SelectedYear = DateTime.Now.Year.ToString();
            GetQuarterlyData(SelectedYear);
        }
        public bool IsSelected { get; set; }
        public string SelectedYear { get; set; }
        public string ProgressIsVisible { get; set; }
        public string Name { get; set; }
        public PackIconKind Icon { get; set; }
        public ObservableCollection<QuarterlySystemModel> Quarters { get; set; }

        public async void GetQuarterlyData(string selectedYear)
        {
            await Task.Run(() =>
            {
                SelectedYear = selectedYear;
                _monthlyVM = SimpleIoc.Default.GetInstance<MonthlySystemViewModel>();

                // reset all quarters
                foreach (var quarter in Quarters)
                {
                    quarter.QuarterlyFluidTransferAction = TimeSpan.Zero;
                    quarter.QuarterlyWashAction = TimeSpan.Zero;
                    quarter.QuarterlyShakerAction = TimeSpan.Zero;
                    quarter.QuarterlyIncubateAction = TimeSpan.Zero;
                    quarter.QuarterlyHeatAction = TimeSpan.Zero;
                    quarter.QuarterlyReadWellAction = TimeSpan.Zero;
                    quarter.QuarterlyPrimeAction = TimeSpan.Zero;
                    quarter.QuarterlyProbeWashAction = TimeSpan.Zero;
                    quarter.QuarterlyTotalRunTime = TimeSpan.Zero;
                    quarter.QuarterlyScreenTime = TimeSpan.Zero;
                    quarter.QuarterlyLowTime = TimeSpan.Zero;
                    quarter.QuarterlyHighTime = TimeSpan.Zero;
                    quarter.QuarterlyExtraHighTime = TimeSpan.Zero;
                    quarter.QuarterlyTppaTime = TimeSpan.Zero;
                    quarter.QuarterlyRprTppaTime = TimeSpan.Zero;


                    quarter.QuarterlyPrimeInstrumentCount = "0";
                    quarter.QuarterlyProbeAlignmentCount = "0";
                    quarter.QuarterlyReaderAlignmentCount = "0";
                    quarter.QuarterlyReaderCalibrationCount = "0";
                    quarter.QuarterlyLuminescenceReaderCalCount = "0";
                    quarter.QuarterlyWashPumpCalibrationCount = "0";
                    quarter.QuarterlyCameraAlignmentCount = "0";
                    quarter.QuarterlyCameraFocusCount = "0";
                    quarter.QuarterlyOpenSoftwareCount = "0";
                    quarter.QuarterlyCloseSoftwareCount = "0";
                    quarter.QuarterlyNonSoftwareRelatedCount = "0";

                    quarter.QuarterlyTotalWorklistRun = 0;
                    quarter.QuarterlyCompletedWorklistRun = 0;
                    quarter.QuarterlyStoppedWorklistRun = 0;
                    quarter.QuarterlyImageCaptureCount = 0;
                    quarter.QuarterlyScreenCount = 0;
                    quarter.QuarterlyLowCount = 0;
                    quarter.QuarterlyHighCount = 0;
                    quarter.QuarterlyExtraHighCount = 0;
                    quarter.QuarterlyTppaCount = 0;
                    quarter.QuarterlyRprTppaCount = 0;
                }

                int result;
                foreach (var month in _monthlyVM.MonthlyLogs)
                {
                    if (month.Year.Equals(SelectedYear))
                    {
                        if (month.Month.Equals("Jan") || month.Month.Equals("Feb") || month.Month.Equals("Mar"))
                        {
                            Quarters[0].QuarterlyFluidTransferAction += month.FluidTransferAction;
                            Quarters[0].QuarterlyWashAction += month.WashAction;
                            Quarters[0].QuarterlyShakerAction += month.ShakerAction;
                            Quarters[0].QuarterlyIncubateAction += month.IncubateAction;
                            Quarters[0].QuarterlyHeatAction += month.HeatAction;
                            Quarters[0].QuarterlyReadWellAction += month.ReadWellAction;
                            Quarters[0].QuarterlyPrimeAction += month.PrimeAction;
                            Quarters[0].QuarterlyProbeWashAction += month.ProbeWashAction;
                            Quarters[0].QuarterlyTotalRunTime += month.TotalRunTime;
                            Quarters[0].QuarterlyImageCaptureCount += month.ImageCaptureCount;
                            Quarters[0].QuarterlyScreenTime += month.ScreenTime;
                            Quarters[0].QuarterlyLowTime += month.LowTime;
                            Quarters[0].QuarterlyHighTime += month.HighTime;
                            Quarters[0].QuarterlyExtraHighTime += month.ExtraHighTime;
                            Quarters[0].QuarterlyTppaTime += month.TppaTime;
                            Quarters[0].QuarterlyRprTppaTime += month.RprTppaTime;
                            Quarters[0].QuarterlyScreenCount += month.ScreenCount;
                            Quarters[0].QuarterlyLowCount += month.LowCount;
                            Quarters[0].QuarterlyHighCount += month.HighCount;
                            Quarters[0].QuarterlyExtraHighCount += month.ExtraHighCount;
                            Quarters[0].QuarterlyTppaCount += month.TppaCount;
                            Quarters[0].QuarterlyRprTppaCount += month.RprTppaCount;

                            Quarters[0].QuarterlyPrimeInstrumentCount = int.TryParse(month.PrimeInstrumentCount, out result) ?
                                (result + int.Parse(Quarters[0].QuarterlyPrimeInstrumentCount)).ToString() : month.PrimeInstrumentCount;
                            Quarters[0].QuarterlyProbeAlignmentCount = int.TryParse(month.ProbeAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[0].QuarterlyProbeAlignmentCount)).ToString() : month.ProbeAlignmentCount;
                            Quarters[0].QuarterlyReaderAlignmentCount = int.TryParse(month.ReaderAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[0].QuarterlyReaderAlignmentCount)).ToString() : month.ReaderAlignmentCount;
                            Quarters[0].QuarterlyReaderCalibrationCount = int.TryParse(month.ReaderCalibrationCount, out result) ?
                                (result + int.Parse(Quarters[0].QuarterlyReaderCalibrationCount)).ToString() : month.ReaderCalibrationCount;
                            Quarters[0].QuarterlyLuminescenceReaderCalCount = int.TryParse(month.LuminescenceReaderCalCount, out result) ?
                                (result + int.Parse(Quarters[0].QuarterlyLuminescenceReaderCalCount)).ToString() : month.LuminescenceReaderCalCount;
                            Quarters[0].QuarterlyWashPumpCalibrationCount = int.TryParse(month.WashPumpCalibrationCount, out result) ?
                                (result + int.Parse(Quarters[0].QuarterlyWashPumpCalibrationCount)).ToString() : month.WashPumpCalibrationCount;
                            Quarters[0].QuarterlyCameraAlignmentCount = int.TryParse(month.CameraAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[0].QuarterlyCameraAlignmentCount)).ToString() : month.CameraAlignmentCount;
                            Quarters[0].QuarterlyCameraFocusCount = int.TryParse(month.CameraFocusCount, out result) ?
                                (result + int.Parse(Quarters[0].QuarterlyCameraFocusCount)).ToString() : month.CameraFocusCount;
                            Quarters[0].QuarterlyOpenSoftwareCount = int.TryParse(month.OpenSoftwareCount, out result) ?
                                (result + int.Parse(Quarters[0].QuarterlyOpenSoftwareCount)).ToString() : month.OpenSoftwareCount;
                            Quarters[0].QuarterlyCloseSoftwareCount = int.TryParse(month.CloseSoftwareCount, out result) ?
                                (result + int.Parse(Quarters[0].QuarterlyCloseSoftwareCount)).ToString() : month.CloseSoftwareCount;
                            Quarters[0].QuarterlyNonSoftwareRelatedCount = int.TryParse(month.NonSoftwareRelatedCount, out result) ?
                                (result + int.Parse(Quarters[0].QuarterlyNonSoftwareRelatedCount)).ToString() : month.NonSoftwareRelatedCount;

                            Quarters[0].QuarterlyTotalWorklistRun += month.TotalWorklistRun;
                            Quarters[0].QuarterlyCompletedWorklistRun += month.CompletedWorklistRun;
                            Quarters[0].QuarterlyStoppedWorklistRun += month.StoppedWorklistRun;
                        }
                        else if (month.Month.Equals("Apr") || month.Month.Equals("May") || month.Month.Equals("Jun"))
                        {
                            Quarters[1].QuarterlyFluidTransferAction += month.FluidTransferAction;
                            Quarters[1].QuarterlyWashAction += month.WashAction;
                            Quarters[1].QuarterlyShakerAction += month.ShakerAction;
                            Quarters[1].QuarterlyIncubateAction += month.IncubateAction;
                            Quarters[1].QuarterlyHeatAction += month.HeatAction;
                            Quarters[1].QuarterlyReadWellAction += month.ReadWellAction;
                            Quarters[1].QuarterlyPrimeAction += month.PrimeAction;
                            Quarters[1].QuarterlyProbeWashAction += month.ProbeWashAction;
                            Quarters[1].QuarterlyTotalRunTime += month.TotalRunTime;
                            Quarters[1].QuarterlyImageCaptureCount += month.ImageCaptureCount;
                            Quarters[1].QuarterlyScreenTime += month.ScreenTime;
                            Quarters[1].QuarterlyLowTime += month.LowTime;
                            Quarters[1].QuarterlyHighTime += month.HighTime;
                            Quarters[1].QuarterlyExtraHighTime += month.ExtraHighTime;
                            Quarters[1].QuarterlyTppaTime += month.TppaTime;
                            Quarters[1].QuarterlyRprTppaTime += month.RprTppaTime;
                            Quarters[1].QuarterlyScreenCount += month.ScreenCount;
                            Quarters[1].QuarterlyLowCount += month.LowCount;
                            Quarters[1].QuarterlyHighCount += month.HighCount;
                            Quarters[1].QuarterlyExtraHighCount += month.ExtraHighCount;
                            Quarters[1].QuarterlyTppaCount += month.TppaCount;
                            Quarters[1].QuarterlyRprTppaCount += month.RprTppaCount;

                            Quarters[1].QuarterlyPrimeInstrumentCount = int.TryParse(month.PrimeInstrumentCount, out result) ?
                                (result + int.Parse(Quarters[1].QuarterlyPrimeInstrumentCount)).ToString() : month.PrimeInstrumentCount;
                            Quarters[1].QuarterlyProbeAlignmentCount = int.TryParse(month.ProbeAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[1].QuarterlyProbeAlignmentCount)).ToString() : month.ProbeAlignmentCount;
                            Quarters[1].QuarterlyReaderAlignmentCount = int.TryParse(month.ReaderAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[1].QuarterlyReaderAlignmentCount)).ToString() : month.ReaderAlignmentCount;
                            Quarters[1].QuarterlyReaderCalibrationCount = int.TryParse(month.ReaderCalibrationCount, out result) ?
                                (result + int.Parse(Quarters[1].QuarterlyReaderCalibrationCount)).ToString() : month.ReaderCalibrationCount;
                            Quarters[1].QuarterlyLuminescenceReaderCalCount = int.TryParse(month.LuminescenceReaderCalCount, out result) ?
                                (result + int.Parse(Quarters[1].QuarterlyLuminescenceReaderCalCount)).ToString() : month.LuminescenceReaderCalCount;
                            Quarters[1].QuarterlyWashPumpCalibrationCount = int.TryParse(month.WashPumpCalibrationCount, out result) ?
                                (result + int.Parse(Quarters[1].QuarterlyWashPumpCalibrationCount)).ToString() : month.WashPumpCalibrationCount;
                            Quarters[1].QuarterlyCameraAlignmentCount = int.TryParse(month.CameraAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[1].QuarterlyCameraAlignmentCount)).ToString() : month.CameraAlignmentCount;
                            Quarters[1].QuarterlyCameraFocusCount = int.TryParse(month.CameraFocusCount, out result) ?
                                (result + int.Parse(Quarters[1].QuarterlyCameraFocusCount)).ToString() : month.CameraFocusCount;
                            Quarters[1].QuarterlyOpenSoftwareCount = int.TryParse(month.OpenSoftwareCount, out result) ?
                                (result + int.Parse(Quarters[1].QuarterlyOpenSoftwareCount)).ToString() : month.OpenSoftwareCount;
                            Quarters[1].QuarterlyCloseSoftwareCount = int.TryParse(month.CloseSoftwareCount, out result) ?
                                (result + int.Parse(Quarters[1].QuarterlyCloseSoftwareCount)).ToString() : month.CloseSoftwareCount;
                            Quarters[1].QuarterlyNonSoftwareRelatedCount = int.TryParse(month.NonSoftwareRelatedCount, out result) ?
                                (result + int.Parse(Quarters[1].QuarterlyNonSoftwareRelatedCount)).ToString() : month.NonSoftwareRelatedCount;

                            Quarters[1].QuarterlyTotalWorklistRun += month.TotalWorklistRun;
                            Quarters[1].QuarterlyCompletedWorklistRun += month.CompletedWorklistRun;
                            Quarters[1].QuarterlyStoppedWorklistRun += month.StoppedWorklistRun;
                        }
                        else if (month.Month.Equals("Jul") || month.Month.Equals("Aug") || month.Month.Equals("Sep"))
                        {
                            Quarters[2].QuarterlyFluidTransferAction += month.FluidTransferAction;
                            Quarters[2].QuarterlyWashAction += month.WashAction;
                            Quarters[2].QuarterlyShakerAction += month.ShakerAction;
                            Quarters[2].QuarterlyIncubateAction += month.IncubateAction;
                            Quarters[2].QuarterlyHeatAction += month.HeatAction;
                            Quarters[2].QuarterlyReadWellAction += month.ReadWellAction;
                            Quarters[2].QuarterlyPrimeAction += month.PrimeAction;
                            Quarters[2].QuarterlyProbeWashAction += month.ProbeWashAction;
                            Quarters[2].QuarterlyTotalRunTime += month.TotalRunTime;
                            Quarters[2].QuarterlyImageCaptureCount += month.ImageCaptureCount;
                            Quarters[2].QuarterlyScreenTime += month.ScreenTime;
                            Quarters[2].QuarterlyLowTime += month.LowTime;
                            Quarters[2].QuarterlyHighTime += month.HighTime;
                            Quarters[2].QuarterlyExtraHighTime += month.ExtraHighTime;
                            Quarters[2].QuarterlyTppaTime += month.TppaTime;
                            Quarters[2].QuarterlyRprTppaTime += month.RprTppaTime;
                            Quarters[2].QuarterlyScreenCount += month.ScreenCount;
                            Quarters[2].QuarterlyLowCount += month.LowCount;
                            Quarters[2].QuarterlyHighCount += month.HighCount;
                            Quarters[2].QuarterlyExtraHighCount += month.ExtraHighCount;
                            Quarters[2].QuarterlyTppaCount += month.TppaCount;
                            Quarters[2].QuarterlyRprTppaCount += month.RprTppaCount;

                            Quarters[2].QuarterlyPrimeInstrumentCount = int.TryParse(month.PrimeInstrumentCount, out result) ?
                                (result + int.Parse(Quarters[2].QuarterlyPrimeInstrumentCount)).ToString() : month.PrimeInstrumentCount;
                            Quarters[2].QuarterlyProbeAlignmentCount = int.TryParse(month.ProbeAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[2].QuarterlyProbeAlignmentCount)).ToString() : month.ProbeAlignmentCount;
                            Quarters[2].QuarterlyReaderAlignmentCount = int.TryParse(month.ReaderAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[2].QuarterlyReaderAlignmentCount)).ToString() : month.ReaderAlignmentCount;
                            Quarters[2].QuarterlyReaderCalibrationCount = int.TryParse(month.ReaderCalibrationCount, out result) ?
                                (result + int.Parse(Quarters[2].QuarterlyReaderCalibrationCount)).ToString() : month.ReaderCalibrationCount;
                            Quarters[2].QuarterlyLuminescenceReaderCalCount = int.TryParse(month.LuminescenceReaderCalCount, out result) ?
                                (result + int.Parse(Quarters[2].QuarterlyLuminescenceReaderCalCount)).ToString() : month.LuminescenceReaderCalCount;
                            Quarters[2].QuarterlyWashPumpCalibrationCount = int.TryParse(month.WashPumpCalibrationCount, out result) ?
                                (result + int.Parse(Quarters[2].QuarterlyWashPumpCalibrationCount)).ToString() : month.WashPumpCalibrationCount;
                            Quarters[2].QuarterlyCameraAlignmentCount = int.TryParse(month.CameraAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[2].QuarterlyCameraAlignmentCount)).ToString() : month.CameraAlignmentCount;
                            Quarters[2].QuarterlyCameraFocusCount = int.TryParse(month.CameraFocusCount, out result) ?
                                (result + int.Parse(Quarters[2].QuarterlyCameraFocusCount)).ToString() : month.CameraFocusCount;
                            Quarters[2].QuarterlyOpenSoftwareCount = int.TryParse(month.OpenSoftwareCount, out result) ?
                                (result + int.Parse(Quarters[2].QuarterlyOpenSoftwareCount)).ToString() : month.OpenSoftwareCount;
                            Quarters[2].QuarterlyCloseSoftwareCount = int.TryParse(month.CloseSoftwareCount, out result) ?
                                (result + int.Parse(Quarters[2].QuarterlyCloseSoftwareCount)).ToString() : month.CloseSoftwareCount;
                            Quarters[2].QuarterlyNonSoftwareRelatedCount = int.TryParse(month.NonSoftwareRelatedCount, out result) ?
                                (result + int.Parse(Quarters[2].QuarterlyNonSoftwareRelatedCount)).ToString() : month.NonSoftwareRelatedCount;

                            Quarters[2].QuarterlyTotalWorklistRun += month.TotalWorklistRun;
                            Quarters[2].QuarterlyCompletedWorklistRun += month.CompletedWorklistRun;
                            Quarters[2].QuarterlyStoppedWorklistRun += month.StoppedWorklistRun;
                        }
                        else if (month.Month.Equals("Oct") || month.Month.Equals("Nov") || month.Month.Equals("Dec"))
                        {
                            Quarters[3].QuarterlyFluidTransferAction += month.FluidTransferAction;
                            Quarters[3].QuarterlyWashAction += month.WashAction;
                            Quarters[3].QuarterlyShakerAction += month.ShakerAction;
                            Quarters[3].QuarterlyIncubateAction += month.IncubateAction;
                            Quarters[3].QuarterlyHeatAction += month.HeatAction;
                            Quarters[3].QuarterlyReadWellAction += month.ReadWellAction;
                            Quarters[3].QuarterlyPrimeAction += month.PrimeAction;
                            Quarters[3].QuarterlyProbeWashAction += month.ProbeWashAction;
                            Quarters[3].QuarterlyTotalRunTime += month.TotalRunTime;
                            Quarters[3].QuarterlyImageCaptureCount += month.ImageCaptureCount;
                            Quarters[3].QuarterlyScreenTime += month.ScreenTime;
                            Quarters[3].QuarterlyLowTime += month.LowTime;
                            Quarters[3].QuarterlyHighTime += month.HighTime;
                            Quarters[3].QuarterlyExtraHighTime += month.ExtraHighTime;
                            Quarters[3].QuarterlyTppaTime += month.TppaTime;
                            Quarters[3].QuarterlyRprTppaTime += month.RprTppaTime;
                            Quarters[3].QuarterlyScreenCount += month.ScreenCount;
                            Quarters[3].QuarterlyLowCount += month.LowCount;
                            Quarters[3].QuarterlyHighCount += month.HighCount;
                            Quarters[3].QuarterlyExtraHighCount += month.ExtraHighCount;
                            Quarters[3].QuarterlyTppaCount += month.TppaCount;
                            Quarters[3].QuarterlyRprTppaCount += month.RprTppaCount;

                            Quarters[3].QuarterlyPrimeInstrumentCount = int.TryParse(month.PrimeInstrumentCount, out result) ?
                                (result + int.Parse(Quarters[3].QuarterlyPrimeInstrumentCount)).ToString() : month.PrimeInstrumentCount;
                            Quarters[3].QuarterlyProbeAlignmentCount = int.TryParse(month.ProbeAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[3].QuarterlyProbeAlignmentCount)).ToString() : month.ProbeAlignmentCount;
                            Quarters[3].QuarterlyReaderAlignmentCount = int.TryParse(month.ReaderAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[3].QuarterlyReaderAlignmentCount)).ToString() : month.ReaderAlignmentCount;
                            Quarters[3].QuarterlyReaderCalibrationCount = int.TryParse(month.ReaderCalibrationCount, out result) ?
                                (result + int.Parse(Quarters[3].QuarterlyReaderCalibrationCount)).ToString() : month.ReaderCalibrationCount;
                            Quarters[3].QuarterlyLuminescenceReaderCalCount = int.TryParse(month.LuminescenceReaderCalCount, out result) ?
                                (result + int.Parse(Quarters[3].QuarterlyLuminescenceReaderCalCount)).ToString() : month.LuminescenceReaderCalCount;
                            Quarters[3].QuarterlyWashPumpCalibrationCount = int.TryParse(month.WashPumpCalibrationCount, out result) ?
                                (result + int.Parse(Quarters[3].QuarterlyWashPumpCalibrationCount)).ToString() : month.WashPumpCalibrationCount;
                            Quarters[3].QuarterlyCameraAlignmentCount = int.TryParse(month.CameraAlignmentCount, out result) ?
                                (result + int.Parse(Quarters[3].QuarterlyCameraAlignmentCount)).ToString() : month.CameraAlignmentCount;
                            Quarters[3].QuarterlyCameraFocusCount = int.TryParse(month.CameraFocusCount, out result) ?
                                (result + int.Parse(Quarters[3].QuarterlyCameraFocusCount)).ToString() : month.CameraFocusCount;
                            Quarters[3].QuarterlyOpenSoftwareCount = int.TryParse(month.OpenSoftwareCount, out result) ?
                                (result + int.Parse(Quarters[3].QuarterlyOpenSoftwareCount)).ToString() : month.OpenSoftwareCount;
                            Quarters[3].QuarterlyCloseSoftwareCount = int.TryParse(month.CloseSoftwareCount, out result) ?
                                (result + int.Parse(Quarters[3].QuarterlyCloseSoftwareCount)).ToString() : month.CloseSoftwareCount;
                            Quarters[3].QuarterlyNonSoftwareRelatedCount = int.TryParse(month.NonSoftwareRelatedCount, out result) ?
                                (result + int.Parse(Quarters[3].QuarterlyNonSoftwareRelatedCount)).ToString() : month.NonSoftwareRelatedCount;

                            Quarters[3].QuarterlyTotalWorklistRun += month.TotalWorklistRun;
                            Quarters[3].QuarterlyCompletedWorklistRun += month.CompletedWorklistRun;
                            Quarters[3].QuarterlyStoppedWorklistRun += month.StoppedWorklistRun;
                        }
                    }
                }
            });
        }
    }
}
