﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Command;
using PropertyChanged;
using GSDSystemPerformanceTool.Utilities;
using GSD.TestLab.DataCollection;
using System.IO;

namespace GSDSystemPerformanceTool.ViewModel
{
    [AddINotifyPropertyChangedInterface]
    public class YearlySelectorViewModel : IPageViewModel
    {
        private ISubPageViewModel _currentView;
        private List<ISubPageViewModel> _views;

        public YearlySelectorViewModel()
        {
            Name = "Yearly Performance";

            Views = new List<ISubPageViewModel>()
            {
                SimpleIoc.Default.GetInstance<QuarterlySystemViewModel>(),
                SimpleIoc.Default.GetInstance<SemiYearlySystemViewModel>(),
                SimpleIoc.Default.GetInstance<YearlySystemViewModel>()
            };

            CurrentView = Views[2];
            SelectedYear = DateTime.Now.Year.ToString();

            Quarters = Views[0] as QuarterlySystemViewModel;
            SemiYears = Views[1] as SemiYearlySystemViewModel;
            Yearly = Views[2] as YearlySystemViewModel;

            string initWorklistFolder = Directory.Exists(@"C:\Users\Public\Documents\Storm\Status Log Files")
                ? @"C:\Users\Public\Documents\Storm\Status Log Files"
                : @"C:\Users\Public\Documents\AIX1000\Status Log Files";
            string initDBFile = File.Exists(@"C:\ProgramData\Gold Standard Diagnostics\Storm\DataFolder\DataBase\StormDB.sqlite")
                ? @"C:\ProgramData\Gold Standard Diagnostics\Storm\DataFolder\DataBase\StormDB.sqlite"
                : @"C:\ProgramData\Gold Standard Diagnostics\AIX1000\DataFolder\DataBase\DataBaseFile.sqlite3";

            ApplicationName = DataCollection.GetApplicationName(initWorklistFolder);
            InstrumentName = DataCollection.GetInstrumentName(initWorklistFolder);
            ComputerName = DataCollection.GetComputerName(initWorklistFolder);

            ViewQuarterlyCommand = new RelayCommand(ViewQuarterlyExecute);
            ViewSemiYearlyCommand = new RelayCommand(ViewSemiYearlyExecute);
            ViewYearlyCommand = new RelayCommand(ViewYearlyExecute);

            UpdateYearCommand = new RelayCommand<string>(UpdateDataExecute);

            ProgressIsVisible = "Hidden";
        }

        private void ChangeView(ISubPageViewModel vm)
        {
            if (!Views.Contains(vm))
            {
                Views.Add(vm);
            }

            CurrentView = Views.FirstOrDefault(v => v == vm);
        }

        private void ViewQuarterlyExecute()
        {
            ChangeView(Views[0]);
        }
        private void ViewSemiYearlyExecute()
        {
            ChangeView(Views[1]);
        }
        private void ViewYearlyExecute()
        {
            ChangeView(Views[2]);
        }

        private async void UpdateDataExecute(string year)
        {
            Opacity = "0.35";
            ProgressIsVisible = "Visible";

            await Task.Run(() =>
            {
                Quarters?.GetQuarterlyData(year);
                SemiYears?.GetSemiYearlyData(year);
                Yearly?.GetYearlyData(year);
            });

            Opacity = "1";
            ProgressIsVisible = "Hidden";
        }

        public ISubPageViewModel CurrentView
        {
            get { return _currentView; }
            set
            {
                if (value == null)
                {
                    return;
                }

                if (_currentView == null)
                {
                    _currentView = value;
                    _currentView.IsSelected = true;
                    return;
                }

                foreach (var view in _views)
                {
                    view.IsSelected = false;
                }

                _currentView = value;
                _currentView.IsSelected = true;
            }
        }

        public List<ISubPageViewModel> Views
        {
            get
            {
                if (_views == null)
                {
                    _views = new List<ISubPageViewModel>();
                }
                return _views;
            }
            set { _views = value; }
        }

        public ICommand ViewQuarterlyCommand { get; set; }
        public ICommand ViewSemiYearlyCommand { get; set; }
        public ICommand ViewYearlyCommand { get; set; }
        public ICommand UpdateYearCommand { get; set; }
        public bool IsSelected { get; set; }
        public string SelectedYear { get; set; }
        public string ApplicationName { get; set; } = string.Empty;
        public string InstrumentName { get; set; } = string.Empty;
        public string ComputerName { get; set; } = string.Empty;
        public QuarterlySystemViewModel Quarters { get; set; }
        public SemiYearlySystemViewModel SemiYears { get; set; }
        public YearlySystemViewModel Yearly { get; set; }
        public string ProgressIsVisible { get; set; }
        public string Opacity { get; set; }
        public string Name { get; set; }
    }
}
