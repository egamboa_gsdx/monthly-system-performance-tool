﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSDSystemPerformanceTool.ViewModel;
using GalaSoft.MvvmLight.Ioc;
using System.IO;

namespace GSDSystemPerformanceTool
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            // Main View
            SimpleIoc.Default.Register<MainWindowViewModel>();
            SimpleIoc.Default.Register<YearlySelectorViewModel>();
            // Register ViewModels
            SimpleIoc.Default.Register<MonthlySystemViewModel>();
            SimpleIoc.Default.Register<QuarterlySystemViewModel>();
            SimpleIoc.Default.Register<SemiYearlySystemViewModel>();
            SimpleIoc.Default.Register<YearlySystemViewModel>();
        }

        public MainWindowViewModel MainVM => SimpleIoc.Default.GetInstance<MainWindowViewModel>();
        public YearlySelectorViewModel YearlySelectorVM => SimpleIoc.Default.GetInstance<YearlySelectorViewModel>();
        public MonthlySystemViewModel MonthlyVM => SimpleIoc.Default.GetInstance<MonthlySystemViewModel>();
        public QuarterlySystemViewModel QuarterlyVM => SimpleIoc.Default.GetInstance<QuarterlySystemViewModel>();
        public SemiYearlySystemViewModel SemiYearlyVM => SimpleIoc.Default.GetInstance<SemiYearlySystemViewModel>();
        public YearlySystemViewModel YearlyVM => SimpleIoc.Default.GetInstance<YearlySystemViewModel>();        
    }
}
