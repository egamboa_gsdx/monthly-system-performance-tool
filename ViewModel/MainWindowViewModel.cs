﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;

using PropertyChanged;

using GSDSystemPerformanceTool.ViewModel;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using System.Windows;
using GSDSystemPerformanceTool.Model;
using GSDSystemPerformanceTool.View;
using GSDSystemPerformanceTool.Utilities;
using GSD.TestLab.DataCollection;

namespace GSDSystemPerformanceTool
{
    [AddINotifyPropertyChangedInterface]
    public class MainWindowViewModel
    {
        private IPageViewModel _currentPage;
        private List<IPageViewModel> _pages;
        private string _selectedYear;

        public MainWindowViewModel()
        {
            // Add View Models to Pages
            Pages = new List<IPageViewModel>()
            {
                SimpleIoc.Default.GetInstance<MonthlySystemViewModel>(),
                SimpleIoc.Default.GetInstance<YearlySelectorViewModel>()
            };

            var filehistory = FileHistoryHelper.GetFileHistory();
            RecentLogsList = new ObservableCollection<RecentLogViewModel>();

            foreach (var file in filehistory)
            {
                RecentLogsList.Add(new RecentLogViewModel($"{file.WorklistFullPath}, {file.DatabaseFullPath}"));
            }

            CurrentPage = Pages[0];

            SelectedYear = DateTime.Now.ToString("yyyy");

            GoToMonthlyCommand = new RelayCommand(GoToMonthlyExecute);
            GoToYearlyCommand = new RelayCommand(GoToYearlyExecute);
            OpenFolderCommand = new RelayCommand(OpenFolderExecute);
            OpenRecentCommand = new RelayCommand<string>(OpenRecentFolderExeucte);
            ExportToExcelCommand = new RelayCommand(ExportToExcelExecute);
            DataCollectionSettingCommand = new RelayCommand(DataCollectionSettingExecute);
            AboutCommand = new RelayCommand(AboutExecute);
            ExitCommand = new RelayCommand(ExitExecute);
            IsEnabled = true;
        }

        private void ChangePage(IPageViewModel viewModel)
        {
            if (!Pages.Contains(viewModel))
            {
                Pages.Add(viewModel);
            }

            CurrentPage = Pages.FirstOrDefault(vm => vm == viewModel);
        }

        private void GoToMonthlyExecute()
        {
            ChangePage(Pages[0]);

        }

        private void GoToYearlyExecute()
        {
            ChangePage(Pages[1]);

        }

        private void OpenFolderExecute()
        {
            if (Pages[0] is MonthlySystemViewModel mvm)
            {
                mvm.SelectFolderCommand?.Execute(null);
            }
            SelectedYear = DateTime.Now.ToString("yyyy");
        }

        private void OpenRecentFolderExeucte(string path)
        {
            var statusLogPath = path.Substring(0, path.IndexOf(","));
            var dbPath = path.Substring(path.IndexOf(",") + 2);
            var mvm = Pages[0] as MonthlySystemViewModel;

            SelectedYear = DateTime.Now.ToString("yyyy");

            mvm.MonthlyLogs = new ObservableCollection<MonthlySystemModel>();

            mvm.ApplicationName = DataCollection.GetApplicationName(statusLogPath);
            mvm.InstrumentName = DataCollection.GetInstrumentName(statusLogPath);
            mvm.ComputerName = DataCollection.GetComputerName(statusLogPath);

            mvm.AddMonths(statusLogPath, dbPath);

            var yvm = Pages[1] as YearlySelectorViewModel;
            yvm.ApplicationName = mvm.ApplicationName;
            yvm.InstrumentName = mvm.InstrumentName;
            yvm.ComputerName = mvm.ComputerName;
            yvm.SelectedYear = SelectedYear;
            yvm.UpdateYearCommand?.Execute(SelectedYear);

            FileHistoryHelper.AddFileHistory(path);
            var newRecentList = new List<RecentLogViewModel>();
            var filehistory = FileHistoryHelper.GetFileHistory();
            foreach (var file in filehistory)
            {
                newRecentList.Add(new RecentLogViewModel(file.FullInstrumentPath));
            }
            RecentLogsList = new ObservableCollection<RecentLogViewModel>(newRecentList);
        }

        private async void ExportToExcelExecute()
        {
            var popup = new WaitingView();
            popup.Show();
            await Task.Run(() =>
            {
                //Pages[0].ProgressIsVisible  = Pages[1].ProgressIsVisible= "Visible";
                //Pages[0].Opacity = Pages[1].Opacity = 0.35;
                IsEnabled = false;
                ExportHelper.ExportToExcel(CurrentPage.SelectedYear);
                IsEnabled = true;
                //Pages[0].ProgressIsVisible = Pages[1].ProgressIsVisible = "Hidden";
                //Pages[0].Opacity = Pages[1].Opacity = 1.0;
            });
            popup.Close();
        }

        private void DataCollectionSettingExecute()
        {
            DataCollectionSchedulerDialogView dv = new DataCollectionSchedulerDialogView();
            dv.ShowDialog();
        }

        private void AboutExecute()
        {
            var aboutView = new AboutView();
            aboutView.Show();
        }

        private void ExitExecute()
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to close this application?",
                "Hold me", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                System.Windows.Application.Current.Shutdown();
            }
        }

        public List<IPageViewModel> Pages
        {
            get
            {
                if(_pages == null)
                        _pages = new List<IPageViewModel>();
                return _pages;
            }
            set
            {
                _pages = value;
            }
        }

        public IPageViewModel CurrentPage
        {
            get => _currentPage;
            set
            {
                if (value == null)
                {
                    return;
                }

                if (_currentPage == null)
                {
                    _currentPage = value;
                    _currentPage.IsSelected = true;
                    return;
                }

                foreach (var page in _pages)
                {
                    page.IsSelected = false;
                }

                _currentPage = value;
                _currentPage.IsSelected = true;
            }
        }

        public ICommand GoToMonthlyCommand { get; set; }
        public ICommand GoToQuarterlyCommand { get; set; }
        public ICommand GoToSemiYearlyCommand { get; set; }
        public ICommand GoToYearlyCommand { get; set; }
        public ICommand OpenFolderCommand { get; set; }
        public ICommand OpenRecentCommand { get; set; }
        public ICommand ExportToExcelCommand { get; set; }
        public ICommand DataCollectionSettingCommand { get; set; }
        public ICommand AboutCommand { get; set; }
        public ICommand ExitCommand { get; set; }
        public bool IsEnabled { get; set; }
        public string SelectedYear
        {
            get { return _selectedYear; }
            set
            {
                foreach (var page in Pages)
                {
                    page.SelectedYear = value;
                }
                _selectedYear = value;
                if (Pages[0] is MonthlySystemViewModel mvm)
                {
                    mvm.SelectedYearCommand.Execute(null);
                }
            }
        }
        public List<string> Years
        {
            get
            {
                List<string> allyears = new List<string>();

                var currentyear = DateTime.Now.Year;

                for (int i = 2019; i < currentyear + 1; i++)
                {
                    allyears.Add(i.ToString());
                }

                return allyears;
            }
        }

        public IList<RecentLogViewModel> RecentLogsList { get; set; }
    }
}
