﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Text.RegularExpressions;
using GSDSystemPerformanceTool.ViewModel;
using GSDSystemPerformanceTool.Model;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Win32;
using Excel = Microsoft.Office.Interop.Excel;
using GSD.TestLab.DataCollection;
using GSD.TestLab.DataCollection.Enums;
using Microsoft.Office.Interop.Excel;


namespace GSDSystemPerformanceTool.Utilities
{
    public class DataCollector
    {
        private string _filepath = @"C:\Users\Public\Documents\GSD System Performance\";
        private string _filename = "MonthlySystemPerformance.xlsx";

        private string _statuslogsFilePath;

        private static string _delimiter = "M;";

        private DataCollection _dataCollection;

        public DataCollector()
        {
            _statuslogsFilePath = @"C:\Users\Public\Documents\AIX1000\Status Log Files\";
            _dataCollection = new DataCollection();
        }
        public string StatuLogsFilePath
        {
            get { return _statuslogsFilePath; }
            set { _statuslogsFilePath = value; }
        }

        public void InitDataCollection(ObservableCollection<MonthlySystemModel> months, string folderpath)
        {
            if (Directory.Exists(folderpath))
            {
                try
                {
                    foreach (var month in months)
                    {
                        int monthnum = DateTime.ParseExact(month.Month, "MMM", CultureInfo.CurrentCulture).Month;
                        var folder = folderpath + $"\\{month.Year}_{monthnum:D2}";

                        if (Directory.Exists(folder))
                        {
                            string[] filepaths = Directory.GetFiles(folder);
                            month.TotalWorklistRun = filepaths.Length;
                            month.TotalRunTime = TimeSpan.Zero;

                            foreach (var filepath in filepaths)
                            {
                                if (filepath.Contains("MLI"))
                                {
                                    continue;
                                }
                                string[] filelines = File.ReadAllLines(filepath);

                                var totalruntimestring = filelines.SkipWhile(x => x != "----Worklist Actions----").Skip(1).TakeWhile(x => x != string.Empty).ToList();
                                if (int.Parse(month.Year) < 2021)
                                {
                                    totalruntimestring = filelines.SkipWhile(x => !x.Contains("Worklist Started")).TakeWhile(x => x != string.Empty).ToList();
                                }

                                

                                if (filepath.Contains("Stopped"))
                                {
                                    month.StoppedWorklistRun++;
                                }
                                month.FluidTransferAction += _dataCollection.TotalFluidTransfer(filelines.ToList());
                                month.ReadWellAction += _dataCollection.GetActionTime(totalruntimestring, WorklistActionType.ReadWell);
                                month.WashAction += _dataCollection.GetActionTime(totalruntimestring, WorklistActionType.WashWell);
                                month.PrimeAction += _dataCollection.GetActionTime(totalruntimestring, WorklistActionType.Prime);
                                month.ShakerAction += _dataCollection.TotalShaker(filelines.ToList());
                                month.HeatAction += _dataCollection.GetActionTime(filelines.ToList(), WorklistActionType.Heat);
                                month.IncubateAction += _dataCollection.TotalIncubation(filelines.ToList());
                                month.ProbeWashAction += _dataCollection.GetActionTime(filelines.ToList(), WorklistActionType.ProbeWash);
                                month.ImageCaptureCount += _dataCollection.GetImageCaptureCount(filelines.ToList());
                                month.ScreenCount += _dataCollection.GetScreenCount(filelines.ToList());
                                month.LowCount += _dataCollection.GetLowCount(filelines.ToList());
                                month.HighCount += _dataCollection.GetHighCount(filelines.ToList());    
                                month.ExtraHighCount += _dataCollection.GetExtraHighCount(filelines.ToList());
                                month.TppaCount +=_dataCollection.GetTppaCount(filelines.ToList());
                                month.RprTppaCount += _dataCollection.GetRprTppaCount(filelines.ToList());  
                                month.ScreenTime += _dataCollection.GetScreenTime(filelines.ToList());
                                month.LowTime += _dataCollection.GetLowTime(filelines.ToList());
                                month.HighTime += _dataCollection.GetHighTime(filelines.ToList());
                                month.ExtraHighTime += _dataCollection.GetExtraHighTime(filelines.ToList());
                                month.TppaTime += _dataCollection.GetTppaTime(filelines.ToList());
                                month.RprTppaTime += _dataCollection.GetRprTppaTime(filelines.ToList());
                               // month.TotalRunTime += _dataCollection.GetTotalWorklistRunTime(filelines.ToList());

                            }
                            month.CompletedWorklistRun = month.ScreenCount+month.LowCount+month.HighCount+month.ExtraHighCount+month.TppaCount+month.RprTppaCount;
                            month.TotalRunTime = month.ScreenTime+month.LowTime+month.HighTime+month.ExtraHighTime+month.TppaTime+month.RprTppaTime;
                        }
                        else
                        {
                            month.FluidTransferAction = TimeSpan.FromSeconds(0);
                            month.ReadWellAction = TimeSpan.FromSeconds(0);
                            month.WashAction = TimeSpan.FromSeconds(0);
                            month.PrimeAction = TimeSpan.FromSeconds(0);
                            month.ShakerAction = TimeSpan.FromSeconds(0);
                            month.HeatAction = TimeSpan.FromSeconds(0);
                            month.IncubateAction = TimeSpan.FromSeconds(0);
                            month.TotalRunTime = TimeSpan.FromSeconds(0);
                            month.TotalWorklistRun = 0;
                            month.CompletedWorklistRun = 0;
                            month.StoppedWorklistRun = 0;
                            month.CompletedWorklistRun = 0;
                            month.ImageCaptureCount = 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"There was an issue with gathering resources.\n{ex.Message}.");
                    foreach (var month in months)
                    {
                        month.FluidTransferAction = TimeSpan.FromSeconds(0);
                        month.ReadWellAction = TimeSpan.FromSeconds(0);
                        month.WashAction = TimeSpan.FromSeconds(0);
                        month.PrimeAction = TimeSpan.FromSeconds(0);
                        month.ShakerAction = TimeSpan.FromSeconds(0);
                        month.HeatAction = TimeSpan.FromSeconds(0);
                        month.IncubateAction = TimeSpan.FromSeconds(0);
                        month.TotalRunTime = TimeSpan.FromSeconds(0);
                        month.TotalWorklistRun = 0;
                        month.CompletedWorklistRun = 0;
                        month.StoppedWorklistRun = 0;
                        month.CompletedWorklistRun = 0;
                        month.ImageCaptureCount = 0;
                    }
                    return;
                }
            }
            else
            {
                MessageBox.Show($"Could not find work list logs folder.\n'{folderpath}'");
                foreach (var month in months)
                {
                    month.FluidTransferAction = TimeSpan.FromSeconds(0);
                    month.ReadWellAction = TimeSpan.FromSeconds(0);
                    month.WashAction = TimeSpan.FromSeconds(0);
                    month.PrimeAction = TimeSpan.FromSeconds(0);
                    month.ShakerAction = TimeSpan.FromSeconds(0);
                    month.HeatAction = TimeSpan.FromSeconds(0);
                    month.IncubateAction = TimeSpan.FromSeconds(0);
                    month.TotalRunTime = TimeSpan.FromSeconds(0);
                    month.TotalWorklistRun = 0;
                    month.CompletedWorklistRun = 0;
                    month.StoppedWorklistRun = 0;
                    month.CompletedWorklistRun = 0;
                    month.ImageCaptureCount = 0;
                }
            }
        }

        public void GetSQLData(ObservableCollection<MonthlySystemModel> months, string instrumentName, string dbFilePath, string applicationName)
        {

            if (!dbFilePath.Contains("NoDb"))
            {
                if (!File.Exists(dbFilePath))
                {
                    foreach (var month in months)
                    {
                        month.ProbeAlignmentCount = month.PrimeInstrumentCount = month.CameraAlignmentCount = month.CameraFocusCount =
                            month.WashPumpCalibrationCount = month.CloseSoftwareCount = month.OpenSoftwareCount = month.NonSoftwareRelatedCount = "0";

                        month.ReaderAlignmentCount = month.ReaderCalibrationCount = month.LuminescenceReaderCalCount = applicationName.Contains("Storm") ? "0" : "N/A";

                        foreach (var day in month.Days)
                        {
                            day.DailyProbeAlignmentCount = day.DailyPrimeInstrumentCount = day.DailyCameraAlignmentCount = day.DailyCameraFocusCount =
                            day.DailyWashPumpCalibrationCount = day.DailyCloseSoftwareCount = day.DailyOpenSoftwareCount = day.DailyNonSoftwareRelatedCount = "0";

                            day.DailyReaderAlignmentCount = day.DailyReaderCalibrationCount = day.DailyLuminescenceReaderCalCount = applicationName.Contains("Storm") ? "0" : "N/A";
                        }
                    }
                    return;
                }

                SQLiteConnection sqlite_conn = new SQLiteConnection($"Data Source={dbFilePath};" +
                "Version=3;Compress=True;");

                // This is the path to the DB
                // replace 'Data Source' above with this file path when ready to deploy
                // C:\ProgramData\Gold Standard Diagnostics\Storm\DataFolder\DataBase\StormDB.sqlite

                // Reset maintenance alignments as this method will read all data
                foreach (var month in months)
                {
                    month.ProbeAlignmentCount = month.PrimeInstrumentCount = month.CameraAlignmentCount = month.CameraFocusCount =
                        month.WashPumpCalibrationCount = month.CloseSoftwareCount = month.OpenSoftwareCount = month.NonSoftwareRelatedCount = "0";

                    month.ReaderAlignmentCount = month.ReaderCalibrationCount = month.LuminescenceReaderCalCount = applicationName.Contains("Storm") ? "0" : "N/A";

                    foreach (var day in month.Days)
                    {
                        day.DailyProbeAlignmentCount = day.DailyPrimeInstrumentCount = day.DailyCameraAlignmentCount = day.DailyCameraFocusCount =
                        day.DailyWashPumpCalibrationCount = day.DailyCloseSoftwareCount = day.DailyOpenSoftwareCount = day.DailyNonSoftwareRelatedCount = "0";

                        day.DailyReaderAlignmentCount = day.DailyReaderCalibrationCount = day.DailyLuminescenceReaderCalCount = applicationName.Contains("Storm") ? "0" : "N/A";
                    }
                }

                try
                {
                    sqlite_conn.Open();
                    SQLiteDataReader sqlite_datareader;
                    SQLiteCommand sqlite_cmd;
                    sqlite_cmd = sqlite_conn.CreateCommand();

                    int instrumentID = 0;
                    string aixInstrumentNum = string.Empty;
                    if (applicationName.Contains("Storm"))
                    {
                        sqlite_cmd.CommandText = $"SELECT Id FROM tblInstruments WHERE InstrumentName = '{instrumentName}'";

                        sqlite_datareader = sqlite_cmd.ExecuteReader();

                        while (sqlite_datareader.Read())
                        {
                            instrumentID = sqlite_datareader.GetInt32(0);
                        }
                    }
                    else
                    {
                        sqlite_cmd.CommandText = $"SELECT DISTINCT InstrumentSerialNumberAsString FROM Worklists WHERE InstrumentID = '{instrumentName}'";
                        sqlite_datareader = sqlite_cmd.ExecuteReader();

                        while (sqlite_datareader.Read())
                        {
                            aixInstrumentNum = sqlite_datareader.GetString(0);
                        }

                    }

                    Thread.Sleep(100);
                    if (instrumentID != 0 || !string.IsNullOrEmpty(aixInstrumentNum))
                    {
                        SQLiteCommand sql_maintenancecmd = sqlite_conn.CreateCommand();
                        sql_maintenancecmd.CommandText = applicationName.Contains("Storm") ? $"SELECT * FROM tblMaintenances WHERE InstrumentId = {instrumentID}"
                            : $"SELECT * FROM Maintenances WHERE InstrumentSerialNumber = '{aixInstrumentNum}'";

                        int dateIndex = applicationName.Contains("Storm") ? 4 : 3;
                        int maintenanceIndex = applicationName.Contains("Storm") ? 3 : 2;

                        sqlite_datareader = sql_maintenancecmd.ExecuteReader();
                        while (sqlite_datareader.Read())
                        {
                            DateTime maintenancedate = sqlite_datareader.GetDateTime(dateIndex);
                            MonthlySystemModel msm = months.FirstOrDefault(x => x.Month.Equals(maintenancedate.ToString("MMM")) && x.Year.Equals(maintenancedate.ToString("yyyy")));

                            if (msm != null)
                            {
                                int currentMonthResult,
                                    monthPrimeCount = int.TryParse(msm.PrimeInstrumentCount, out currentMonthResult) ? currentMonthResult : 0,
                                    monthProbeCount = int.TryParse(msm.ProbeAlignmentCount, out currentMonthResult) ? currentMonthResult : 0,
                                    monthReaderAlignCount = int.TryParse(msm.ReaderAlignmentCount, out currentMonthResult) ? currentMonthResult : 0,
                                    monthReaderCalCount = int.TryParse(msm.ReaderCalibrationCount, out currentMonthResult) ? currentMonthResult : 0,
                                    monthLumReaderCount = int.TryParse(msm.LuminescenceReaderCalCount, out currentMonthResult) ? currentMonthResult : 0,
                                    monthWashPumpCount = int.TryParse(msm.WashPumpCalibrationCount, out currentMonthResult) ? currentMonthResult : 0,
                                    monthCameraAlignCount = int.TryParse(msm.CameraAlignmentCount, out currentMonthResult) ? currentMonthResult : 0,
                                    monthCloseSoftwareCount = int.TryParse(msm.CloseSoftwareCount, out currentMonthResult) ? currentMonthResult : 0,
                                    monthNonSWCount = int.TryParse(msm.NonSoftwareRelatedCount, out currentMonthResult) ? currentMonthResult : 0,
                                    monthCameraFocusCount = int.TryParse(msm.CameraFocusCount, out currentMonthResult) ? currentMonthResult : 0,
                                    monthOpenSWCount = int.TryParse(msm.OpenSoftwareCount, out currentMonthResult) ? currentMonthResult : 0;

                                DailySystemModel day = msm.Days.FirstOrDefault(x => x.DayIndex == maintenancedate.Day);

                                if (day != null)
                                {
                                    int currentDayResult,
                                    dayPrimeCount = int.TryParse(day.DailyPrimeInstrumentCount, out currentDayResult) ? currentDayResult : 0,
                                    dayProbeCount = int.TryParse(day.DailyProbeAlignmentCount, out currentDayResult) ? currentDayResult : 0,
                                    dayReaderAlignCount = int.TryParse(day.DailyReaderAlignmentCount, out currentDayResult) ? currentDayResult : 0,
                                    dayReaderCalCount = int.TryParse(day.DailyReaderCalibrationCount, out currentDayResult) ? currentDayResult : 0,
                                    dayLumReaderCount = int.TryParse(day.DailyLuminescenceReaderCalCount, out currentDayResult) ? currentDayResult : 0,
                                    dayWashPumpCount = int.TryParse(day.DailyWashPumpCalibrationCount, out currentDayResult) ? currentDayResult : 0,
                                    dayCameraAlignCount = int.TryParse(day.DailyCameraAlignmentCount, out currentDayResult) ? currentDayResult : 0,
                                    dayCloseSoftwareCount = int.TryParse(day.DailyCloseSoftwareCount, out currentDayResult) ? currentDayResult : 0,
                                    dayNonSWCount = int.TryParse(day.DailyNonSoftwareRelatedCount, out currentDayResult) ? currentDayResult : 0,
                                    dayCameraFocusCount = int.TryParse(day.DailyCameraFocusCount, out currentDayResult) ? currentDayResult : 0,
                                    dayOpenSWCount = int.TryParse(day.DailyOpenSoftwareCount, out currentDayResult) ? currentDayResult : 0;

                                    switch (sqlite_datareader.GetInt32(maintenanceIndex))
                                    {
                                        case 0:
                                            monthPrimeCount++;
                                            dayPrimeCount++;
                                            break;
                                        case 1:
                                            monthProbeCount++;
                                            dayProbeCount++;
                                            break;
                                        case 2:
                                            if (applicationName.Contains("Storm"))
                                            {
                                                monthReaderAlignCount++;
                                                dayReaderAlignCount++;
                                            }
                                            else
                                            {
                                                monthCameraAlignCount++;
                                                dayCameraAlignCount++;
                                            }
                                            break;
                                        case 3:
                                            if (applicationName.Contains("Storm"))
                                            {
                                                monthReaderCalCount++;
                                                dayReaderCalCount++;
                                            }
                                            else
                                            {
                                                monthCameraFocusCount++;
                                                dayCameraFocusCount++;
                                            }
                                            break;
                                        case 4:
                                            if (applicationName.Contains("Storm"))
                                            {
                                                monthLumReaderCount++;
                                                dayLumReaderCount++;
                                            }
                                            else
                                            {
                                                monthWashPumpCount++;
                                                dayWashPumpCount++;
                                            }
                                            break;
                                        case 5:
                                            if (applicationName.Contains("Storm"))
                                            {
                                                monthWashPumpCount++;
                                                dayWashPumpCount++;
                                            }
                                            else
                                            {
                                                monthCloseSoftwareCount++;
                                                dayCloseSoftwareCount++;
                                            }
                                            break;
                                        case 6:
                                            if (applicationName.Contains("Storm"))
                                            {
                                                monthCameraAlignCount++;
                                                dayCameraAlignCount++;
                                            }
                                            else
                                            {
                                                monthNonSWCount++;
                                                dayNonSWCount++;
                                            }
                                            break;
                                        case 7:
                                            if (applicationName.Contains("Storm"))
                                            {
                                                monthCloseSoftwareCount++;
                                                dayCloseSoftwareCount++;
                                            }
                                            else
                                            {
                                                monthOpenSWCount++;
                                                dayOpenSWCount++;
                                            }
                                            break;
                                        case 8: // AIX Should never get to this and the below cases...but you never know
                                            monthNonSWCount++;
                                            dayNonSWCount++;
                                            break;
                                        case 9:
                                            monthLumReaderCount++;
                                            dayLumReaderCount++;
                                            break;
                                        case 10:
                                            monthCameraFocusCount++;
                                            dayCameraFocusCount++;
                                            break;
                                        case 11:
                                            monthOpenSWCount++;
                                            dayOpenSWCount++;
                                            break;
                                        default:
                                            break;
                                    }

                                    day.DailyPrimeInstrumentCount = dayPrimeCount.ToString();
                                    day.DailyProbeAlignmentCount = dayProbeCount.ToString();
                                    day.DailyCameraAlignmentCount = dayCameraAlignCount.ToString();
                                    day.DailyCameraFocusCount = dayCameraFocusCount.ToString();
                                    day.DailyCloseSoftwareCount = dayCloseSoftwareCount.ToString();
                                    day.DailyNonSoftwareRelatedCount = dayNonSWCount.ToString();
                                    day.DailyOpenSoftwareCount = dayOpenSWCount.ToString();
                                    day.DailyWashPumpCalibrationCount = dayWashPumpCount.ToString();

                                    // These are storm specific maintenance types
                                    day.DailyLuminescenceReaderCalCount = applicationName.Contains("Storm") ? dayLumReaderCount.ToString() : "N/A";
                                    day.DailyReaderAlignmentCount = applicationName.Contains("Storm") ? dayReaderAlignCount.ToString() : "N/A";
                                    day.DailyReaderCalibrationCount = applicationName.Contains("Storm") ? dayReaderCalCount.ToString() : "N/A";

                                    bool maintenanceDone = !day.DailyPrimeInstrumentCount.Equals("0") || !day.DailyProbeAlignmentCount.Equals("0") ||
                                        (!day.DailyReaderAlignmentCount.Equals("0") && !day.DailyReaderAlignmentCount.Equals("N/A")) ||
                                        (!day.DailyReaderCalibrationCount.Equals("0") && !day.DailyReaderCalibrationCount.Equals("N/A")) ||
                                        (!day.DailyLuminescenceReaderCalCount.Equals("0") && !day.DailyLuminescenceReaderCalCount.Equals("N/A")) ||
                                        !day.DailyWashPumpCalibrationCount.Equals("0") || !day.DailyCameraAlignmentCount.Equals("0") ||
                                        !day.DailyCloseSoftwareCount.Equals("0") || !day.DailyOpenSoftwareCount.Equals("0") || !day.DailyNonSoftwareRelatedCount.Equals("0");

                                    if (day.WorklistRan == Enums.WorklistRanStatus.NoWorklistRan && maintenanceDone)
                                    {
                                        day.WorklistRan = Enums.WorklistRanStatus.Maintenance;
                                        day.Header = $"{day.Month} {day.DayIndex} \tMaintenance Only";
                                    }
                                    else if (day.WorklistRan == Enums.WorklistRanStatus.WorklistRan && maintenanceDone)
                                    {
                                        day.WorklistRan = Enums.WorklistRanStatus.WorklistAndMaintenance;
                                        day.Header = $"{day.Month} {day.DayIndex} \t{day.DailyTotalWorklistRun} Worklist(s) Ran and Maintenance";
                                    }
                                }

                                msm.PrimeInstrumentCount = monthPrimeCount.ToString();
                                msm.ProbeAlignmentCount = monthProbeCount.ToString();
                                msm.CameraAlignmentCount = monthCameraAlignCount.ToString();
                                msm.CameraFocusCount = monthCameraFocusCount.ToString();
                                msm.CloseSoftwareCount = monthCloseSoftwareCount.ToString();
                                msm.NonSoftwareRelatedCount = monthNonSWCount.ToString();
                                msm.OpenSoftwareCount = monthOpenSWCount.ToString();
                                msm.WashPumpCalibrationCount = monthWashPumpCount.ToString();

                                // These are storm specific maintenance types
                                msm.LuminescenceReaderCalCount = applicationName.Contains("Storm") ? monthLumReaderCount.ToString() : "N/A";
                                msm.ReaderAlignmentCount = applicationName.Contains("Storm") ? monthReaderAlignCount.ToString() : "N/A";
                                msm.ReaderCalibrationCount = applicationName.Contains("Storm") ? monthReaderCalCount.ToString() : "N/A";
                            }
                        }
                    }
                    sqlite_conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("There is an issue with the database being used.\nException:{0}. ({1})", ex.GetType().ToString(), ex.Message),
                            "You done fucked up!", MessageBoxButton.OK, MessageBoxImage.Error);
                    foreach (var m in months)
                    {
                        m.PrimeInstrumentCount = m.ProbeAlignmentCount = m.CameraAlignmentCount = m.CameraFocusCount = "Database not accessible";
                        m.CloseSoftwareCount = m.OpenSoftwareCount = m.NonSoftwareRelatedCount = m.WashPumpCalibrationCount = "Database not accessible";
                        m.LuminescenceReaderCalCount = m.ReaderAlignmentCount = m.ReaderCalibrationCount = "Database not accessible";
                    }

                }
            }
            else
            {
                foreach (var month in months)
                {
                    month.CameraAlignmentCount = month.CameraFocusCount = month.ReaderAlignmentCount = month.ReaderCalibrationCount =
                    month.PrimeInstrumentCount = month.ProbeAlignmentCount = month.LuminescenceReaderCalCount = month.WashPumpCalibrationCount =
                    month.CloseSoftwareCount = month.OpenSoftwareCount = month.NonSoftwareRelatedCount = "N/A";
                    foreach (var day in month.Days)
                    {
                        day.DailyProbeAlignmentCount = day.DailyPrimeInstrumentCount = day.DailyCameraAlignmentCount = day.DailyCameraFocusCount =
                            day.DailyReaderAlignmentCount = day.DailyReaderCalibrationCount = day.DailyLuminescenceReaderCalCount =
                        day.DailyWashPumpCalibrationCount = day.DailyCloseSoftwareCount = day.DailyOpenSoftwareCount = day.DailyNonSoftwareRelatedCount = "N/A";
                    }
                }
            }
        }

        public void CollectData()
        {
            //Excel.Application xlApp = new Excel.Application();
            //Excel.Workbook xlworkbook = null;
            //Excel.Sheets xlworksheets = null;
            //Excel.Range er = null;
            //object misValue = Missing.Value;
            //string year = DateTime.Now.ToString("yyyy");
            
            //if (xlApp == null)
            //{
            //    MessageBox.Show("Excel is not properly installed!", "Excel not installed", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return;
            //}

            //if (!Directory.Exists(_filepath))
            //{
            //    Directory.CreateDirectory(_filepath);
            //}

            //if (!File.Exists(_filepath + string.Format("{0}{1}", year, _filename)))
            //{
            //    try
            //    {
            //        xlworkbook = xlApp.Workbooks.Add(misValue);
            //        xlworksheets = xlworkbook.Worksheets;
            //        var newxlworksheet = (Excel.Worksheet)xlworksheets.get_Item(1);
            //        newxlworksheet.Name = _monthlylogs[0].Month;

            //        er = newxlworksheet.get_Range("A:A", misValue);
            //        er.EntireColumn.ColumnWidth = 40;

            //        newxlworksheet.Cells[1, 1] = $"{_monthlylogs[0].Month} Monthly Report";
            //        newxlworksheet.Cells[3, 1] = "# of Probe Alignments";
            //        newxlworksheet.Cells[4, 1] = "# of Reader Alignments";
            //        newxlworksheet.Cells[5, 1] = "# of Camera Alignments";
            //        newxlworksheet.Cells[6, 1] = "Total # of Worklist Runs";
            //        newxlworksheet.Cells[7, 1] = "# of Completed Worklist Runs";
            //        newxlworksheet.Cells[8, 1] = "# of Stopped Worklist Runs";
            //        newxlworksheet.Cells[9, 1] = "Total Instrument Runtime (hh:mm:ss)";
            //        newxlworksheet.Cells[10, 1] = "Fluid Transfer Action(s) (hh:mm:ss)";
            //        newxlworksheet.Cells[11, 1] = "Wash Action(s) (hh:mm:ss)";
            //        newxlworksheet.Cells[12, 1] = "Shaker Action(s) (hh:mm:ss)";
            //        newxlworksheet.Cells[13, 1] = "Incubation Action(s) (hh:mm:ss)";
            //        newxlworksheet.Cells[14, 1] = "Heat Action(s) (hh:mm:ss)";
            //        newxlworksheet.Cells[15, 1] = "Read Well Action(s) (hh:mm:ss)";
            //        newxlworksheet.Cells[16, 1] = "Priming (hh:mm:ss)";
            //        newxlworksheet.Cells[17, 1] = "Probe Wash Action(s) (hh:mm:ss)";

            //        newxlworksheet.Cells[3, 2] = _monthlylogs[0].ProbeAlignmentCount;
            //        newxlworksheet.Cells[4, 2] = _monthlylogs[0].ReaderAlignmentCount;
            //        newxlworksheet.Cells[5, 2] = _monthlylogs[0].CameraAlignmentCount;
            //        newxlworksheet.Cells[6, 2] = _monthlylogs[0].TotalWorklistRun;
            //        newxlworksheet.Cells[7, 2] = _monthlylogs[0].CompletedWorklistRun;
            //        newxlworksheet.Cells[8, 2] = _monthlylogs[0].StoppedWorklistRun;
            //        newxlworksheet.Cells[9, 2] = _monthlylogs[0].TotalRunTime.ToString(@"hh\:mm\:ss");
            //        newxlworksheet.Cells[10, 2] = _monthlylogs[0].FluidTransferAction.ToString(@"hh\:mm\:ss");
            //        newxlworksheet.Cells[11, 2] = _monthlylogs[0].WashAction.ToString(@"hh\:mm\:ss");
            //        newxlworksheet.Cells[12, 2] = _monthlylogs[0].ShakerAction.ToString(@"hh\:mm\:ss");
            //        newxlworksheet.Cells[13, 2] = _monthlylogs[0].IncubateAction.ToString(@"hh\:mm\:ss");
            //        newxlworksheet.Cells[14, 2] = _monthlylogs[0].HeatAction.ToString(@"hh\:mm\:ss");
            //        newxlworksheet.Cells[15, 2] = _monthlylogs[0].ReadWellAction.ToString(@"hh\:mm\:ss");
            //        newxlworksheet.Cells[16, 2] = _monthlylogs[0].PrimeAction.ToString(@"hh\:mm\:ss");
            //        newxlworksheet.Cells[17, 2] = _monthlylogs[0].ProbeWashAction.ToString(@"hh\:mm\:ss");

            //        er = newxlworksheet.get_Range("B:B", misValue);
            //        er.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            //        for (int i = 1; i < _monthlylogs.Count; i++)
            //        {
            //            newxlworksheet = (Excel.Worksheet)xlworksheets.Add(After: xlworksheets[xlworksheets.Count]);

            //            newxlworksheet.Name = _monthlylogs[i].Month;

            //            er = newxlworksheet.get_Range("A:A", misValue);
            //            er.EntireColumn.ColumnWidth = 40;

            //            newxlworksheet.Cells[1, 1] = $"{_monthlylogs[i].Month} Monthly Report";
            //            newxlworksheet.Cells[3, 1] = "# of Probe Alignments";
            //            newxlworksheet.Cells[4, 1] = "# of Reader Alignments";
            //            newxlworksheet.Cells[5, 1] = "# of Camera Alignments";
            //            newxlworksheet.Cells[6, 1] = "Total # of Worklist Runs";
            //            newxlworksheet.Cells[7, 1] = "# of Completed Worklist Runs";
            //            newxlworksheet.Cells[8, 1] = "# of Stopped Worklist Runs";
            //            newxlworksheet.Cells[9, 1] = "Total Instrument Runtime (hh:mm:ss)";
            //            newxlworksheet.Cells[10, 1] = "Fluid Transfer Action(s) (hh:mm:ss)";
            //            newxlworksheet.Cells[11, 1] = "Wash Action(s) (hh:mm:ss)";
            //            newxlworksheet.Cells[12, 1] = "Shaker Action(s) (hh:mm:ss)";
            //            newxlworksheet.Cells[13, 1] = "Incubation Action(s) (hh:mm:ss)";
            //            newxlworksheet.Cells[14, 1] = "Heat Action(s) (hh:mm:ss)";
            //            newxlworksheet.Cells[15, 1] = "Read Well Action(s) (hh:mm:ss)";
            //            newxlworksheet.Cells[16, 1] = "Priming (hh:mm:ss)";
            //            newxlworksheet.Cells[17, 1] = "Probe Wash Action(s) (hh:mm:ss)";

            //            newxlworksheet.Cells[3, 2] = _monthlylogs[i].ProbeAlignmentCount;
            //            newxlworksheet.Cells[4, 2] = _monthlylogs[i].ReaderAlignmentCount;
            //            newxlworksheet.Cells[5, 2] = _monthlylogs[i].CameraAlignmentCount;
            //            newxlworksheet.Cells[6, 2] = _monthlylogs[i].TotalWorklistRun;
            //            newxlworksheet.Cells[7, 2] = _monthlylogs[i].CompletedWorklistRun;
            //            newxlworksheet.Cells[8, 2] = _monthlylogs[i].StoppedWorklistRun;
            //            newxlworksheet.Cells[9, 2] = _monthlylogs[i].TotalRunTime.ToString(@"hh\:mm\:ss");
            //            newxlworksheet.Cells[10, 2] = _monthlylogs[i].FluidTransferAction.ToString(@"hh\:mm\:ss");
            //            newxlworksheet.Cells[11, 2] = _monthlylogs[i].WashAction.ToString(@"hh\:mm\:ss");
            //            newxlworksheet.Cells[12, 2] = _monthlylogs[i].ShakerAction.ToString(@"hh\:mm\:ss");
            //            newxlworksheet.Cells[13, 2] = _monthlylogs[i].IncubateAction.ToString(@"hh\:mm\:ss");
            //            newxlworksheet.Cells[14, 2] = _monthlylogs[i].HeatAction.ToString(@"hh\:mm\:ss");
            //            newxlworksheet.Cells[15, 2] = _monthlylogs[i].ReadWellAction.ToString(@"hh\:mm\:ss");
            //            newxlworksheet.Cells[16, 2] = _monthlylogs[i].PrimeAction.ToString(@"hh\:mm\:ss");
            //            newxlworksheet.Cells[17, 2] = _monthlylogs[i].ProbeWashAction.ToString(@"hh\:mm\:ss");

            //            er = newxlworksheet.get_Range("B:B", misValue);
            //            er.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            //        }

            //        newxlworksheet = (Excel.Worksheet)xlworksheets.get_Item(1);
            //        newxlworksheet.Select();

            //        xlworkbook.SaveAs2(_filepath + string.Format("{0}{1}", year, _filename));
            //        xlworkbook.Close(true, misValue, misValue);
            //        xlApp.Quit();

                    
            //        Marshal.ReleaseComObject(newxlworksheet);
            //        Marshal.ReleaseComObject(xlworksheets);
            //        Marshal.ReleaseComObject(xlworkbook);
            //        Marshal.ReleaseComObject(xlApp);

            //        newxlworksheet = null;
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.Message);
            //    }
            //    finally
            //    {
            //        xlApp = null;
            //        xlworkbook = null;
            //        xlworksheets = null;
            //    }
            //}
            //else
            //{
            //    try
            //    {
            //        xlworkbook = xlApp.Workbooks.Open(_filepath + string.Format("{0}{1}", year, _filename));
            //        xlworksheets = xlworkbook.Worksheets;
            //        Excel.Worksheet worksheet = null;

            //        foreach (var monthlylog in _monthlylogs)
            //        {
            //           worksheet = xlworksheets.OfType<Excel.Worksheet>().FirstOrDefault(w => w.Name.Equals(monthlylog.Month));

            //            if (worksheet == null)
            //            {
            //                worksheet = (Excel.Worksheet)xlworksheets.Add(After: xlworksheets[xlworksheets.Count]);

            //                worksheet.Name = monthlylog.Month;

            //                er = worksheet.get_Range("A:A", misValue);
            //                er.EntireColumn.ColumnWidth = 40;

            //                worksheet.Cells[1, 1] = $"{monthlylog.Month} Monthly Report";
            //                worksheet.Cells[3, 1] = "# of Probe Alignments";
            //                worksheet.Cells[4, 1] = "# of Reader Alignments";
            //                worksheet.Cells[5, 1] = "# of Camera Alignments";
            //                worksheet.Cells[6, 1] = "Total # of Worklist Runs";
            //                worksheet.Cells[7, 1] = "# of Completed Worklist Runs";
            //                worksheet.Cells[8, 1] = "# of Stopped Worklist Runs";
            //                worksheet.Cells[9, 1] = "Total Instrument Runtime (hh:mm:ss)";
            //                worksheet.Cells[10, 1] = "Fluid Transfer Action(s) (hh:mm:ss)";
            //                worksheet.Cells[11, 1] = "Wash Action(s) (hh:mm:ss)";
            //                worksheet.Cells[12, 1] = "Shaker Action(s) (hh:mm:ss)";
            //                worksheet.Cells[13, 1] = "Incubation Action(s) (hh:mm:ss)";
            //                worksheet.Cells[14, 1] = "Heat Action(s) (hh:mm:ss)";
            //                worksheet.Cells[15, 1] = "Read Well Action(s) (hh:mm:ss)";
            //                worksheet.Cells[16, 1] = "Priming (hh:mm:ss)";
            //                worksheet.Cells[17, 1] = "Probe Wash Action(s) (hh:mm:ss)";
                            
            //                worksheet.Cells[3, 2] = monthlylog.ProbeAlignmentCount;
            //                worksheet.Cells[4, 2] = monthlylog.ReaderAlignmentCount;
            //                worksheet.Cells[5, 2] = monthlylog.CameraAlignmentCount;
            //                worksheet.Cells[6, 2] = monthlylog.TotalWorklistRun;
            //                worksheet.Cells[7, 2] = monthlylog.CompletedWorklistRun;
            //                worksheet.Cells[8, 2] = monthlylog.StoppedWorklistRun;
            //                worksheet.Cells[9, 2] = monthlylog.TotalRunTime.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[10, 2] = monthlylog.FluidTransferAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[11, 2] = monthlylog.WashAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[12, 2] = monthlylog.ShakerAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[13, 2] = monthlylog.IncubateAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[14, 2] = monthlylog.HeatAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[15, 2] = monthlylog.ReadWellAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[16, 2] = monthlylog.PrimeAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[17, 2] = monthlylog.ProbeWashAction.ToString(@"hh\:mm\:ss");

            //                er = worksheet.get_Range("B:B", misValue);
            //                er.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            //            }
            //            else
            //            {
            //                worksheet.Cells[3, 2] = monthlylog.ProbeAlignmentCount;
            //                worksheet.Cells[4, 2] = monthlylog.ReaderAlignmentCount;
            //                worksheet.Cells[5, 2] = monthlylog.CameraAlignmentCount;
            //                worksheet.Cells[6, 2] = monthlylog.TotalWorklistRun;
            //                worksheet.Cells[7, 2] = monthlylog.CompletedWorklistRun;
            //                worksheet.Cells[8, 2] = monthlylog.StoppedWorklistRun;
            //                worksheet.Cells[9, 2] = monthlylog.TotalRunTime.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[10, 2] = monthlylog.FluidTransferAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[11, 2] = monthlylog.WashAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[12, 2] = monthlylog.ShakerAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[13, 2] = monthlylog.IncubateAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[14, 2] = monthlylog.HeatAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[15, 2] = monthlylog.ReadWellAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[16, 2] = monthlylog.PrimeAction.ToString(@"hh\:mm\:ss");
            //                worksheet.Cells[17, 2] = monthlylog.ProbeWashAction.ToString(@"hh\:mm\:ss");
            //            }
            //        }

            //        xlworkbook.Save();
            //        xlworkbook.Close(true, misValue, misValue);
            //        xlApp.Quit();

            //        Marshal.ReleaseComObject(worksheet);
            //        Marshal.ReleaseComObject(xlworksheets);
            //        Marshal.ReleaseComObject(xlworkbook);
            //        Marshal.ReleaseComObject(xlApp);

            //        worksheet = null;
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.Message);
            //    }
            //    finally
            //    {
            //        xlApp = null;
            //        xlworkbook = null;
            //        xlworksheets = null;
            //    }
            //}
        }
    }
}
