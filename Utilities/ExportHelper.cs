﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Runtime.InteropServices;
using GalaSoft.MvvmLight.Ioc;
using GSDSystemPerformanceTool.ViewModel;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;

namespace GSDSystemPerformanceTool.Utilities
{
    public class ExportHelper
    {
        public static void ExportToExcel(string year)
        {
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook workbook = null;
            Excel.Sheets worksheets = null;
            Excel.Worksheet newWorksheet = null;
            Excel.Range er = null;
            object missingVal = Missing.Value;

            var monthlyVM = SimpleIoc.Default.GetInstance<MonthlySystemViewModel>();
            var quarterlyVM = new QuarterlySystemViewModel();
            var semiyearlyVM = new SemiYearlySystemViewModel();
            var yearlyVM = new YearlySystemViewModel();

            var monthInYear = monthlyVM.MonthlyLogs.Where(x => x.Year.Equals(year)).OrderBy(x => x.MonthNum).ToList();
            quarterlyVM.GetQuarterlyData(year);
            semiyearlyVM.GetSemiYearlyData(year);
            yearlyVM.GetYearlyData(year);

            if (xlApp == null)
            {
                MessageBox.Show("Execel is not properly installed! Could not export.", "Excel Is Not Installed", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try
            {
                workbook = xlApp.Workbooks.Add(missingVal);
                worksheets = workbook.Worksheets;
                newWorksheet = worksheets.get_Item(1) as Excel.Worksheet;
                newWorksheet.Name = monthInYear[0].Month.ToString();

                er = newWorksheet.get_Range("A:A", missingVal);
                er.EntireColumn.ColumnWidth = 62;
                er.EntireColumn.RowHeight = 45;
                er.Font.Bold = true;
                er.Font.Size = "25";

                newWorksheet.Cells[1, 1] = $"Application Name: {monthlyVM.ApplicationName}";
                newWorksheet.Cells[2, 1] = $"Instrument Name: {monthlyVM.InstrumentName}";
                newWorksheet.Cells[3, 1] = $"Computer Name: {monthlyVM.ComputerName}";
                
                newWorksheet.Cells[4, 1] = $"{monthInYear[0].Month} {monthInYear[0].Year} Monthly Report";

                newWorksheet.Cells[6, 1] = "Day";
                newWorksheet.Cells[7, 1] = "Build #";
                newWorksheet.Cells[8, 1] = "Installation";
                newWorksheet.Cells[9, 1] = "Daily Startup Prime 10x";
                newWorksheet.Cells[10, 1] = "20x Liquinox Prime";
                newWorksheet.Cells[11, 1] = "50x Prime Di Water";
                newWorksheet.Cells[12, 1] = "Daily Shut Down-Empty Waste";
                newWorksheet.Cells[13, 1] = "Daily Shutdown-50 Prime Di";
                newWorksheet.Cells[14, 1] = "Probe Auto Alignment";
                newWorksheet.Cells[15, 1] = "Probe Manual Alignment";
                newWorksheet.Cells[16, 1] = "Focus Alignment";
                newWorksheet.Cells[17, 1] = "Camera Alignment";
                newWorksheet.Cells[18, 1] = "# of Worklist Run";
                newWorksheet.Cells[19, 1] = "Total Instrument Runtime";
                newWorksheet.Cells[20, 1] = "Fluid Transfer Action";
                newWorksheet.Cells[21, 1] = "Shaker Action";
                newWorksheet.Cells[22, 1] = "Incubation Action";
                newWorksheet.Cells[23, 1] = "# Images Captured";
                newWorksheet.Cells[24, 1] = "# of Screen Action";
                newWorksheet.Cells[25, 1] = "Screen Action Runtime";
                newWorksheet.Cells[26, 1] = "# of Low Titer Action";
                newWorksheet.Cells[27, 1] = "Low Titer Action Runtime";
                newWorksheet.Cells[28, 1] = "# of High Ttiter Action";
                newWorksheet.Cells[29, 1] = "High Titer Action Runtime";
                newWorksheet.Cells[30, 1] = "# of Extra High Titer Action";
                newWorksheet.Cells[31, 1] = "Extra High Titer Action Runtime";
                newWorksheet.Cells[32, 1] = "# of TPPA Test Action";
                newWorksheet.Cells[33, 1] = "TPPA Test Action Runtime";
                newWorksheet.Cells[34, 1] = "# of RPR+TPPA Test Action";
                newWorksheet.Cells[35, 1] = "RPR+TPPA Test Action Runtime";

                newWorksheet.Cells[38, 1] = "Day";
                newWorksheet.Cells[39, 1] = "Build #";
                newWorksheet.Cells[40, 1] = "Installation";
                newWorksheet.Cells[41, 1] = "Daily Startup Prime 10x";
                newWorksheet.Cells[42, 1] = "20x Liquinox Prime";
                newWorksheet.Cells[43, 1] = "50x Prime Di Waster";
                newWorksheet.Cells[44, 1] = "Daily Shut Down-Empty Waste";
                newWorksheet.Cells[45, 1] = "Daily Shutdown-50x Prime Di";
                newWorksheet.Cells[46, 1] = "Probe Auto Alignment";
                newWorksheet.Cells[47, 1] = "Probe Manual Alignment";
                newWorksheet.Cells[48, 1] = "Focus Alignment";
                newWorksheet.Cells[49, 1] = "Camera Alignment";
                newWorksheet.Cells[50, 1] = "# of Completed Worklist";
                newWorksheet.Cells[51, 1] = "Total Instrument Runtime";
                newWorksheet.Cells[52, 1] = "Fluid Transfer Action";
                newWorksheet.Cells[53, 1] = "Shaker Action";
                newWorksheet.Cells[54, 1] = "Incubation Action";
                newWorksheet.Cells[55, 1] = "# Images Captured";
                newWorksheet.Cells[56, 1] = "# of Screen Action";
                newWorksheet.Cells[57, 1] = "Screen Action Runtime";
                newWorksheet.Cells[58, 1] = "# of Low Titer Action";
                newWorksheet.Cells[59, 1] = "Low Titer Action Runtime";
                newWorksheet.Cells[60, 1] = "# of High Ttiter Action";
                newWorksheet.Cells[61, 1] = "High Titer Action Runtime";
                newWorksheet.Cells[62, 1] = "# of Extra High Titer Action";
                newWorksheet.Cells[63, 1] = "Extra High Titer Action Runtime";
                newWorksheet.Cells[64, 1] = "# of TPPA Test Action";
                newWorksheet.Cells[65, 1] = "TPPA Test Action Runtime";
                newWorksheet.Cells[66, 1] = "# of RPR+TPPA Test Action";
                newWorksheet.Cells[67, 1] = "RPR+TPPA Test Action Runtime";
                newWorksheet.Cells[68, 1] = "NOTE: All times in hh:mm:ss";



                int i;
                int j;
                for (i = 0; i <= 15; i++)
                {
                    var column = newWorksheet.Columns[2 + i];
                    column.ColumnWidth = 20;
                    column.Font.Size = 25;
                    newWorksheet.Cells[6, 2 + i] = monthInYear[0].Days[i].DayIndex;
                    column.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    int day= monthInYear[0].Days[i].DayIndex; 
                    DateTime Monthday = new DateTime(Int32.Parse(year), monthInYear[0].MonthNum, day);
                    DayOfWeek newday = Monthday.DayOfWeek;
                    newWorksheet.Cells[5, 2 + i] = Monthday.ToString("ddd"); 
                    newWorksheet.Cells[9, 2 + i] = 1;// daily prime
                    newWorksheet.Cells[10, 2 + i] = 0;// 20 liquinox
                    newWorksheet.Cells[11, 2 + i] = 0;// 50 di water
                    newWorksheet.Cells[12, 2 + i] = 1;// daily empty waste
                    newWorksheet.Cells[13, 2 + i] = 1;// daily shutdwon prime
                    newWorksheet.Cells[14, 2 + i] = 0;// auto align
                    newWorksheet.Cells[15, 2 + i] = 0;// manual align
                    newWorksheet.Cells[16, 2 + i] = 0;// focus align
                    newWorksheet.Cells[17, 2 + i] = 0;// camera align
                    if ( newday == DayOfWeek.Sunday || newday== DayOfWeek.Saturday)
                    {
                        newWorksheet.Cells[9, 2 + i] = 0;// daily prime
                        newWorksheet.Cells[12, 2+ i] = 0;// daily empty waste
                        newWorksheet.Cells[13, 2+ i] = 0;// daily shutdwon prime
                    }
                    if ( newday == DayOfWeek.Monday)
                    {
                        
                        newWorksheet.Cells[10, 2 + i] = 1;// 20 liquinox
                        newWorksheet.Cells[11, 2 + i] = 1;// 50 di water
                        newWorksheet.Cells[14, 2 + i] = 1;// auto align
                        newWorksheet.Cells[16, 2 + i] = 1;// focus align
                        newWorksheet.Cells[17, 2 + i] = 1;// camera align
                    }
                    
                    //newWorksheet.Cells[7, 2 + i] = monthInYear[0].Days[i].DailyPrimeInstrumentCount;
                    

                    //newWorksheet.Cells[8, 2 + i] = monthInYear[0].Days[i].DailyProbeAlignmentCount;
                    //newWorksheet.Cells[9, 2 + i] = monthInYear[0].Days[i].DailyReaderAlignmentCount;
                    //newWorksheet.Cells[10, 2 + i] = monthInYear[0].Days[i].DailyReaderCalibrationCount;
                    //newWorksheet.Cells[11, 2 + i] = monthInYear[0].Days[i].DailyLuminescenceReaderCalCount;
                    //newWorksheet.Cells[12, 2 + i] = monthInYear[0].Days[i].DailyWashPumpCalibrationCount;
                    //newWorksheet.Cells[13, 2 + i] = monthInYear[0].Days[i].DailyCameraAlignmentCount;
                    //newWorksheet.Cells[14, 2 + i] = monthInYear[0].Days[i].DailyCameraFocusCount;
                    //newWorksheet.Cells[15, 2 + i] = monthInYear[0].Days[i].DailyOpenSoftwareCount;
                    //newWorksheet.Cells[16, 2 + i] = monthInYear[0].Days[i].DailyCloseSoftwareCount;
                    //newWorksheet.Cells[17, 2 + i] = monthInYear[0].Days[i].DailyNonSoftwareRelatedCount;
                    newWorksheet.Cells[18, 2 + i] = monthInYear[0].Days[i].DailyCompletedWorklistRun;
                    newWorksheet.Cells[19, 2 + i] = monthInYear[0].Days[i].DailyTotalRunTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[20, 2 + i] = monthInYear[0].Days[i].DailyFluidTransferAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[21, 2 + i] = monthInYear[0].Days[i].DailyShakerAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[22, 2 + i] = monthInYear[0].Days[i].DailyIncubateAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[23, 2 + i] = monthInYear[0].Days[i].DailyImageCaptureCount;
                    newWorksheet.Cells[24, 2 + i] = monthInYear[0].Days[i].DailyScreenCount;
                    newWorksheet.Cells[25, 2 + i] = monthInYear[0].Days[i].DailyScreenTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[26, 2 + i] = monthInYear[0].Days[i].DailyLowCount;
                    newWorksheet.Cells[27, 2 + i] = monthInYear[0].Days[i].DailyLowTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[28, 2 + i] = monthInYear[0].Days[i].DailyHighCount;
                    newWorksheet.Cells[29, 2 + i] = monthInYear[0].Days[i].DailyHighTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[30, 2 + i] = monthInYear[0].Days[i].DailyExtraHighCount;
                    newWorksheet.Cells[31, 2 + i] = monthInYear[0].Days[i].DailyExtraHighTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[32, 2 + i] = monthInYear[0].Days[i].DailyTppaCount;
                    newWorksheet.Cells[33, 2 + i] = monthInYear[0].Days[i].DailyTppaTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[34, 2 + i] = monthInYear[0].Days[i].DailyRprTppaCount;
                    newWorksheet.Cells[35, 2 + i] = monthInYear[0].Days[i].DailyRprTppaTime.ToString(@"hh\:mm\:ss");

                }
                for (i = 16; i < monthInYear[0].NumOfDays; i++)
                {
                    var column = newWorksheet.Columns[(2 + i) - 16];
                    column.ColumnWidth = 20;
                    column.Font.Size = 25;
                    newWorksheet.Cells[38, (2 + i) - 16] = monthInYear[0].Days[i].DayIndex;
                    column.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    int day = monthInYear[0].Days[i].DayIndex;
                    DateTime Monthday = new DateTime(Int32.Parse(year), monthInYear[0].MonthNum, day);
                    DayOfWeek newday = Monthday.DayOfWeek;
                    newWorksheet.Cells[37, (2 + i) - 16] = Monthday.ToString("ddd");
                    newWorksheet.Cells[41, (2 + i) - 16] = 1;// daily prime
                    newWorksheet.Cells[42, (2 + i) - 16] = 0;// 20 liquinox
                    newWorksheet.Cells[43, (2 + i) - 16] = 0;// 50 di water
                    newWorksheet.Cells[44, (2 + i) - 16] = 1;// daily empty waste
                    newWorksheet.Cells[45, (2 + i) - 16] = 1;// daily shutdwon prime
                    newWorksheet.Cells[46, (2 + i) - 16] = 0;// auto align
                    newWorksheet.Cells[47, (2 + i) - 16] = 0;// manual align
                    newWorksheet.Cells[48, (2 + i) - 16] = 0;// focus align
                    newWorksheet.Cells[49, (2 + i) - 16] = 0;// camera align
                    if (newday == DayOfWeek.Sunday || newday == DayOfWeek.Saturday)
                    {
                        newWorksheet.Cells[41, (2 + i) - 16] = 0;// daily prime
                        newWorksheet.Cells[42, (2 + i) - 16] = 0;// daily empty waste
                        newWorksheet.Cells[43, (2 + i) - 16] = 0;// daily shutdwon prime
                    }
                    if (newday == DayOfWeek.Monday)
                    {

                        newWorksheet.Cells[42, (2 + i) - 16] = 1;// 20 liquinox
                        newWorksheet.Cells[43, (2 + i) - 16] = 1;// 50 di water
                        newWorksheet.Cells[46, (2 + i) - 16] = 1;// auto align
                        newWorksheet.Cells[48, (2 + i) - 16] = 1;// focus align
                        newWorksheet.Cells[49, (2 + i) - 16] = 1;// camera align
                    }
                    //newWorksheet.Cells[7, 2 + i] = monthInYear[0].Days[i].DailyPrimeInstrumentCount;
                    //newWorksheet.Cells[8, 2 + i] = monthInYear[0].Days[i].DailyProbeAlignmentCount;
                    //newWorksheet.Cells[9, 2 + i] = monthInYear[0].Days[i].DailyReaderAlignmentCount;
                    //newWorksheet.Cells[10, 2 + i] = monthInYear[0].Days[i].DailyReaderCalibrationCount;
                    //newWorksheet.Cells[11, 2 + i] = monthInYear[0].Days[i].DailyLuminescenceReaderCalCount;
                    //newWorksheet.Cells[12, 2 + i] = monthInYear[0].Days[i].DailyWashPumpCalibrationCount;
                    //newWorksheet.Cells[13, 2 + i] = monthInYear[0].Days[i].DailyCameraAlignmentCount;
                    //newWorksheet.Cells[14, 2 + i] = monthInYear[0].Days[i].DailyCameraFocusCount;
                    //newWorksheet.Cells[15, 2 + i] = monthInYear[0].Days[i].DailyOpenSoftwareCount;
                    //newWorksheet.Cells[16, 2 + i] = monthInYear[0].Days[i].DailyCloseSoftwareCount;
                    //newWorksheet.Cells[17, 2 + i] = monthInYear[0].Days[i].DailyNonSoftwareRelatedCount;
                    newWorksheet.Cells[50, (2 + i)-16] = monthInYear[0].Days[i].DailyCompletedWorklistRun;
                    newWorksheet.Cells[51, (2 + i) - 16] = monthInYear[0].Days[i].DailyTotalRunTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[52, (2 + i) - 16] = monthInYear[0].Days[i].DailyFluidTransferAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[53, (2 + i) - 16] = monthInYear[0].Days[i].DailyShakerAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[54, (2 + i) - 16] = monthInYear[0].Days[i].DailyIncubateAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[55, (2 + i) - 16] = monthInYear[0].Days[i].DailyImageCaptureCount;
                    newWorksheet.Cells[56, (2 + i) - 16] = monthInYear[0].Days[i].DailyScreenCount;
                    newWorksheet.Cells[57, (2 + i) - 16] = monthInYear[0].Days[i].DailyScreenTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[58, (2 + i) - 16] = monthInYear[0].Days[i].DailyLowCount;
                    newWorksheet.Cells[59, (2 + i) - 16] = monthInYear[0].Days[i].DailyLowTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[60, (2 + i) - 16] = monthInYear[0].Days[i].DailyHighCount;
                    newWorksheet.Cells[61, (2 + i) - 16] = monthInYear[0].Days[i].DailyHighTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[62, (2 + i) - 16] = monthInYear[0].Days[i].DailyExtraHighCount;
                    newWorksheet.Cells[63, (2 + i) - 16] = monthInYear[0].Days[i].DailyExtraHighTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[64, (2 + i) - 16] = monthInYear[0].Days[i].DailyTppaCount;
                    newWorksheet.Cells[65, (2 + i) - 16] = monthInYear[0].Days[i].DailyTppaTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[66, (2 + i) - 16] = monthInYear[0].Days[i].DailyRprTppaCount;
                    newWorksheet.Cells[67, (2 + i) - 16] = monthInYear[0].Days[i].DailyRprTppaTime.ToString(@"hh\:mm\:ss");

                }
                

                newWorksheet.Columns[(2 + i) - 16].ColumnWidth = 25;
                
                newWorksheet.Columns[(2 + i) - 16].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                newWorksheet.Cells[38, (2 + i) - 16] = "Month Total";
                newWorksheet.Cells[38, (2 + i) - 16].Font.Size= 20;
                newWorksheet.Cells[38, (2 + i) - 16].Font.Bold = true;
                //newWorksheet.Cells[7, 2 + i] = monthInYear[0].PrimeInstrumentCount;
                //newWorksheet.Cells[8, 2 + i] = monthInYear[0].ProbeAlignmentCount;
                //newWorksheet.Cells[9, 2 + i] = monthInYear[0].ReaderAlignmentCount;
                //newWorksheet.Cells[10, 2 + i] = monthInYear[0].ReaderCalibrationCount;
                //newWorksheet.Cells[11, 2 + i] = monthInYear[0].LuminescenceReaderCalCount;
                //newWorksheet.Cells[12, 2 + i] = monthInYear[0].WashPumpCalibrationCount;
                //newWorksheet.Cells[13, 2 + i] = monthInYear[0].CameraAlignmentCount;
                //newWorksheet.Cells[14, 2 + i] = monthInYear[0].CameraFocusCount;
                //newWorksheet.Cells[15, 2 + i] = monthInYear[0].OpenSoftwareCount;
                //newWorksheet.Cells[16, 2 + i] = monthInYear[0].CloseSoftwareCount;
                //newWorksheet.Cells[17, 2 + i] = monthInYear[0].NonSoftwareRelatedCount;
                newWorksheet.Cells[50, (2 + i) - 16] = monthInYear[0].CompletedWorklistRun;
                newWorksheet.Cells[51, (2 + i) - 16] = monthInYear[0].TotalRunTime.ToString(@"hh\:mm\:ss");
                newWorksheet.Cells[52, (2 + i) - 16] = monthInYear[0].FluidTransferAction.ToString(@"hh\:mm\:ss");
                newWorksheet.Cells[53, (2 + i) - 16] = monthInYear[0].ShakerAction.ToString(@"hh\:mm\:ss");
                newWorksheet.Cells[54, (2 + i) - 16] = monthInYear[0].IncubateAction.ToString(@"hh\:mm\:ss");
                newWorksheet.Cells[55, (2 + i) - 16] = monthInYear[0].ImageCaptureCount;
                newWorksheet.Cells[56, (2 + i) - 16] = monthInYear[0].ScreenCount;
                newWorksheet.Cells[57, (2 + i) - 16] = monthInYear[0].ScreenTime.ToString(@"hh\:mm\:ss");
                newWorksheet.Cells[58, (2 + i) - 16] = monthInYear[0].LowCount;
                newWorksheet.Cells[59, (2 + i) - 16] = monthInYear[0].LowTime.ToString(@"hh\:mm\:ss");
                newWorksheet.Cells[60, (2 + i) - 16] = monthInYear[0].HighCount;
                newWorksheet.Cells[61, (2 + i) - 16] = monthInYear[0].HighTime.ToString(@"hh\:mm\:ss");
                newWorksheet.Cells[62, (2 + i) - 16] = monthInYear[0].ExtraHighCount;
                newWorksheet.Cells[63, (2 + i) - 16] = monthInYear[0].ExtraHighTime.ToString(@"hh\:mm\:ss");
                newWorksheet.Cells[64, (2 + i) - 16] = monthInYear[0].TppaCount;
                newWorksheet.Cells[65, (2 + i) - 16] = monthInYear[0].TppaTime.ToString(@"hh\:mm\:ss");
                newWorksheet.Cells[66, (2 + i) - 16] = monthInYear[0].RprTppaCount;
                newWorksheet.Cells[67, (2 + i) - 16] = monthInYear[0].RprTppaTime.ToString(@"hh\:mm\:ss");

                string columnName = "";
                int columnNum = (2 + i) - 16;

                while (columnNum > 0)
                {
                    int modulo = (columnNum - 1) % 26;
                    columnName = Convert.ToChar('A' + modulo) + columnName;
                    columnNum = (columnNum - modulo) / 26;
                }

                er = newWorksheet.get_Range("A6", $"{columnName}6");
                er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
                er.Cells.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);

                er = newWorksheet.get_Range("A38", $"{columnName}38");
                er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
                er.Cells.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                er.Font.Size = "25";
                for (int r = 7; r <= 35; r++)
                {
                    if (r % 2 == 0)
                    {
                        er = newWorksheet.get_Range($"A{r}", $"{columnName}{r}");
                        er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.AliceBlue);
                    }
                }
                for (int r = 39; r <= 67; r++)
                {
                    if (r % 2 == 0)
                    {
                        er = newWorksheet.get_Range($"A{r}", $"{columnName}{r}");
                        er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.AliceBlue);
                    }
                }

                for (int k = 1; k < monthInYear.Count; k++)
                {
                    newWorksheet = (Excel.Worksheet)worksheets.Add(After: worksheets[worksheets.Count]);
                    newWorksheet.Name = monthInYear[k].Month.ToString();

                    
                    er = newWorksheet.get_Range("A:A", missingVal);
                    er.EntireColumn.ColumnWidth = 62;
                    er.EntireColumn.RowHeight = 45;
                    er.Font.Bold = true;
                    er.Font.Size = "25";

                    newWorksheet.Cells[1, 1] = $"Application Name: {monthlyVM.ApplicationName}";
                    newWorksheet.Cells[2, 1] = $"Instrument Name: {monthlyVM.InstrumentName}";
                    newWorksheet.Cells[3, 1] = $"Computer Name: {monthlyVM.ComputerName}";

                    newWorksheet.Cells[4, 1] = $"{monthInYear[k].Month} {monthInYear[k].Year} Monthly Report";

                    newWorksheet.Cells[6, 1] = "Day";
                    newWorksheet.Cells[7, 1] = "Build #";
                    newWorksheet.Cells[8, 1] = "Installation";
                    newWorksheet.Cells[9, 1] = "Daily Startup Prime 10x";
                    newWorksheet.Cells[10, 1] = "20x Liquinox Prime";
                    newWorksheet.Cells[11, 1] = "50x Prime Di Water";
                    newWorksheet.Cells[12, 1] = "Daily Shut Down-Empty Waste";
                    newWorksheet.Cells[13, 1] = "Daily Shutdown-50 Prime Di";
                    newWorksheet.Cells[14, 1] = "Probe Auto Alignment";
                    newWorksheet.Cells[15, 1] = "Probe Manual Alignment";
                    newWorksheet.Cells[16, 1] = "Focus Alignment";
                    newWorksheet.Cells[17, 1] = "Camera Alignment";
                    newWorksheet.Cells[18, 1] = "# of Worklist Run";
                    newWorksheet.Cells[19, 1] = "Total Instrument Runtime";
                    newWorksheet.Cells[20, 1] = "Fluid Transfer Action";
                    newWorksheet.Cells[21, 1] = "Shaker Action";
                    newWorksheet.Cells[22, 1] = "Incubation Action";
                    newWorksheet.Cells[23, 1] = "# Images Captured";
                    newWorksheet.Cells[24, 1] = "# of Screen Action";
                    newWorksheet.Cells[25, 1] = "Screen Action Runtime";
                    newWorksheet.Cells[26, 1] = "# of Low Titer Action";
                    newWorksheet.Cells[27, 1] = "Low Titer Action Runtime";
                    newWorksheet.Cells[28, 1] = "# of High Ttiter Action";
                    newWorksheet.Cells[29, 1] = "High Titer Action Runtime";
                    newWorksheet.Cells[30, 1] = "# of Extra High Titer Action";
                    newWorksheet.Cells[31, 1] = "Extra High Titer Action Runtime";
                    newWorksheet.Cells[32, 1] = "# of TPPA Test Action";
                    newWorksheet.Cells[33, 1] = "TPPA Test Action Runtime";
                    newWorksheet.Cells[34, 1] = "# of RPR+TPPA Test Action";
                    newWorksheet.Cells[35, 1] = "RPR+TPPA Test Action Runtime";

                    newWorksheet.Cells[38, 1] = "Day";
                    newWorksheet.Cells[39, 1] = "Build #";
                    newWorksheet.Cells[40, 1] = "Installation";
                    newWorksheet.Cells[41, 1] = "Daily Startup Prime 10x";
                    newWorksheet.Cells[42, 1] = "20x Liquinox Prime";
                    newWorksheet.Cells[43, 1] = "50x Prime Di Waster";
                    newWorksheet.Cells[44, 1] = "Daily Shut Down-Empty Waste";
                    newWorksheet.Cells[45, 1] = "Daily Shutdown-50x Prime Di";
                    newWorksheet.Cells[46, 1] = "Probe Auto Alignment";
                    newWorksheet.Cells[47, 1] = "Probe Manual Alignment";
                    newWorksheet.Cells[48, 1] = "Focus Alignment";
                    newWorksheet.Cells[49, 1] = "Camera Alignment";
                    newWorksheet.Cells[50, 1] = "# of Completed Worklist";
                    newWorksheet.Cells[51, 1] = "Total Instrument Runtime";
                    newWorksheet.Cells[52, 1] = "Fluid Transfer Action";
                    newWorksheet.Cells[53, 1] = "Shaker Action";
                    newWorksheet.Cells[54, 1] = "Incubation Action";
                    newWorksheet.Cells[55, 1] = "# Images Captured";
                    newWorksheet.Cells[56, 1] = "# of Screen Action";
                    newWorksheet.Cells[57, 1] = "Screen Action Runtime";
                    newWorksheet.Cells[58, 1] = "# of Low Titer Action";
                    newWorksheet.Cells[59, 1] = "Low Titer Action Runtime";
                    newWorksheet.Cells[60, 1] = "# of High Ttiter Action";
                    newWorksheet.Cells[61, 1] = "High Titer Action Runtime";
                    newWorksheet.Cells[62, 1] = "# of Extra High Titer Action";
                    newWorksheet.Cells[63, 1] = "Extra High Titer Action Runtime";
                    newWorksheet.Cells[64, 1] = "# of TPPA Test Action";
                    newWorksheet.Cells[65, 1] = "TPPA Test Action Runtime";
                    newWorksheet.Cells[66, 1] = "# of RPR+TPPA Test Action";
                    newWorksheet.Cells[67, 1] = "RPR+TPPA Test Action Runtime";
                    newWorksheet.Cells[68, 1] = "NOTE: All times in hh:mm:ss";


                    for (i = 0; i <=15; i++)
                    {
                        var column = newWorksheet.Columns[2 + i];
                        column.ColumnWidth = 20;
                        column.Font.Size = 25;
                        newWorksheet.Cells[6, 2 + i] = monthInYear[0].Days[i].DayIndex;
                        column.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                        int day = monthInYear[0].Days[i].DayIndex;
                        DateTime Monthday = new DateTime(Int32.Parse(year), monthInYear[0].MonthNum, day);
                        DayOfWeek newday = Monthday.DayOfWeek;
                        newWorksheet.Cells[5, 2 + i] = Monthday.ToString("ddd");
                        newWorksheet.Cells[9, 2 + i] = 1;// daily prime
                        newWorksheet.Cells[10, 2 + i] = 0;// 20 liquinox
                        newWorksheet.Cells[11, 2 + i] = 0;// 50 di water
                        newWorksheet.Cells[12, 2 + i] = 1;// daily empty waste
                        newWorksheet.Cells[13, 2 + i] = 1;// daily shutdwon prime
                        newWorksheet.Cells[14, 2 + i] = 0;// auto align
                        newWorksheet.Cells[15, 2 + i] = 0;// manual align
                        newWorksheet.Cells[16, 2 + i] = 0;// focus align
                        newWorksheet.Cells[17, 2 + i] = 0;// camera align
                        if (newday == DayOfWeek.Sunday || newday == DayOfWeek.Saturday)
                        {
                            newWorksheet.Cells[9, 2 + i] = 0;// daily prime
                            newWorksheet.Cells[12, 2 + i] = 0;// daily empty waste
                            newWorksheet.Cells[13, 2 + i] = 0;// daily shutdwon prime
                        }
                        if (newday == DayOfWeek.Monday)
                        {

                            newWorksheet.Cells[10, 2 + i] = 1;// 20 liquinox
                            newWorksheet.Cells[11, 2 + i] = 1;// 50 di water
                            newWorksheet.Cells[14, 2 + i] = 1;// auto align
                            newWorksheet.Cells[16, 2 + i] = 1;// focus align
                            newWorksheet.Cells[17, 2 + i] = 1;// camera align
                        }
                        //newWorksheet.Cells[7, 2 + i] = monthInYear[0].Days[i].DailyPrimeInstrumentCount;
                        //newWorksheet.Cells[8, 2 + i] = monthInYear[0].Days[i].DailyProbeAlignmentCount;
                        //newWorksheet.Cells[9, 2 + i] = monthInYear[0].Days[i].DailyReaderAlignmentCount;
                        //newWorksheet.Cells[10, 2 + i] = monthInYear[0].Days[i].DailyReaderCalibrationCount;
                        //newWorksheet.Cells[11, 2 + i] = monthInYear[0].Days[i].DailyLuminescenceReaderCalCount;
                        //newWorksheet.Cells[12, 2 + i] = monthInYear[0].Days[i].DailyWashPumpCalibrationCount;
                        //newWorksheet.Cells[13, 2 + i] = monthInYear[0].Days[i].DailyCameraAlignmentCount;
                        //newWorksheet.Cells[14, 2 + i] = monthInYear[0].Days[i].DailyCameraFocusCount;
                        //newWorksheet.Cells[15, 2 + i] = monthInYear[0].Days[i].DailyOpenSoftwareCount;
                        //newWorksheet.Cells[16, 2 + i] = monthInYear[0].Days[i].DailyCloseSoftwareCount;
                        //newWorksheet.Cells[17, 2 + i] = monthInYear[0].Days[i].DailyNonSoftwareRelatedCount;
                        newWorksheet.Cells[18, 2 + i] = monthInYear[k].Days[i].DailyCompletedWorklistRun;
                        newWorksheet.Cells[19, 2 + i] = monthInYear[k].Days[i].DailyTotalRunTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[20, 2 + i] = monthInYear[k].Days[i].DailyFluidTransferAction.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[21, 2 + i] = monthInYear[k].Days[i].DailyShakerAction.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[22, 2 + i] = monthInYear[k].Days[i].DailyIncubateAction.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[23, 2 + i] = monthInYear[k].Days[i].DailyImageCaptureCount;
                        newWorksheet.Cells[24, 2 + i] = monthInYear[k].Days[i].DailyScreenCount;
                        newWorksheet.Cells[25, 2 + i] = monthInYear[k].Days[i].DailyScreenTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[26, 2 + i] = monthInYear[k].Days[i].DailyLowCount;
                        newWorksheet.Cells[27, 2 + i] = monthInYear[k].Days[i].DailyLowTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[28, 2 + i] = monthInYear[k].Days[i].DailyHighCount;
                        newWorksheet.Cells[29, 2 + i] = monthInYear[k].Days[i].DailyHighTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[30, 2 + i] = monthInYear[k].Days[i].DailyExtraHighCount;
                        newWorksheet.Cells[31, 2 + i] = monthInYear[k].Days[i].DailyExtraHighTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[32, 2 + i] = monthInYear[k].Days[i].DailyTppaCount;
                        newWorksheet.Cells[33, 2 + i] = monthInYear[k].Days[i].DailyTppaTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[34, 2 + i] = monthInYear[k].Days[i].DailyRprTppaCount;
                        newWorksheet.Cells[35, 2 + i] = monthInYear[k].Days[i].DailyRprTppaTime.ToString(@"hh\:mm\:ss");


                    }
                    for (i = 16; i < monthInYear[k].NumOfDays; i++)
                    {
                        var column = newWorksheet.Columns[(2 + i) - 16];
                        column.ColumnWidth = 20;
                        column.Font.Size = 25;
                        column.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                        newWorksheet.Cells[38, (2 + i) - 16] = monthInYear[k].Days[i].DayIndex;
                        newWorksheet.Cells[38, (2 + i) - 16].Font.Bold = true;

                        int day = monthInYear[0].Days[i].DayIndex;
                        DateTime Monthday = new DateTime(Int32.Parse(year), monthInYear[0].MonthNum, day);
                        DayOfWeek newday = Monthday.DayOfWeek;
                        newWorksheet.Cells[37, (2 + i) - 16] = Monthday.ToString("ddd");
                        newWorksheet.Cells[41, (2 + i) - 16] = 1;// daily prime
                        newWorksheet.Cells[42, (2 + i) - 16] = 0;// 20 liquinox
                        newWorksheet.Cells[43, (2 + i) - 16] = 0;// 50 di water
                        newWorksheet.Cells[44, (2 + i) - 16] = 1;// daily empty waste
                        newWorksheet.Cells[45, (2 + i) - 16] = 1;// daily shutdwon prime
                        newWorksheet.Cells[46, (2 + i) - 16] = 0;// auto align
                        newWorksheet.Cells[47, (2 + i) - 16] = 0;// manual align
                        newWorksheet.Cells[48, (2 + i) - 16] = 0;// focus align
                        newWorksheet.Cells[49, (2 + i) - 16] = 0;// camera align
                        if (newday == DayOfWeek.Sunday || newday == DayOfWeek.Saturday)
                        {
                            newWorksheet.Cells[41, (2 + i) - 16] = 0;// daily prime
                            newWorksheet.Cells[42, (2 + i) - 16] = 0;// daily empty waste
                            newWorksheet.Cells[43, (2 + i) - 16] = 0;// daily shutdwon prime
                        }
                        if (newday == DayOfWeek.Monday)
                        {

                            newWorksheet.Cells[42, (2 + i) - 16] = 1;// 20 liquinox
                            newWorksheet.Cells[43, (2 + i) - 16] = 1;// 50 di water
                            newWorksheet.Cells[46, (2 + i) - 16] = 1;// auto align
                            newWorksheet.Cells[48, (2 + i) - 16] = 1;// focus align
                            newWorksheet.Cells[49, (2 + i) - 16] = 1;// camera align
                        }
                        //newWorksheet.Cells[7, 2 + i] = monthInYear[k].Days[i].DailyPrimeInstrumentCount;
                        //newWorksheet.Cells[8, 2 + i] = monthInYear[k].Days[i].DailyProbeAlignmentCount;
                        //newWorksheet.Cells[9, 2 + i] = monthInYear[k].Days[i].DailyReaderAlignmentCount;
                        //newWorksheet.Cells[10, 2 + i] = monthInYear[k].Days[i].DailyReaderCalibrationCount;
                        //newWorksheet.Cells[11, 2 + i] = monthInYear[k].Days[i].DailyLuminescenceReaderCalCount;
                        //newWorksheet.Cells[12, 2 + i] = monthInYear[k].Days[i].DailyWashPumpCalibrationCount;
                        //newWorksheet.Cells[13, 2 + i] = monthInYear[k].Days[i].DailyCameraAlignmentCount;
                        //newWorksheet.Cells[14, 2 + i] = monthInYear[k].Days[i].DailyCameraFocusCount;
                        //newWorksheet.Cells[15, 2 + i] = monthInYear[k].Days[i].DailyOpenSoftwareCount;
                        //newWorksheet.Cells[16, 2 + i] = monthInYear[k].Days[i].DailyCloseSoftwareCount;
                        //newWorksheet.Cells[17, 2 + i] = monthInYear[k].Days[i].DailyNonSoftwareRelatedCount;
                        newWorksheet.Cells[50, (2 + i) - 16] = monthInYear[k].Days[i].DailyCompletedWorklistRun;
                        newWorksheet.Cells[51, (2 + i) - 16] = monthInYear[k].Days[i].DailyTotalRunTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[52, (2 + i) - 16] = monthInYear[k].Days[i].DailyFluidTransferAction.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[53, (2 + i) - 16] = monthInYear[k].Days[i].DailyShakerAction.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[54, (2 + i) - 16] = monthInYear[k].Days[i].DailyIncubateAction.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[55, (2 + i) - 16] = monthInYear[k].Days[i].DailyImageCaptureCount;
                        newWorksheet.Cells[56, (2 + i) - 16] = monthInYear[k].Days[i].DailyScreenCount;
                        newWorksheet.Cells[57, (2 + i) - 16] = monthInYear[k].Days[i].DailyScreenTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[58, (2 + i) - 16] = monthInYear[k].Days[i].DailyLowCount;
                        newWorksheet.Cells[59, (2 + i) - 16] = monthInYear[k].Days[i].DailyLowTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[60, (2 + i) - 16] = monthInYear[k].Days[i].DailyHighCount;
                        newWorksheet.Cells[61, (2 + i) - 16] = monthInYear[k].Days[i].DailyHighTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[62, (2 + i) - 16] = monthInYear[k].Days[i].DailyExtraHighCount;
                        newWorksheet.Cells[63, (2 + i) - 16] = monthInYear[k].Days[i].DailyExtraHighTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[64, (2 + i) - 16] = monthInYear[k].Days[i].DailyTppaCount;
                        newWorksheet.Cells[65, (2 + i) - 16] = monthInYear[k].Days[i].DailyTppaTime.ToString(@"hh\:mm\:ss");
                        newWorksheet.Cells[66, (2 + i) - 16] = monthInYear[k].Days[i].DailyRprTppaCount;
                        newWorksheet.Cells[67, (2 + i) - 16] = monthInYear[k].Days[i].DailyRprTppaTime.ToString(@"hh\:mm\:ss");

                    }
                    TimeSpan time = new TimeSpan(200, 60, 60);

                    newWorksheet.Columns[(2 + i) - 16].ColumnWidth = 20;
                    newWorksheet.Columns[(2 + i) - 16].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    newWorksheet.Cells[38, (2 + i) - 16].Font.Size = 20;
                    newWorksheet.Cells[38, (2 + i) - 16] = "Month Total";
                    newWorksheet.Cells[38, (2 + i) - 16].Font.Bold = true;
                    //newWorksheet.Cells[7, 2 + i] = monthInYear[k].PrimeInstrumentCount;
                    //newWorksheet.Cells[8, 2 + i] = monthInYear[k].ProbeAlignmentCount;
                    //newWorksheet.Cells[9, 2 + i] = monthInYear[k].ReaderAlignmentCount;
                    //newWorksheet.Cells[10, 2 + i] = monthInYear[k].ReaderCalibrationCount;
                    //newWorksheet.Cells[11, 2 + i] = monthInYear[k].LuminescenceReaderCalCount;
                    //newWorksheet.Cells[12, 2 + i] = monthInYear[k].WashPumpCalibrationCount;
                    //newWorksheet.Cells[13, 2 + i] = monthInYear[k].CameraAlignmentCount;
                    //newWorksheet.Cells[14, 2 + i] = monthInYear[k].CameraFocusCount;
                    //newWorksheet.Cells[15, 2 + i] = monthInYear[k].OpenSoftwareCount;
                    //newWorksheet.Cells[16, 2 + i] = monthInYear[k].CloseSoftwareCount;
                    //newWorksheet.Cells[17, 2 + i] = monthInYear[k].NonSoftwareRelatedCount;
                    newWorksheet.Cells[50, (2 + i) - 16] = monthInYear[k].CompletedWorklistRun;
                    newWorksheet.Cells[51, (2 + i) - 16] = string.Format("{0}:{1}:{2}", (int)monthInYear[k].TotalRunTime.TotalHours, monthInYear[k].TotalRunTime.Minutes, monthInYear[k].TotalRunTime.Seconds);
                    newWorksheet.Cells[52, (2 + i) - 16] = string.Format("{0}:{1}:{2}", (int)monthInYear[k].FluidTransferAction.TotalHours, monthInYear[k].FluidTransferAction.Minutes, monthInYear[k].FluidTransferAction.Seconds);
                    newWorksheet.Cells[53, (2 + i) - 16] = monthInYear[k].ShakerAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[54, (2 + i) - 16] = monthInYear[k].IncubateAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[55, (2 + i) - 16] = monthInYear[k].ImageCaptureCount;
                    newWorksheet.Cells[56, (2 + i) - 16] = monthInYear[k].ScreenCount;
                    newWorksheet.Cells[57, (2 + i) - 16] = string.Format("{0}:{1}:{2}", (int)monthInYear[k].ScreenTime.TotalHours, monthInYear[k].ScreenTime.Minutes, monthInYear[k].ScreenTime.Seconds);
                    newWorksheet.Cells[58, (2 + i) - 16] = monthInYear[k].LowCount;
                    newWorksheet.Cells[59, (2 + i) - 16] = string.Format("{0}:{1}:{2}", (int)monthInYear[k].LowTime.TotalHours, monthInYear[k].LowTime.Minutes, monthInYear[k].LowTime.Seconds);
                    newWorksheet.Cells[60, (2 + i) - 16] = monthInYear[k].HighCount;
                    newWorksheet.Cells[61, (2 + i) - 16] = string.Format("{0}:{1}:{2}", (int)monthInYear[k].HighTime.TotalHours, monthInYear[k].HighTime.Minutes, monthInYear[k].HighTime.Seconds);
                    newWorksheet.Cells[62, (2 + i) - 16] = monthInYear[k].ExtraHighCount;
                    newWorksheet.Cells[63, (2 + i) - 16] = string.Format("{0}:{1}:{2}", (int)monthInYear[k].ExtraHighTime.TotalHours, monthInYear[k].ExtraHighTime.Minutes, monthInYear[k].ExtraHighTime.Seconds);
                    newWorksheet.Cells[64, (2 + i) - 16] = monthInYear[k].TppaCount;
                    newWorksheet.Cells[65, (2 + i) - 16] = string.Format("{0}:{1}:{2}", (int)monthInYear[k].TppaTime.TotalHours, monthInYear[k].TppaTime.Minutes, monthInYear[k].TppaTime.Seconds);
                    newWorksheet.Cells[66, (2 + i) - 16] = monthInYear[k].RprTppaCount;
                    newWorksheet.Cells[67, (2 + i) - 16] = string.Format("{0}:{1}:{2}", (int)monthInYear[k].RprTppaTime.TotalHours, monthInYear[k].RprTppaTime.Minutes, monthInYear[k].RprTppaTime.Seconds);

                    columnName = "";
                    columnNum = (2 + i) - 16;
                    while (columnNum > 0)
                    {
                        int modulo = 0;
                        if(monthInYear[k].NumOfDays ==31)
                        {
                            modulo = (columnNum - 1) % 26;
                        }
                        else if(monthInYear[k].NumOfDays==30)
                        {
                            modulo = (columnNum) % 26;
                        }
                        else
                        {
                            modulo = (columnNum +2) % 26;
                        }
                        
                        columnName = Convert.ToChar('A' + modulo) + columnName;
                        columnNum = (columnNum - modulo) / 26;
                    }
                    
                    er = newWorksheet.get_Range("A6", $"{columnName}6");
                    er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
                    er.Cells.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);

                    er = newWorksheet.get_Range("A38", $"{columnName}38");
                    er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
                    er.Cells.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                    er.Font.Size = "25";
                    for (int r = 7; r <= 35; r++)
                    {
                        if (r % 2 == 0)
                        {
                            er = newWorksheet.get_Range($"A{r}", $"{columnName}{r}");
                            er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.AliceBlue);
                        }
                    }
                    for (int r = 39; r <= 66; r++)
                    {
                        if (r % 2 == 0)
                        {
                            er = newWorksheet.get_Range($"A{r}", $"{columnName}{r}");
                            er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.AliceBlue);
                        }
                    }
                }

                // Add Quarter-To-Date Worksheet to workbook
                newWorksheet = (Excel.Worksheet)worksheets.Add(After: worksheets[worksheets.Count]);
                newWorksheet.Name = "Quarter-To-Date";
                er = newWorksheet.get_Range("A:A", missingVal);
                er.EntireColumn.ColumnWidth = 45;
                er.Font.Bold = true;

                newWorksheet.Cells[1, 1] = $"Application Name: {monthlyVM.ApplicationName}";
                newWorksheet.Cells[2, 1] = $"Instrument Name: {monthlyVM.InstrumentName}";
                newWorksheet.Cells[3, 1] = $"Computer Name: {monthlyVM.ComputerName}";

                newWorksheet.Cells[4, 1] = $"{year} Quarter-To-Date Report";

                newWorksheet.Cells[6, 1] = "Quarter";
                newWorksheet.Cells[7, 1] = "Build #";
                newWorksheet.Cells[8, 1] = "Installation";
                newWorksheet.Cells[9, 1] = "Daily Startup Prime 10x";
                newWorksheet.Cells[10, 1] = "20x Liquinox Prime";
                newWorksheet.Cells[11, 1] = "50x Prime Di Waster";
                newWorksheet.Cells[12, 1] = "Daily Shut Down - Empty Waste";
                newWorksheet.Cells[13, 1] = "Daily Shutdown - 50x Prime Di";
                newWorksheet.Cells[14, 1] = "Probe Auto Alignment";
                newWorksheet.Cells[15, 1] = "Probe Manual Alignment";
                newWorksheet.Cells[16, 1] = "Focus Alignment";
                newWorksheet.Cells[17, 1] = "Camera Alignment";
                newWorksheet.Cells[18, 1] = "# of Completed Worklist Runs";
                newWorksheet.Cells[19, 1] = "Total Instrument Runtime (hh:mm:ss)";
                newWorksheet.Cells[20, 1] = "Fluid Transfer Action(s) (hh:mm:ss)";
                newWorksheet.Cells[21, 1] = "Shaker Action(s) (hh:mm:ss)";
                newWorksheet.Cells[22, 1] = "Incubation Action(s) (hh:mm:ss)";
                newWorksheet.Cells[23, 1] = "Image(s) Captured";
                newWorksheet.Cells[24, 1] = "# of Screen Action";
                newWorksheet.Cells[25, 1] = "Screen Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[26, 1] = "# of Low Titer Action";
                newWorksheet.Cells[27, 1] = "Low Titer Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[28, 1] = "# of High Ttiter Action";
                newWorksheet.Cells[29, 1] = "High Titer Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[30, 1] = "# of Extra High Titer Action";
                newWorksheet.Cells[31, 1] = "Extra High Titer Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[32, 1] = "# of TPPA Test Action";
                newWorksheet.Cells[33, 1] = "TPPA Test Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[34, 1] = "# of RPR+TPPA Test Action";
                newWorksheet.Cells[35, 1] = "RPR+TPPA Test Action Runtime (hh:mm:ss)";

                for (j = 0; j < quarterlyVM.Quarters.Count; j++)
                {
                    newWorksheet.Columns[2 + j].ColumnWidth = 20;
                    newWorksheet.Columns[2 + j].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    newWorksheet.Cells[6, 2 + j] = quarterlyVM.Quarters[j].Name;
                    newWorksheet.Cells[6, 2 + j].Font.Bold = true;
                    //newWorksheet.Cells[7, 2 + j] = quarterlyVM.Quarters[j].QuarterlyPrimeInstrumentCount;
                    //newWorksheet.Cells[8, 2 + j] = quarterlyVM.Quarters[j].QuarterlyProbeAlignmentCount;
                    //newWorksheet.Cells[9, 2 + j] = quarterlyVM.Quarters[j].QuarterlyReaderAlignmentCount;
                    //newWorksheet.Cells[10, 2 + j] = quarterlyVM.Quarters[j].QuarterlyReaderCalibrationCount;
                    //newWorksheet.Cells[11, 2 + j] = quarterlyVM.Quarters[j].QuarterlyLuminescenceReaderCalCount;
                    //newWorksheet.Cells[12, 2 + j] = quarterlyVM.Quarters[j].QuarterlyWashPumpCalibrationCount;
                    //newWorksheet.Cells[13, 2 + j] = quarterlyVM.Quarters[j].QuarterlyCameraAlignmentCount;
                    //newWorksheet.Cells[14, 2 + j] = quarterlyVM.Quarters[j].QuarterlyCameraFocusCount;
                    //newWorksheet.Cells[15, 2 + j] = quarterlyVM.Quarters[j].QuarterlyOpenSoftwareCount;
                    //newWorksheet.Cells[16, 2 + j] = quarterlyVM.Quarters[j].QuarterlyCloseSoftwareCount;
                    //newWorksheet.Cells[17, 2 + j] = quarterlyVM.Quarters[j].QuarterlyNonSoftwareRelatedCount;
                    newWorksheet.Cells[18, 2 + j] = quarterlyVM.Quarters[j].QuarterlyCompletedWorklistRun;
                    newWorksheet.Cells[19, 2 + j] = string.Format("{0}:{1}:{2}", (int)quarterlyVM.Quarters[j].QuarterlyTotalRunTime.TotalHours, quarterlyVM.Quarters[j].QuarterlyTotalRunTime.Minutes, quarterlyVM.Quarters[j].QuarterlyTotalRunTime.Seconds);
                    newWorksheet.Cells[20, 2 + j] =  string.Format("{0}:{1}:{2}", (int)quarterlyVM.Quarters[j].QuarterlyFluidTransferAction.TotalHours, quarterlyVM.Quarters[j].QuarterlyFluidTransferAction.Minutes, quarterlyVM.Quarters[j].QuarterlyFluidTransferAction.Seconds);
                    newWorksheet.Cells[21, 2 + j] = quarterlyVM.Quarters[j].QuarterlyShakerAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[22, 2 + j] = string.Format("{0}:{1}:{2}", (int)quarterlyVM.Quarters[j].QuarterlyIncubateAction.TotalHours, quarterlyVM.Quarters[j].QuarterlyIncubateAction.Minutes, quarterlyVM.Quarters[j].QuarterlyIncubateAction.Seconds);
                    newWorksheet.Cells[23, 2 + j] = quarterlyVM.Quarters[j].QuarterlyImageCaptureCount;
                    newWorksheet.Cells[24, 2 + j] = quarterlyVM.Quarters[j].QuarterlyScreenCount;
                    newWorksheet.Cells[25, 2 + j] = string.Format("{0}:{1}:{2}", (int)quarterlyVM.Quarters[j].QuarterlyScreenTime.TotalHours, quarterlyVM.Quarters[j].QuarterlyScreenTime.Minutes, quarterlyVM.Quarters[j].QuarterlyScreenTime.Seconds);
                    newWorksheet.Cells[26, 2 + j] = quarterlyVM.Quarters[j].QuarterlyLowCount;
                    newWorksheet.Cells[27, 2 + j] = string.Format("{0}:{1}:{2}", (int)quarterlyVM.Quarters[j].QuarterlyLowTime.TotalHours, quarterlyVM.Quarters[j].QuarterlyLowTime.Minutes, quarterlyVM.Quarters[j].QuarterlyLowTime.Seconds);
                    newWorksheet.Cells[28, 2 + j] = quarterlyVM.Quarters[j].QuarterlyHighCount;
                    newWorksheet.Cells[29, 2 + j] = string.Format("{0}:{1}:{2}", (int)quarterlyVM.Quarters[j].QuarterlyHighTime.TotalHours, quarterlyVM.Quarters[j].QuarterlyHighTime.Minutes, quarterlyVM.Quarters[j].QuarterlyHighTime.Seconds);
                    newWorksheet.Cells[30, 2 + j] = quarterlyVM.Quarters[j].QuarterlyExtraHighCount;
                    newWorksheet.Cells[31, 2 + j] = string.Format("{0}:{1}:{2}", (int)quarterlyVM.Quarters[j].QuarterlyExtraHighTime.TotalHours, quarterlyVM.Quarters[j].QuarterlyExtraHighTime.Minutes, quarterlyVM.Quarters[j].QuarterlyExtraHighTime.Seconds);
                    newWorksheet.Cells[32, 2 + j] = quarterlyVM.Quarters[j].QuarterlyTppaCount;
                    newWorksheet.Cells[33, 2 + j] = string.Format("{0}:{1}:{2}", (int)quarterlyVM.Quarters[j].QuarterlyTppaTime.TotalHours, quarterlyVM.Quarters[j].QuarterlyTppaTime.Minutes, quarterlyVM.Quarters[j].QuarterlyTppaTime.Seconds);
                    newWorksheet.Cells[34, 2 + j] = quarterlyVM.Quarters[j].QuarterlyRprTppaCount;
                    newWorksheet.Cells[35, 2 + j] = string.Format("{0}:{1}:{2}", (int)quarterlyVM.Quarters[j].QuarterlyRprTppaTime.TotalHours, quarterlyVM.Quarters[j].QuarterlyRprTppaTime.Minutes, quarterlyVM.Quarters[j].QuarterlyTppaTime.Seconds);
                }

                columnName = "";
                columnNum = 2 + j - 1;
                while (columnNum > 0)
                {
                    int modulo = (columnNum - 1) % 26;
                    columnName = Convert.ToChar('A' + modulo) + columnName;
                    columnNum = (columnNum - modulo) / 26;
                }

                er = newWorksheet.get_Range("A6", $"{columnName}6");
                er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
                er.Cells.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                for (int r = 7; r <= 30; r++)
                {
                    if (r % 2 == 0)
                    {
                        er = newWorksheet.get_Range($"A{r}", $"{columnName}{r}");
                        er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.AliceBlue);
                    }
                }

                // Add Half-Year-To_Date Worksheet
                newWorksheet = (Excel.Worksheet)worksheets.Add(After: worksheets[worksheets.Count]);
                newWorksheet.Name = "Half-Year-To-Date";
                er = newWorksheet.get_Range("A:A", missingVal);
                er.EntireColumn.ColumnWidth = 45;
                er.Font.Bold = true;

                newWorksheet.Cells[1, 1] = $"Application Name: {monthlyVM.ApplicationName}";
                newWorksheet.Cells[2, 1] = $"Instrument Name: {monthlyVM.InstrumentName}";
                newWorksheet.Cells[3, 1] = $"Computer Name: {monthlyVM.ComputerName}";

                newWorksheet.Cells[4, 1] = $"{year} Half-Year-To-Date Report";

                newWorksheet.Cells[6, 1] = "Half";
                newWorksheet.Cells[7, 1] = "Build #";
                newWorksheet.Cells[8, 1] = "Installation";
                newWorksheet.Cells[9, 1] = "Daily Startup Prime 10x";
                newWorksheet.Cells[10, 1] = "20x Liquinox Prime";
                newWorksheet.Cells[11, 1] = "50x Prime Di Waster";
                newWorksheet.Cells[12, 1] = "Daily Shut Down - Empty Waste";
                newWorksheet.Cells[13, 1] = "Daily Shutdown - 50x Prime Di";
                newWorksheet.Cells[14, 1] = "Probe Auto Alignment";
                newWorksheet.Cells[15, 1] = "Probe Manual Alignment";
                newWorksheet.Cells[16, 1] = "Focus Alignment";
                newWorksheet.Cells[17, 1] = "Camera Alignment";
                newWorksheet.Cells[18, 1] = "# of Completed Worklist Runs";
                newWorksheet.Cells[19, 1] = "Total Instrument Runtime (hh:mm:ss)";
                newWorksheet.Cells[20, 1] = "Fluid Transfer Action(s) (hh:mm:ss)";
                newWorksheet.Cells[21, 1] = "Shaker Action(s) (hh:mm:ss)";
                newWorksheet.Cells[22, 1] = "Incubation Action(s) (hh:mm:ss)";
                newWorksheet.Cells[23, 1] = "Image(s) Captured";
                newWorksheet.Cells[24, 1] = "# of Screen Action";
                newWorksheet.Cells[25, 1] = "Screen Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[26, 1] = "# of Low Titer Action";
                newWorksheet.Cells[27, 1] = "Low Titer Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[28, 1] = "# of High Ttiter Action";
                newWorksheet.Cells[29, 1] = "High Titer Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[30, 1] = "# of Extra High Titer Action";
                newWorksheet.Cells[31, 1] = "Extra High Titer Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[32, 1] = "# of TPPA Test Action";
                newWorksheet.Cells[33, 1] = "TPPA Test Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[34, 1] = "# of RPR+TPPA Test Action";
                newWorksheet.Cells[35, 1] = "RPR+TPPA Test Action Runtime (hh:mm:ss)";

                for (j = 0; j < semiyearlyVM.SemiYears.Count; j++)
                {
                    newWorksheet.Columns[2 + j].ColumnWidth = 20;
                    newWorksheet.Columns[2 + j].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    newWorksheet.Cells[6, 2 + j] = semiyearlyVM.SemiYears[j].Name;
                    newWorksheet.Cells[6, 2 + j].Font.Bold = true;
                    //newWorksheet.Cells[7, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyPrimeInstrumentCount;
                    //newWorksheet.Cells[8, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyProbeAlignmentCount;
                    //newWorksheet.Cells[9, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyReaderAlignmentCount;
                    //newWorksheet.Cells[10, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyReaderCalibrationCount;
                    //newWorksheet.Cells[11, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyLuminescenceReaderCalCount;
                    //newWorksheet.Cells[12, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyWashPumpCalibrationCount;
                    //newWorksheet.Cells[13, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyCameraAlignmentCount;
                    //newWorksheet.Cells[14, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyCameraFocusCount;
                    //newWorksheet.Cells[15, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyOpenSoftwareCount;
                    //newWorksheet.Cells[16, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyCloseSoftwareCount;
                    //newWorksheet.Cells[17, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyNonSoftwareRelatedCount;
                    newWorksheet.Cells[18, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyCompletedWorklistRun;
                    newWorksheet.Cells[19, 2 + j] = string.Format("{0}:{1}:{2}", (int)semiyearlyVM.SemiYears[j].SemiYearlyTotalRunTime.TotalHours, semiyearlyVM.SemiYears[j].SemiYearlyTotalRunTime.Minutes, semiyearlyVM.SemiYears[j].SemiYearlyTotalRunTime.Seconds);
                    newWorksheet.Cells[20, 2 + j] = string.Format("{0}:{1}:{2}", (int)semiyearlyVM.SemiYears[j].SemiYearlyFluidTransferAction.TotalHours, semiyearlyVM.SemiYears[j].SemiYearlyFluidTransferAction.Minutes, semiyearlyVM.SemiYears[j].SemiYearlyFluidTransferAction.Seconds);
                    newWorksheet.Cells[21, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyShakerAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[22, 2 + j] = string.Format("{0}:{1}:{2}", (int)semiyearlyVM.SemiYears[j].SemiYearlyIncubateAction.TotalHours, semiyearlyVM.SemiYears[j].SemiYearlyIncubateAction.Minutes, semiyearlyVM.SemiYears[j].SemiYearlyIncubateAction.Seconds);
                    newWorksheet.Cells[23, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyImageCaptureCount;
                    newWorksheet.Cells[24, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyScreenCount;
                    newWorksheet.Cells[25, 2 + j] = string.Format("{0}:{1}:{2}", (int)semiyearlyVM.SemiYears[j].SemiYearlyScreenTime.TotalHours, semiyearlyVM.SemiYears[j].SemiYearlyScreenTime.Minutes, semiyearlyVM.SemiYears[j].SemiYearlyScreenTime.Seconds);
                    newWorksheet.Cells[26, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyLowCount;
                    newWorksheet.Cells[27, 2 + j] = string.Format("{0}:{1}:{2}", (int)semiyearlyVM.SemiYears[j].SemiYearlyLowTime.TotalHours, semiyearlyVM.SemiYears[j].SemiYearlyLowTime.Minutes, semiyearlyVM.SemiYears[j].SemiYearlyLowTime.Seconds);
                    newWorksheet.Cells[28, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyHighCount;
                    newWorksheet.Cells[29, 2 + j] = string.Format("{0}:{1}:{2}", (int)semiyearlyVM.SemiYears[j].SemiYearlyHighTime.TotalHours, semiyearlyVM.SemiYears[j].SemiYearlyHighTime.Minutes, semiyearlyVM.SemiYears[j].SemiYearlyHighTime.Seconds);
                    newWorksheet.Cells[30, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyExtraHighCount;
                    newWorksheet.Cells[31, 2 + j] = string.Format("{0}:{1}:{2}", (int)semiyearlyVM.SemiYears[j].SemiYearlyExtraHighTime.TotalHours, semiyearlyVM.SemiYears[j].SemiYearlyExtraHighTime.Minutes, semiyearlyVM.SemiYears[j].SemiYearlyExtraHighTime.Seconds);
                    newWorksheet.Cells[32, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyTppaCount;
                    newWorksheet.Cells[33, 2 + j] = string.Format("{0}:{1}:{2}", (int)semiyearlyVM.SemiYears[j].SemiYearlyTppaTime.TotalHours, semiyearlyVM.SemiYears[j].SemiYearlyTppaTime.Minutes, semiyearlyVM.SemiYears[j].SemiYearlyTppaTime.Seconds);
                    newWorksheet.Cells[34, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyRprTppaCount;
                    newWorksheet.Cells[35, 2 + j] = string.Format("{0}:{1}:{2}", (int)semiyearlyVM.SemiYears[j].SemiYearlyRprTppaTime.TotalHours, semiyearlyVM.SemiYears[j].SemiYearlyRprTppaTime.Minutes, semiyearlyVM.SemiYears[j].SemiYearlyRprTppaTime.Seconds);
                    /*
                    newWorksheet.Cells[7, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyPrimeInstrumentCount;
                    newWorksheet.Cells[8, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyProbeAlignmentCount;
                    newWorksheet.Cells[9, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyReaderAlignmentCount;
                    newWorksheet.Cells[10, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyReaderCalibrationCount;
                    newWorksheet.Cells[11, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyLuminescenceReaderCalCount;
                    newWorksheet.Cells[12, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyWashPumpCalibrationCount;
                    newWorksheet.Cells[13, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyCameraAlignmentCount;
                    newWorksheet.Cells[14, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyCameraFocusCount;
                    newWorksheet.Cells[15, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyOpenSoftwareCount;
                    newWorksheet.Cells[16, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyCloseSoftwareCount;
                    newWorksheet.Cells[17, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyNonSoftwareRelatedCount;
                    newWorksheet.Cells[18, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyTotalWorklistRun;
                    newWorksheet.Cells[19, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyCompletedWorklistRun;
                    newWorksheet.Cells[20, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyStoppedWorklistRun;
                    newWorksheet.Cells[21, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyTotalRunTime.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[22, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyFluidTransferAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[23, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyWashAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[24, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyShakerAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[25, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyIncubateAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[26, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyHeatAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[27, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyReadWellAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[28, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyPrimeAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[29, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyProbeWashAction.ToString(@"hh\:mm\:ss");
                    newWorksheet.Cells[30, 2 + j] = semiyearlyVM.SemiYears[j].SemiYearlyImageCaptureCount; */
                }

                columnName = "";
                columnNum = 2 + j - 1;
                while (columnNum > 0)
                {
                    int modulo = (columnNum - 1) % 26;
                    columnName = Convert.ToChar('A' + modulo) + columnName;
                    columnNum = (columnNum - modulo) / 26;
                }

                er = newWorksheet.get_Range("A6", $"{columnName}6");
                er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
                er.Cells.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                for (int r = 7; r <= 30; r++)
                {
                    if (r % 2 == 0)
                    {
                        er = newWorksheet.get_Range($"A{r}", $"{columnName}{r}");
                        er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.AliceBlue);
                    }
                }

                // Add Year-To-Date Worksheet
                newWorksheet = (Excel.Worksheet)worksheets.Add(After: worksheets[worksheets.Count]);
                newWorksheet.Name = "Year-To-Date";
                er = newWorksheet.get_Range("A:A", missingVal);
                er.EntireColumn.ColumnWidth = 45;
                er.Font.Bold = true;

                newWorksheet.Cells[1, 1] = $"Application Name: {monthlyVM.ApplicationName}";
                newWorksheet.Cells[2, 1] = $"Instrument Name: {monthlyVM.InstrumentName}";
                newWorksheet.Cells[3, 1] = $"Computer Name: {monthlyVM.ComputerName}";

                newWorksheet.Cells[4, 1] = $"{year} Year-To-Date Report";

                newWorksheet.Cells[7, 1] = "Build #";
                newWorksheet.Cells[8, 1] = "Installation";
                newWorksheet.Cells[9, 1] = "Daily Startup Prime 10x";
                newWorksheet.Cells[10, 1] = "20x Liquinox Prime";
                newWorksheet.Cells[11, 1] = "50x Prime Di Waster";
                newWorksheet.Cells[12, 1] = "Daily Shut Down - Empty Waste";
                newWorksheet.Cells[13, 1] = "Daily Shutdown - 50x Prime Di";
                newWorksheet.Cells[14, 1] = "Probe Auto Alignment";
                newWorksheet.Cells[15, 1] = "Probe Manual Alignment";
                newWorksheet.Cells[16, 1] = "Focus Alignment";
                newWorksheet.Cells[17, 1] = "Camera Alignment";
                newWorksheet.Cells[18, 1] = "# of Completed Worklist Runs";
                newWorksheet.Cells[19, 1] = "Total Instrument Runtime (hh:mm:ss)";
                newWorksheet.Cells[20, 1] = "Fluid Transfer Action(s) (hh:mm:ss)";
                newWorksheet.Cells[21, 1] = "Shaker Action(s) (hh:mm:ss)";
                newWorksheet.Cells[22, 1] = "Incubation Action(s) (hh:mm:ss)";
                newWorksheet.Cells[23, 1] = "Image(s) Captured";
                newWorksheet.Cells[24, 1] = "# of Screen Action";
                newWorksheet.Cells[25, 1] = "Screen Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[26, 1] = "# of Low Titer Action";
                newWorksheet.Cells[27, 1] = "Low Titer Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[28, 1] = "# of High Ttiter Action";
                newWorksheet.Cells[29, 1] = "High Titer Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[30, 1] = "# of Extra High Titer Action";
                newWorksheet.Cells[31, 1] = "Extra High Titer Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[32, 1] = "# of TPPA Test Action";
                newWorksheet.Cells[33, 1] = "TPPA Test Action Runtime (hh:mm:ss)";
                newWorksheet.Cells[34, 1] = "# of RPR+TPPA Test Action";
                newWorksheet.Cells[35, 1] = "RPR+TPPA Test Action Runtime (hh:mm:ss)";

                er = newWorksheet.get_Range("B:B", missingVal);
                er.EntireColumn.ColumnWidth = 20;
                er.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                //newWorksheet.Cells[7, 2] = yearlyVM.Year.YearlyPrimeInstrumentCount;
                //newWorksheet.Cells[8, 2] = yearlyVM.Year.YearlyProbeAlignmentCount;
                //newWorksheet.Cells[9, 2] = yearlyVM.Year.YearlyReaderAlignmentCount;
                //newWorksheet.Cells[10, 2] = yearlyVM.Year.YearlyReaderCalibrationCount;
                //newWorksheet.Cells[11, 2] = yearlyVM.Year.YearlyLuminescenceReaderCalCount;
                //newWorksheet.Cells[12, 2] = yearlyVM.Year.YearlyWashPumpCalibrationCount;
                //newWorksheet.Cells[13, 2] = yearlyVM.Year.YearlyCameraAlignmentCount;
                //newWorksheet.Cells[14, 2] = yearlyVM.Year.YearlyCameraFocusCount;
                //newWorksheet.Cells[15, 2] = yearlyVM.Year.YearlyOpenSoftwareCount;
                //newWorksheet.Cells[16, 2] = yearlyVM.Year.YearlyCloseSoftwareCount;
                //newWorksheet.Cells[17, 2] = yearlyVM.Year.YearlyNonSoftwareRelatedCount;
                newWorksheet.Cells[18, 2] = yearlyVM.Year.YearlyCompletedWorklistRun;
                newWorksheet.Cells[19, 2] = string.Format("{0}:{1}:{2}", (int)yearlyVM.Year.YearlyTotalRunTime.TotalHours, yearlyVM.Year.YearlyTotalRunTime.Minutes, yearlyVM.Year.YearlyTotalRunTime.Seconds);
                newWorksheet.Cells[20, 2] = string.Format("{0}:{1}:{2}", (int)yearlyVM.Year.YearlyFluidTransferAction.TotalHours, yearlyVM.Year.YearlyFluidTransferAction.Minutes, yearlyVM.Year.YearlyFluidTransferAction.Seconds);
                newWorksheet.Cells[21, 2] = yearlyVM.Year.YearlyShakerAction.ToString(@"hh\:mm\:ss");
                newWorksheet.Cells[22, 2] = string.Format("{0}:{1}:{2}", (int)yearlyVM.Year.YearlyIncubateAction.TotalHours, yearlyVM.Year.YearlyIncubateAction.Minutes, yearlyVM.Year.YearlyIncubateAction.Seconds);
                newWorksheet.Cells[23, 2] = yearlyVM.Year.YearlyImageCaptureCount;
                newWorksheet.Cells[24, 2] = yearlyVM.Year.YearlyScreenCount;
                newWorksheet.Cells[25, 2] = string.Format("{0}:{1}:{2}", (int)yearlyVM.Year.YearlyScreenTime.TotalHours, yearlyVM.Year.YearlyScreenTime.Minutes, yearlyVM.Year.YearlyScreenTime.Seconds);
                newWorksheet.Cells[26, 2] = yearlyVM.Year.YearlyLowCount;
                newWorksheet.Cells[27, 2] = string.Format("{0}:{1}:{2}", (int)yearlyVM.Year.YearlyLowTime.TotalHours, yearlyVM.Year.YearlyLowTime.Minutes, yearlyVM.Year.YearlyLowTime.Seconds);
                newWorksheet.Cells[28, 2] = yearlyVM.Year.YearlyHighCount;
                newWorksheet.Cells[29, 2] = string.Format("{0}:{1}:{2}", (int)yearlyVM.Year.YearlyHighTime.TotalHours, yearlyVM.Year.YearlyHighTime.Minutes, yearlyVM.Year.YearlyHighTime.Seconds);
                newWorksheet.Cells[30, 2] = yearlyVM.Year.YearlyExtraHighCount;
                newWorksheet.Cells[31, 2] = string.Format("{0}:{1}:{2}", (int)yearlyVM.Year.YearlyExtraHighTime.TotalHours, yearlyVM.Year.YearlyExtraHighTime.Minutes, yearlyVM.Year.YearlyExtraHighTime.Seconds);
                newWorksheet.Cells[32, 2] = yearlyVM.Year.YearlyTppaCount;
                newWorksheet.Cells[33, 2] = string.Format("{0}:{1}:{2}", (int)yearlyVM.Year.YearlyTppaTime.TotalHours, yearlyVM.Year.YearlyTppaTime.Minutes, yearlyVM.Year.YearlyTppaTime.Seconds);
                newWorksheet.Cells[34, 2] = yearlyVM.Year.YearlyRprTppaCount;
                newWorksheet.Cells[35, 2] = string.Format("{0}:{1}:{2}", (int)yearlyVM.Year.YearlyRprTppaTime.TotalHours, yearlyVM.Year.YearlyRprTppaTime.Minutes, yearlyVM.Year.YearlyRprTppaTime.Seconds);

                /*  newWorksheet.Cells[6, 2] = yearlyVM.Year.YearlyPrimeInstrumentCount;
                  newWorksheet.Cells[7, 2] = yearlyVM.Year.YearlyProbeAlignmentCount;
                  newWorksheet.Cells[8, 2] = yearlyVM.Year.YearlyReaderAlignmentCount;
                  newWorksheet.Cells[9, 2] = yearlyVM.Year.YearlyReaderCalibrationCount;
                  newWorksheet.Cells[10, 2] = yearlyVM.Year.YearlyLuminescenceReaderCalCount;
                  newWorksheet.Cells[11, 2] = yearlyVM.Year.YearlyWashPumpCalibrationCount;
                  newWorksheet.Cells[12, 2] = yearlyVM.Year.YearlyCameraAlignmentCount;
                  newWorksheet.Cells[13, 2] = yearlyVM.Year.YearlyCameraFocusCount;
                  newWorksheet.Cells[14, 2] = yearlyVM.Year.YearlyOpenSoftwareCount;
                  newWorksheet.Cells[15, 2] = yearlyVM.Year.YearlyCloseSoftwareCount;
                  newWorksheet.Cells[16, 2] = yearlyVM.Year.YearlyNonSoftwareRelatedCount;
                  newWorksheet.Cells[17, 2] = yearlyVM.Year.YearlyTotalWorklistRun;
                  newWorksheet.Cells[18, 2] = yearlyVM.Year.YearlyCompletedWorklistRun;
                  newWorksheet.Cells[19, 2] = yearlyVM.Year.YearlyStoppedWorklistRun;
                  newWorksheet.Cells[20, 2] = yearlyVM.Year.YearlyTotalRunTime.ToString(@"hh\:mm\:ss");
                  newWorksheet.Cells[21, 2] = yearlyVM.Year.YearlyFluidTransferAction.ToString(@"hh\:mm\:ss");
                  newWorksheet.Cells[22, 2] = yearlyVM.Year.YearlyWashAction.ToString(@"hh\:mm\:ss");
                  newWorksheet.Cells[23, 2] = yearlyVM.Year.YearlyShakerAction.ToString(@"hh\:mm\:ss");
                  newWorksheet.Cells[24, 2] = yearlyVM.Year.YearlyIncubateAction.ToString(@"hh\:mm\:ss");
                  newWorksheet.Cells[25, 2] = yearlyVM.Year.YearlyHeatAction.ToString(@"hh\:mm\:ss");
                  newWorksheet.Cells[26, 2] = yearlyVM.Year.YearlyReadWellAction.ToString(@"hh\:mm\:ss");
                  newWorksheet.Cells[27, 2] = yearlyVM.Year.YearlyPrimeAction.ToString(@"hh\:mm\:ss");
                  newWorksheet.Cells[28, 2] = yearlyVM.Year.YearlyProbeWashAction.ToString(@"hh\:mm\:ss");
                  newWorksheet.Cells[29, 2] = yearlyVM.Year.YearlyImageCaptureCount;*/

                for (int r = 6; r < 30; r++)
                {
                    if (r % 2 == 0)
                    {
                        er = newWorksheet.get_Range($"A{r}", $"B{r}");
                        er.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.AliceBlue);
                    }
                }

                foreach (Excel.Worksheet ws in workbook.Worksheets)
                {
                    ws.Activate();
                    ws.Application.ActiveWindow.SplitColumn = 1;
                    ws.Application.ActiveWindow.FreezePanes = true;
                }

                xlApp.Visible = true;

                Marshal.ReleaseComObject(xlApp);
                Marshal.ReleaseComObject(worksheets);
                Marshal.ReleaseComObject(workbook);
                Marshal.ReleaseComObject(newWorksheet);
                Marshal.ReleaseComObject(er);

                newWorksheet = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                xlApp.Quit();
            }
            finally
            {
                xlApp = null;
                workbook = null;
                worksheets = null;
                er = null;
            }
        }

        public static void ExportToCSV(string year) { }
    }
}
