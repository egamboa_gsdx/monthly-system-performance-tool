﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSDSystemPerformanceTool
{
    public static class Navigator
    {
        private static IDictionary<string, List<Action<object>>> _actions = new Dictionary<string, List<Action<object>>>();

        public static void Subscribe(string token, Action<object> callback)
        {
            if (!_actions.ContainsKey(token))
            {
                var list = new List<Action<object>>();
                list.Add(callback);
                _actions.Add(token, list);
            }
            else
            {
                bool found = false;
                foreach (var action in _actions[token])
                {
                    if (action.Method.ToString() == callback.Method.ToString())
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    _actions[token].Add(callback);
                }
            }
        }

        public static void Unsubscribe(string token, Action<object> callback)
        {
            if (_actions.ContainsKey(token))
            {
                _actions[token].Remove(callback);
            }
        }

        public static void Notify (string token, object args = null)
        {
            if (_actions.ContainsKey(token))
            {
                foreach (var callback in _actions[token])
                {
                    callback(args);
                }
            }
        }
    }
}
