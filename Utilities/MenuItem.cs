﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSDSystemPerformanceTool
{
    public class MenuItem : MenuItemBase
    {
        public string Title { get; set; }
        public string Key { get; set; }

        public MenuItem(string title, string key)
        {
            Title = title;
            Key = key;
        }
    }

    public abstract class MenuItemBase
    {
        public string Key { get; set; }
    }
}
